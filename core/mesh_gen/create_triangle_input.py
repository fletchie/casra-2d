#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 11:52:01 2018

Creates the input .poly file for triangle i.e. construction of finite element mesh

Triangle software: https://www.cs.cmu.edu/~quake/triangle.html

@author: charlie
"""

import numpy as np

def create_triangle_input(points, edges, filename, bm):
    px = np.hsplit(points, 2)[0]
    py = np.hsplit(points, 2)[1]
    #perform delaunay triangulation of points
    f = open(filename, 'w')
    f.write(str(len(px)) + '\t' + '2 \t 1 \t 1')
    for i in range(len(px)):
        f.write('\n')
        f.write(str(i + 1) + '\t' + str(px[i][0]) + '\t' + str(py[i][0]) + '\t 0')
    f.write('\n')
    f.write('\n')

    #define lines/facets
    f.write(str(len(edges)) + ' \t 1')
    e0 = np.hsplit(edges, 2)[0]
    e1 = np.hsplit(edges, 2)[1]
    f.write('\n')
    for i in range(len(edges)):
        f.write(str(i + 1) + '\t' + str(e0[i][0]) + '\t'  + str(e1[i][0]) + '\t' + str(bm[i]))
        f.write('\n')

    f.write('\n')
    f.write('0')
    f.write('\n')
    f.write('\n')
    f.write('0')
    f.write('\n')

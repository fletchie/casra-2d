

"""
Created on Thursday September 6 14:20:00 2018

Constructs tip mesh from extracted zero-level contour (tip boundary) using
the Delaunay tessellation program `Triangle`

Triangle software: https://www.cs.cmu.edu/~quake/triangle.html

@author: charlie
"""

import numpy as np
import tempfile
import subprocess
import os

import sys

import core.mesh_gen.create_triangle_input as create_triangle_input

def generate_mesh(segments, dc, n, mesh_area = 25.0, mesh_angle = 24.0):

    edges = []
    points = np.array([]).reshape((0, 2))

    bm = []

    #segments[0] = mesh_refinement(segments[0], dc, n)

    for A in segments:
        for i in range(len(points) + 1, len(A) + 1 + len(points)):

            if len(points) == 0:
                bm.append(1)
            else:
                bm.append(0)

            edges.append([i, len(points) + ( i - len(points) ) % (len(A)) + 1])


        p_n = np.vstack((A[:, 0], A[:, 1])).T
        points = np.vstack((points, p_n))

    edges = np.array(edges)

    #create temporary directory for mesh files
    temp_dirpath = tempfile.mkdtemp()

    #write out input .poly file for c++ triangle script
    filename = 'tip.poly'

    cwd = os.getcwd()
    os.chdir(temp_dirpath)

    create_triangle_input.create_triangle_input(points, edges, filename, bm)

    #call triangle to generate mesh
    subprocess.check_output(['triangle','-q' + str(mesh_angle) + ' -a' + str(mesh_area), filename])

    os.chdir(cwd)

    return temp_dirpath, segments[0]

###perform boundary mesh refinement in regions involving a change in dielectric properties
def mesh_refinement(boundary, dc, n):

    dc_boundary_values = dc[boundary[n:-n, 0].astype(int), boundary[n:-n, 1].astype(int)]

    if np.sum(dc_boundary_values < 1e3) > 0:
        #ref = - 8.0 * np.log(dc_boundary_values/1.0e6)/np.log(1.0e6)
        n_boundary = []
        l_b_v = len(dc_boundary_values)
        for i in range(0, l_b_v):

            #perform mesh refinement at boundary
            if dc_boundary_values[i] < 1e3 or dc_boundary_values[(i - 1) % len(dc_boundary_values)] < 1e3 or dc_boundary_values[(i + 1) % len(dc_boundary_values)] < 1e3:

                for j in np.linspace(0.0, 1.0, 19, endpoint = False):
                    bv = boundary[n:-n][i] * (1.0 - j) + boundary[n:-n][i + 1] * j
                    n_boundary.append(bv)
            else:
                n_boundary.append(boundary[n:-n][i])

            #append back on shank boundary nodes
        n_boundary = np.vstack((boundary[:n], np.array(n_boundary), boundary[-n:]))
    else:
        n_boundary = boundary

    return n_boundary

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26/09/18 19:07:12 2018

Optimised extension velocity construction through Cython
although not as sophisticated algorithmically as the fast marching method (higher complexity)

@author: charlie
"""

import cython

import numpy as np
cimport numpy as np

#external function
cdef extern from "math.h":
    double sqrt(double m)


#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def initialisation(double[:, ::1] boundary, int x, int y, int mode):

  cdef Py_ssize_t bmax = boundary.shape[0]

  field = np.zeros((x, y), dtype = np.double)
  cdef double[:, ::1] field_view = field

  cdef double dist1 = 2.0 * (x * x + y * y)
  cdef double dist2 =  2.0 * (x * x + y * y)
  cdef double tdist = 0.0

  cdef double c1x = 0.0
  cdef double c1y = 0.0

  cdef double c2x = 0.0
  cdef double c2y = 0.0

  cdef Py_ssize_t i, j, v
  cdef Py_ssize_t xmax = x
  cdef Py_ssize_t ymax = y

  cdef double sb = 0.0
  cdef double sa = 0.0
  cdef double l = 0.0
  cdef double AB = 0.0
  cdef double cp = 0.0
  cdef int flip = 1

  for i in range(xmax):
    for j in range(ymax):

      dist1 = 2.0 * (x * x + y * y)
      #sbv1 = 0.0

      for v in range(bmax):
        tdist = (i - boundary[v, 0])**2.0 + (j - boundary[v, 1])**2.0

        if tdist < dist1:
          dist2 = dist1
          dist1 = tdist

          c2x = c1x
          c2y = c1y

          c1x = boundary[v, 0]
          c1y = boundary[v, 1]

        elif tdist < dist2:
          dist2 = tdist

          c2x = boundary[v, 0]
          c2y = boundary[v, 1]

      AB = sqrt((c2x - c1x) ** 2.0 + (c2y - c1y) ** 2.0)

      sb = ( (c2x - i) * (c2x - c1x) + (c2y - j) * (c2y - c1y) )/AB
      sa = ( (c1x - i) * (c2x - c1x) + (c1y - j) * (c2y - c1y) )/AB

      l = (sb - sa)/AB

      flip = -1
      #is point inside or outside polygon?
      if mode == 1:
        for v in range(bmax):
          if ((boundary[v, 1] > (j + 1e-6)) != (boundary[(v + 1 % bmax), 1] > (j + 1e-6))) and (i < (boundary[(v + 1 % bmax), 0]-boundary[v, 0]) * (j-boundary[v, 1]) / (boundary[(v + 1 % bmax), 1] - boundary[v, 1]) + boundary[v, 0]):
            flip = - flip
      else:

        for v in range(bmax):
          if j > min(boundary[(v + 1) % bmax, 1], boundary[v, 1]):
            if j <= max(boundary[(v + 1) % bmax, 1], boundary[v, 1]):
              if i <= max(boundary[(v + 1) % bmax, 0], boundary[v, 0]):
                if (boundary[v, 1] != boundary[(v + 1) % bmax, 1]):
                  xints = (j - boundary[v, 1]) * (boundary[(v + 1) % bmax, 0] - boundary[v, 0])/(boundary[(v + 1) % bmax, 1] - boundary[v, 1]) + boundary[v, 0]
                if boundary[v, 0] == boundary[(v + 1) % bmax, 0] or i <= xints:
                  flip = - flip

      if l * l >= 1.0:
        field_view[i, j] = sqrt(dist1) * flip
      else:
        field_view[i, j] =  (sqrt(dist1) * l + sqrt(dist2) * (1.0 - l)) * flip

  return field

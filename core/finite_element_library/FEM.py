#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 11:02:10 2018

@author: charlie

This is a library for implementing the 2D finite element weak solution for
triangle elements


Key components:
1. construct stiffness matrix
    - define function for constructing triangle matrix -- triangle_stiffness_matrix
    - Assemble global matrix based off triangle matrix solutions -- construct_global_stiffness_matrix
2. Modify stiffness matrix to handle Dirichlet (fixed potential) boundary conditions
    - Inserts a row of 0s across stiffness matrix, with a 1 at the potential value to be fixed
    - Constructs the right-side b-vector for fixing the potential value
3. Construct the Neumann BC coupling matrix
    - Integrates over each test solution basis functions over mesh boundary. As the basis functions
    are largely zero everywhere a simple analytical solution is used. The matrix returned is of equal
    dimension to the number of boundary nodes.
    - This matrix is then transformed into a larger matrix with dimensions matching the total number of mesh nodes
    over the domain via insertion of zeros. Such a matrix can then be inserted into the total linear system.

"""

import numpy as np
import sys

try:
    import pandas
except ModuleNotFoundError:
    print("Please install latest version of pandas.")
    sys.exit()

import sys

#Import the mesh coordinates - from .node
def obtain_mesh_coords(directory = 'circle.1.node'):
    df = pandas.read_csv(directory, skiprows=[0], sep = '\s+', header = None, comment='#')
    df.columns = ['ind', 'x', 'y', 'p1', 'p2']
    return df[['x', 'y']].values

#Import facet connectivity matrix - from .ele
def obtain_connectivity_Matrix(directory = 'circle.1.ele'):
    df = pandas.read_csv(directory, skiprows=[0], sep = '\s+', header = None, comment='#')
    df.columns = ['ind', 'x1', 'x2', 'x3']
    return df[['x1', 'x2', 'x3']].values - 1

#Obtain unordered mesh boundary - from .poly
def obtain_boundary_indices_unordered(directory = 'circle.1.poly'):
    df = pandas.read_csv(directory, skiprows=[0, 1], sep = '\s+', header = None, comment='#')[:-1]
    df.columns = ['i', 'ind1', 'ind2', 'p1']
    return df[['ind1']].values.astype(int).flatten() - 1

#Obtain ordered mesh boundary - from .poly
def obtain_boundary_indices_ordered(directory = 'circle.1.poly'):
    df = pandas.read_csv(directory, skiprows=[0, 1], sep = '\s+', header = None, comment='#')[:-1]
    df.columns = ['i', 'ind1', 'ind2', 'p1']

    #boundary
    start_0 = 1

    ni1 = df['ind1'].astype(int).tolist()
    ni2 = df['ind2'].astype(int).tolist()

    b1 = [ni1[0]]
    b2 = [ni2[0]]

    del ni1[0]
    del ni2[0]

    #order boundary indices
    for i in range(0, len(ni1)):

        try:
            start_0 = ni1.index(b2[-1])
            b1.append(ni1[start_0])
            b2.append(ni2[start_0])

        except ValueError:
            try:
                start_0 = ni2.index(b2[-1])
                b2.append(ni1[start_0])
                b1.append(ni2[start_0])

            except ValueError:
                #This appears to be required due to an error in the triangle mesh generator
                #where non-boundary edges are sometimes included within the .poly file
                break

        del ni1[start_0]
        del ni2[start_0]

    return np.array(b1).astype(int) - 1, np.array(b2).astype(int) - 1

#construct triangle stiffness matrix given three triangle node points
def triangle_stiffness_matrix_laplace(x):

    """
    Constructs stiffness matrix for particular triangle of the domain.
    Based off analytical solution for elements constructed via
    linear basis functions.

    :param1 x1: triangle node 1
    :param2 x2: triangle node 2
    :param3 x3: triangle node 3
    :returns: stiffness matrix

    """

    x1 = x[0]
    x2 = x[1]
    x3 = x[2]

    n00 = np.sum((x3 - x2) * (x3 - x2))
    n01 = np.sum((x2 - x3) * (x3 - x1))
    n02 = np.sum((x3 - x2) * (x2 - x1))
    n11 = np.sum((x3 - x1) * (x3 - x1))
    n12 = np.sum((x1 - x3) * (x2 - x1))
    n22 = np.sum((x2 - x1) * (x2 - x1))

    #calculate triangle area
    A = abs(x1[0] * (x2[1] - x3[1]) + x2[0] * (x3[1] - x1[1]) + x3[0] * (x1[1] - x2[1]))/2.0

    #calculate the coordinate transform Jacobian and its determinant
    J = np.array([[x2[0] - x1[0], x3[0] - x1[0]], [x2[1] - x1[1], x3[1] - x1[1]]])
    J_det = np.linalg.det(J)

    return A * np.array([[n00, n01, n02], [n01, n11, n12], [n02, n12, n22]])/(J_det) ** 2.0


def construct_global_stiffness_matrix(mesh_coors, connectivity_matrix, c_constant = False):

    """

    Construct global stiffness matrix by calculating values in triangle elements

    :param1 mesh_coors: 2D array of mesh coordinates
    :param2 connectivity_matrix: problem connectivity matrix (element indices of patches)
    :param3 c_constant: The relative permittivity assigned to each triangle tile - None detected, defaults to Laplace solver i.e. c=1.0
    :returns: stiffness matrix

    """

    if type(c_constant) == bool:
        c_constant = np.array([1.0] * len(mesh_coors))

    n_nodes = len(mesh_coors)
    n_el = len(connectivity_matrix)

    stiffness_matrix = np.zeros((n_nodes, n_nodes), dtype = np.float64)

    for k in range(0, n_el):
        for a in range(0, 3):
            for b in range(0, 3):
                i = connectivity_matrix[k, a]
                j = connectivity_matrix[k, b]
                stiffness_matrix[i, j] += c_constant[k] * triangle_stiffness_matrix_laplace(mesh_coors[connectivity_matrix[k]])[a, b]

    return stiffness_matrix

def dirichlet_construction(group, value, global_stiffness_matrix):

    """

    Apply dirichlet boundary conditions to group of indices with potential = value.
    Return dirichlet vector and modified stiffness matrix.
    Employs method of row zeros (produces assymmetric stiffness matrix requiring GMRES).

    :param1 group: array of element indices forming the boundary condition
    :param2 value: array of bc potential values associated with group
    :param5 global_stiffness_matrix: The stiffness matrix
    :returns:
        b_vector -- Vector corresponding to Dirichlet potentials
        global_stiffness_matrix -- The modified problem stiffness matrix (rows corresponding to bc modified)

    """

    b_vector = np.array([0.0] * global_stiffness_matrix.shape[1], dtype = np.float64)

    for i in range(0, len(group)):
        ind = group[i]

        b_vector[ind] = value[i]
        global_stiffness_matrix[ind, ind] = 1
        global_stiffness_matrix[ind, :ind] = 0
        global_stiffness_matrix[ind, (ind+1):] = 0

    return b_vector, global_stiffness_matrix

def neumann_construction_edges(group, fluxes, edge_matrix, mesh_coors, global_stiffness_matrix):

    """

    Implements Neumann boundary conditions at staggered points i.e. fluxes centered on element edges.

    :param1 group: array of element indices forming the boundary condition
    :param2 fluxes: The array of boundary fluxes corresponding to edges
    :param3 edge_matrix: 2D matrix containing edge indices
    :param4 mesh_coors: the coordinates of the mesh
    :param5 global_stiffness_matrix: The stiffness matrix
    :returns:
        g_vector -- Vector corresponding to Neumann fluxes
        global_stiffness_matrix -- The problem stiffness matrix (unchanged but returned for consistency with `dirichlet_construction`)

    """

    g_vector = np.array([0.0] * global_stiffness_matrix.shape[1], dtype = np.float64)

    e1 = edge_matrix[:, 0]
    e2 = edge_matrix[:, 1]

    for bp in group:
        f1 = fluxes[e1 == bp] #returns flux value for edge one associated with node bp
        a1 = np.linalg.norm(mesh_coors[e1[e1 == bp]] - mesh_coors[e2[e1 == bp]])
        f2 = fluxes[e2 == bp] #returns flux value for edge two associated with node bp
        a2 = np.linalg.norm(mesh_coors[e1[e2 == bp]] - mesh_coors[e2[e2 == bp]])
        g_vector[bp] = 1.0/2.0 * (f1 * a1 + f2 * a2)

    return g_vector, global_stiffness_matrix


def neumann_construction_edges_matrix(group, edge_matrix, mesh_coors, global_stiffness_matrix):

    """

    Constructs Neumann flux - load vector transfer matrix at edges i.e. fluxes centered on boundary element centres.
    Allows for straightforward coupling to external BEM solution (with angles).
    Group must be ordered!

    :param1 group: array of element indices forming the boundary condition
    :param2 fluxes: The array of boundary fluxes corresponding to edges
    :param3 edge_matrix: 2D matrix containing edge indices
    :param4 mesh_coors: the coordinates of the mesh
    :param5 global_stiffness_matrix: The stiffness matrix
    :returns:
        g_vector -- Vector corresponding to Neumann fluxes
        global_stiffness_matrix -- The problem stiffness matrix (unchanged but returned for consistency with `dirichlet_construction`)

    """

    neumann_matrix = np.zeros((len(group), len(group)))
    full_neumann_matrix = np.zeros(global_stiffness_matrix.shape)

    for i in range(0, len(group)):

        bp0 = group[(i - 1) % len(group)]
        bp1 = group[i]
        bp2 = group[(i + 1) % len(group)]

        a1 = np.linalg.norm(mesh_coors[bp1] - mesh_coors[bp0])
        a2 = np.linalg.norm(mesh_coors[bp2] - mesh_coors[bp1])

        nmi1 = ((i - 1) % len(group), i)
        nmi2 = ((i) % len(group), i)

        fmi1 = (group[nmi1[0]], group[nmi1[1]])
        fmi2 = (group[nmi2[0]], group[nmi2[1]])

        neumann_matrix[nmi1[0], nmi1[1]] = 1/2.0 * a1
        neumann_matrix[nmi2[0], nmi2[1]] = 1/2.0 * a2

        full_neumann_matrix[fmi1[0], fmi1[1]] = 1/2.0 * a1
        full_neumann_matrix[fmi2[0], fmi2[1]] = 1/2.0 * a2

    return full_neumann_matrix, neumann_matrix, global_stiffness_matrix

def neumann_construction_nodes(group, fluxes, edge_matrix, mesh_coors, global_stiffness_matrix):

    """

    Implements Neumann boundary conditions at nodes i.e. fluxes centered on element nodes.

    :param1 group: array of element indices forming the boundary condition
    :param2 fluxes: The array of boundary fluxes corresponding to edges
    :param3 edge_matrix: 2D matrix containing edge indices
    :param4 mesh_coors: the coordinates of the mesh
    :param5 global_stiffness_matrix: The stiffness matrix
    :returns:
        g_vector -- Vector corresponding to Neumann fluxes
        global_stiffness_matrix -- The problem stiffness matrix (unchanged but returned for consistency with `dirichlet_construction`)

    """

    g_vector = np.array([0.0] * global_stiffness_matrix.shape[1], dtype = np.float64)

    e1 = np.hsplit(edge_matrix, 2)[0].flatten()
    e2 = np.hsplit(edge_matrix, 2)[1].flatten()

    for i in range(0, len(group)):
        bp = group[i]
        f1 = fluxes[i] #returns flux value for basis function central node
        f0 = fluxes[group == e2[e1 == bp][0]] #returns flux value for basis function central node
        f2 = fluxes[group == e1[e2 == bp][0]] #returns flux value for basis function central node

        a0 = np.linalg.norm(mesh_coors[e2[e1 == bp]] - mesh_coors[bp])
        a2 = np.linalg.norm(mesh_coors[e1[e2 == bp]] - mesh_coors[bp])

        g_vector[bp] = a0 * (f1/3.0 + f0/6.0) + a2 * (f1/3.0 + f2/6.0)

    return g_vector, global_stiffness_matrix


#Calculate flux coupling matrix for the linear collocation FEM-BEM system
def N_matrix(group, edge_matrix, mesh_coors, global_stiffness_matrix, alpha):

    """

    Constructs the BEM coupling term for normal flux contributations (i.e. corner handling terms)
    BEM Fluxes and potentials located at nodes contained in A vector (indexes same as group)
    Group must be ordered!

    :param1 group: array of element indices forming the boundary condition
    :param2 edge_matrix: 2D matrix containing edge indices
    :param3 mesh_coors: the coordinates of the mesh
    :param4 global_stiffness_matrix: The stiffness matrix
    :param5 alpha: The internal angles of the external BEM mesh

    :returns:
        full_neumann_flux_matrix -- the flux coupling N matrix in the coupled FEM-BEM equation

    (A + N Ginv F + P) u = b

    """

    neumann_flux_matrix = np.zeros((len(group), len(group)))

    print(len(group))
    print(len(alpha))
    for i in range(0, len(group)):

        bp0 = group[(i - 1) % len(group)]
        bp1 = group[i]
        bp2 = group[(i + 1) % len(group)]

        akm1 = np.linalg.norm(mesh_coors[bp1] - mesh_coors[bp0])
        ak = np.linalg.norm(mesh_coors[bp2] - mesh_coors[bp1])

        alphaj = alpha[i]
        alphajp1 = alpha[(i + 1) % len(group)]

        nmi1 = (i, (i - 1) % len(group))
        nmi2 = (i, i)
        nmi3 = (i, (i + 1) % (len(group)))

        #fmi1 = (group[nmi1[0]], group[nmi1[1]])
        #fmi2 = (group[nmi2[0]], group[nmi2[1]])
        #fmi3 = (group[nmi3[0]], group[nmi3[1]])

        neumann_flux_matrix[nmi1[0], nmi1[1]] += 1/6.0 * akm1
        neumann_flux_matrix[nmi2[0], nmi2[1]] += 1/3.0 * akm1 * np.cos(np.pi - alphaj) + 1/3.0 * ak
        neumann_flux_matrix[nmi3[0], nmi3[1]] += 1/6.0 * ak * np.cos(np.pi - alphajp1)

    return neumann_flux_matrix

#Calculate potential coupling matrix for the linear collocation FEM-BEM system
def P_matrix(group, edge_matrix, mesh_coors, global_stiffness_matrix, alpha):

    """

    Constructs the BEM coupling term for tangential potential contributations (i.e. corner handling terms)
    BEM Fluxes and potentials located at nodes contained in A vector (indexes same as group)
    Group must be ordered!

    :param1 group: array of element indices forming the boundary condition
    :param2 edge_matrix: 2D matrix containing edge indices
    :param3 mesh_coors: the coordinates of the mesh
    :param4 global_stiffness_matrix: The stiffness matrix
    :param5 alpha: The internal angles of the external BEM mesh

    :returns:
        full_neumann_pot_matrix -- the potential coupling P matrix in the coupled FEM-BEM equation

    (A + N Ginv F + P) u = b

    """

    neumann_pot_matrix = np.zeros((len(group), len(group)))

    for i in range(0, len(group)):

        bpm1 = group[(i - 1) % len(group)]
        bp0 = group[i]
        bp1 = group[(i + 1) % len(group)]
        bp2 = group[(i + 2) % len(group)]

        akm1 = np.linalg.norm(mesh_coors[bp0] - mesh_coors[bpm1])
        ak = np.linalg.norm(mesh_coors[bp1] - mesh_coors[bp0])
        akp1 = np.linalg.norm(mesh_coors[bp2] - mesh_coors[bp1])

        alphaj = alpha[i]
        alphajp1 = alpha[(i + 1) % len(group)]

        nmi1 = (i, i)
        nmi2 = (i, (i + 1) % len(group))
        nmi3 = (i, (i + 2) % len(group))

        neumann_pot_matrix[nmi1[0], nmi1[1]] += 1/3.0 * akm1/ak * np.sin(np.pi - alphaj)
        neumann_pot_matrix[nmi2[0], nmi2[1]] += 1/6.0 * ak/akp1 * np.sin(np.pi - alphajp1) - 1/3.0 * akm1/ak * np.sin(np.pi - alphaj)
        neumann_pot_matrix[nmi3[0], nmi3[1]] += - 1/6.0 * ak/akp1 * np.sin(np.pi - alphajp1)

    return neumann_pot_matrix


def neumann_construction_nodes_matrix(group, edge_matrix, mesh_coors, global_stiffness_matrix, connectivity_matrix):

    """

    Constructs Neumann flux - load vector transfer matrix at nodes i.e. fluxes centered on boundary element corners.
    Allows for straightforward coupling to external BEM solution (with angles).
    Group must be ordered!

    :param1 group: array of element indices forming the boundary condition
    :param2 fluxes: The array of boundary fluxes corresponding to edges
    :param3 edge_matrix: 2D matrix containing edge indices
    :param4 mesh_coors: the coordinates of the mesh
    :param5 global_stiffness_matrix: The stiffness matrix
    :returns:
        g_vector -- Vector corresponding to Neumann fluxes
        global_stiffness_matrix -- The problem stiffness matrix (unchanged but returned for consistency with `dirichlet_construction`)

    """

    neumann_matrix = np.zeros((len(group), len(group)))
    full_neumann_matrix = np.zeros(global_stiffness_matrix.shape)

    e1 = np.hsplit(edge_matrix, 2)[0].flatten()
    e2 = np.hsplit(edge_matrix, 2)[1].flatten()

    for i in range(0, len(group)):
        bp = group[i]

        a0 = np.linalg.norm(mesh_coors[e2[e1 == bp]] - mesh_coors[bp])
        a2 = np.linalg.norm(mesh_coors[e1[e2 == bp]] - mesh_coors[bp])

        nmi0 = ((i - 1) % len(group), i)
        nmi1 = ((i) % len(group), i)
        nmi2 = ((i + 1) % len(group), i)

        fmi0 = (group[nmi0[0]], group[nmi0[1]])
        fmi1 = (group[nmi1[0]], group[nmi1[1]])
        fmi2 = (group[nmi2[0]], group[nmi2[1]])

        neumann_matrix[nmi0[0], nmi0[1]] = 1/6.0 * a0
        neumann_matrix[nmi1[0], nmi1[1]] = 1/3.0 * a0 + 1/3.0 * a2
        neumann_matrix[nmi2[0], nmi2[1]] = 1/6.0 * a2

        full_neumann_matrix[fmi0[0], fmi0[1]] = 1/6.0 * a0
        full_neumann_matrix[fmi1[0], fmi1[1]] = 1/3.0 * a0 + 1/3.0 * a2
        full_neumann_matrix[fmi2[0], fmi2[1]] = 1/6.0 * a2


    return full_neumann_matrix, neumann_matrix, global_stiffness_matrix


def expand_n_matrix(mod_mat, group, mesh_coors, stiffness_matrix):

    """

    Expands BEM matrices (inserts zeros) for use in the total BEM-FEM coupling equation
    i.e. Takes matrix defined over boundary nodes and inserts zeros to define matrix over all FEM domain nodes

    :param1 mod_mat: The stiffness matrix modifier (from BEM coupling)
    :param2 group: The ordered group of boundary node indexes used in the BEM coupling
    :param3 mesh_coors: The FEM mesh nodes
    :param4 stiffness_matrix: The problem's stiffness matrix
    :returns: The expanded coupled surface-flux matrix

    """

    exp_n_matrix = np.zeros(stiffness_matrix.shape)

    for i in range(0, mod_mat.shape[0]):
        for j in range(0, mod_mat.shape[1]):
            exp_n_matrix[group[i], group[j]] = mod_mat[i, j]

    return exp_n_matrix

def expand_matrix(mod_mat, group, d0, d1):

    """

    Expands BEM matrices (inserts zeros) for use in the total BEM-FEM coupling equation
    i.e. Takes matrix defined over boundary nodes and inserts zeros to define matrix over all FEM domain nodes

    :param1 mod_mat: The stiffness matrix modifier (from BEM coupling)
    :param2 group: The ordered group of boundary node indexes used in the BEM coupling
    :param3 mesh_coors: The FEM mesh nodes
    :param4 stiffness_matrix: The problem's stiffness matrix
    :returns: The expanded coupled surface-flux matrix

    """

    expand_matrix = np.zeros((d0, d1))

    for i in range(0, mod_mat.shape[0]):
        for j in range(0, mod_mat.shape[1]):
            expand_matrix[group[i], group[j]] = mod_mat[i, j]

    return expand_matrix

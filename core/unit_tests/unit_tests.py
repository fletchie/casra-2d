#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tuesday 13th Nov 2018

Contains unit tests for continuum simulation components and modules

@author: charlie
"""

import numpy as np

def cbem_circle():
    """

    Unit test for constant BEM surface field calculation for a circle geometry.
    Circle radius, potential, and number of panels read as user input.

    """

    print("Running constant BEM unit test for circle geometry")
    R = float(input("enter radius "))
    u = - float(input("enter potential "))
    n = float(input("enter number of panels "))

    x = []
    y = []

    for ang in np.linspace(0.0, 2.0 * np.pi, n):
        x.append(R * np.cos(ang))
        y.append(R * np.sin(ang))

    import core.boundary_element_library.BEM as BEM
    import core.boundary_element_library.BEM_matrix_construction as cBEM

    A, B, C = BEM.prepare_BE(np.stack((x, y), axis = -1), sh = 0, l = 0.0)

    F, G = BEM.construct_matrices(C, A, B)
    F2, G2 = cBEM.construct_matrices(C, A, B)

    Ginv = np.linalg.inv(G)
    Ginv2 = np.linalg.inv(G2)

    print("Calculated field (numpy): " + str(np.mean(np.matmul(np.matmul(Ginv, F), np.array([u] * len(A))))))
    print("Calculated field (cython): " + str(np.mean(np.matmul(np.matmul(Ginv2, F2), np.array([u] * len(A))))))
    print("Theoretical value: " + str(-u/(np.log(R) * R)))

def lbem_circle():

    """
    Unit test for linear BEM surface field calculation for a circle geometry.
    Circle radius, potential, and number of panels read as user input.

    """

    print("Running linear BEM unit test for circle geometry")
    R = float(input("enter radius "))
    u = - float(input("enter potential "))
    n = float(input("enter number of panels "))

    x = []
    y = []

    for ang in np.linspace(0.0, 2.0 * np.pi, n):
        x.append(R * np.cos(ang))
        y.append(R * np.sin(ang))

    import core.boundary_element_library.linear_BEM as lBEM

    A, B, C = lBEM.prepare_BE(np.stack((x, y), axis = -1), sh = 0, l = 0.0)

    F, G, alpha = lBEM.fast_construct_matrices(C, C, A, B)
    #F2, G2, alpha2 = lBEM.construct_matrices(C, C, A, B)
    Ginv = np.linalg.inv(G)
    #Ginv2 = np.linalg.inv(G2)

    BF = np.matmul(np.matmul(Ginv, F), np.array([u] * len(A)))
    #BF2 = np.matmul(np.matmul(Ginv2, F2), np.array([u] * len(A)))

    #print("Calculated field (python): " + str(np.mean(BF2)))
    print("Calculated field (cython): " + str(np.mean(BF)))
    print("Theoretical value: " + str(-u/(np.log(R) * R)))

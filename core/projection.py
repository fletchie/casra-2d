# -*- coding: utf-8 -*-
"""

Methods handling the ion projection steps

@author: Charles
"""

import numpy as np
import scipy as sp
import math
import random
import copy
import sys
import core.boundary_element_library.BEM_matrix_construction as BEM
import core.boundary_element_library.BEM as pBEM

def ion_projection(surface_velocity, BP, BF, A, B, C, surface_normal, detector, phase_map, sys_time, fmin = 0.3, fmax = 0.7, projection_fraction = 0.3, dl = 0.75, domain_scale = 1.0):

    """

    Function for projecting at equidistant points over the tip surface. Returns trajectories for program output.

    :param2 surface_velocity: The level-set boundary velocity -- possible future use for dynamically updating projection
    :param3 BP: The boundary potential values
    :param4 BF: The calculated boundary flux
    :param5 A: The lefthand corner of boundary elements
    :param6 B: The righthand corner of boundary elements
    :param7 C: The centre of boundary elements
    :param8 surface_normal: The boundary element normals
    :param9 detector: The detector instance
    :param10 sys_time: The system time (for timestamp)
    :param11 fmin: The minimum parameterised surface projection position
    :param12 fmax: The maximum parameterised surface projection position
    :param13 dl: The surface distance step after which to project
    :returns: array of ion trajectories

    """

    trajectories = []
    end_points = []
    hit_phases = []
    traj_phases = []
    projection_coords = A[A[:, 1] > np.max(A[:, 1]) * (1.0 - projection_fraction)]
    projection_normal = surface_normal[A[:, 1] > np.max(A[:, 1]) * (1.0 - projection_fraction)]

    #project from set distances along tip (boundary elements can vary in length)
    l = 0.0
    i = 0
    p = projection_coords[0]
    proj = True

    #calculate boundary element normals and tangents
    R = np.array([[0.0, -1.0], [1.0, 0.0]])

    Ne = np.matmul(R, (B - A).T).T
    Te = (B - A)

    Ne = Ne/(np.linalg.norm(Ne, axis = 1))[:, None]
    Te = Te/(np.linalg.norm(Te, axis = 1))[:, None]

    Ne = np.ascontiguousarray(Ne)
    Te = np.ascontiguousarray(Te)
    B = np.ascontiguousarray(B)
    A = np.ascontiguousarray(A)
    BP = np.ascontiguousarray(BP)
    BF = np.ascontiguousarray(BF)

    while proj == True:
        if l + np.linalg.norm(projection_coords[i + 1] - p) < dl:
            l += np.linalg.norm(projection_coords[i + 1] - p)
            p = projection_coords[i + 1]
            i += 1
        else:
            #calculate position along element at which previous projection separation distance dl is reached
            res = sp.optimize.root(dist_min, 0.5, args = (p/domain_scale, projection_coords[i + 1]/domain_scale, (dl - l)/domain_scale), method = 'broyden1')
            a = res['x']
            p = p * (1.0 - a) + projection_coords[i + 1] * a

            l = 0.0

            if a < 0.5:
                n = projection_normal[i - 1] * (0.5 - a) + projection_normal[i] * (a + 0.5)
            else:
                n = projection_normal[i] * (1.5 - a) + projection_normal[i + 1] * (a - 0.5)

            particle = ion(p, np.array([0.0, 0.0]), BF[i], detector.projection_space, sys_time, detector, q = + 1e-14, m = 1)

            #append initial ion phase
            if phase_map[int(round(p[0]/domain_scale)), int(round(p[1]/domain_scale))] == 0:
                bp = particle.p + n/np.linalg.norm(n)  * 1.0 * domain_scale
            else:
                bp = particle.p

            traj_phases.append(int(phase_map[int(round(bp[0]/domain_scale)), int(round(bp[1]/domain_scale))]))
            #integrate ion trajectory
            particle.p = particle.p + n/np.linalg.norm(n)  * 0.1 * domain_scale
            particle.iterate(0.02, BP, BF, Ne, Te, A, B, C)

            trajectories.append(np.array(particle.trajectory)[:, 0:2]/domain_scale)

            if hasattr(particle, 'hit'):
                end_points.append(particle.hit)
                hit_phases.append(int(phase_map[int(round(bp[0]/domain_scale)), int(round(bp[1]/domain_scale))]))

            del particle

        if i == len(projection_coords) - 1:
            proj = False

    return trajectories, end_points, traj_phases, hit_phases

def dist_min(x, a, b, ddl):
    #function to determine equally space ion source positions over BE
    return ddl - np.linalg.norm((b - a) * x)

def derive_mapping(surface_velocity, BP, BF, A, B, C, surface_normal, detector, phase_map, sys_time, fmin = 0.3, fmax = 0.7, projection_fraction = 0.3, mapping_res = 1.0, domain_scale = 1.0):

    """

    Function for deriving the tip-detector surface mapping.

    :param2 surface_velocity: The level-set boundary velocity -- possible future use for dynamically updating projection
    :param3 BP: The boundary potential values
    :param4 BF: The calculated boundary flux
    :param5 A: The lefthand corner of boundary elements
    :param6 B: The righthand corner of boundary elements
    :param7 C: The centre of boundary elements
    :param8 surface_normal: The boundary element normals
    :param9 detector: The detector instance
    :param10 sys_time: The system time (for timestamp)
    :param11 fmin: The minimum parameterised surface projection position
    :param12 fmax: The maximum parameterised surface projection position
    :returns: The ion projection mapping
        image_space -- The coordinates within the sample space
        det_space -- The coordinates within the detector space

    """

    print("Calculate projection mapping...")

    sample_space = []
    det_space = []
    phase = []

    projection_coords = C[C[:, 1] > np.max(C[:, 1]) * (1.0 - projection_fraction)]
    projection_normal = surface_normal[C[:, 1] > np.max(C[:, 1]) * (1.0 - projection_fraction)]

    ion_phase = 1

    #calculate boundary element normals and tangents
    R = np.array([[0.0, -1.0], [1.0, 0.0]])

    Ne = np.matmul(R, (B - A).T).T
    Te = (B - A)

    Ne = Ne/(np.linalg.norm(Ne, axis = 1))[:, None]
    Te = Te/(np.linalg.norm(Te, axis = 1))[:, None]

    Ne = np.ascontiguousarray(Ne)
    Te = np.ascontiguousarray(Te)
    B = np.ascontiguousarray(B)
    A = np.ascontiguousarray(A)
    BP = np.ascontiguousarray(BP)
    BF = np.ascontiguousarray(BF)

    trajectories = []

    for i in range(0, len(projection_coords)):
        #Source ion per BE (from centre)
        #can switch to corner elements (A) as described in paper - for this modify projection_coords and projection_normal
        p = np.array([projection_coords[i, 0], projection_coords[i, 1]])
        n = np.array([projection_normal[i, 0], projection_normal[i, 1]])

        ion_phase = phase_map[int(projection_coords[i, 0]/domain_scale), int(projection_coords[i, 1]/domain_scale)]
        particle = ion(p, np.array([0.0, 0.0]), BF[i], detector.projection_space, sys_time, detector, q = + 1e-14, m = 1.0)
        particle.p = particle.p + n/np.linalg.norm(n) * 0.1 * domain_scale
        particle.iterate(0.02, BP, BF, Ne, Te, A, B, C)
        trajectories.append(np.array(particle.trajectory)[:, 0:2]/domain_scale)

        if hasattr(particle, 'hit'):
            det_hit_loc = copy.deepcopy(particle.hit)
            sample_space.append(projection_coords[i]/domain_scale)
            det_space.append(det_hit_loc)
            phase.append(ion_phase)

        #delete the particle object instance after projection
        del particle

    trajectories = np.array(trajectories)
    print("Mapping calculation complete")

    return {'sample_space': sample_space, 'detector_space': det_space, 'ion_phases': phase, 'timestamp': sys_time}, trajectories

class ion:

    def __init__(self, p, v, bf, domain, sys_time, detector, q = + 1.0, m = 1.0):

        """

        Method for initialising ion instance

        :param1 self: The ion instance
        :param2 p: The initial ion position
        :param3 v: Initial ion velocity
        :param4 bf: The local boundary element flux
        :param5 domain: The projection domain
        :param6 sys_time: The system time (timestamp)
        :param7 detector: The detector object
        :param8 q: Ion charge
        :param9 m: Ion mass
        :returns: 0

        """

        self.q = q
        self.p = np.array([p[0], p[1]]) #positions need redefining
        self.prev_pos = np.array([p[0], p[1]])
        self.v = v
        self.m = m
        self.system_time = sys_time

        self.domain = domain
        self.active = True
        self.detector = detector

        self.field_at_origin = bf
        self.first_it_field = [False]

        #origin (boundary element)
        self.origin = p
        self.detector_impact = None

    #newton trajectory integrator
    def newton(self, t, y, m, q, BP, BF, Ne, Te, A, B):

        """

        One step of the ion trajectory integrator

        :param1 self: The ion instance
        :param2 t: The current projection time
        :param3 y: The phase space vector
        :param4 m: ion mass
        :param5 q: ion charge
        :param6 BP: The tip surface potentials
        :param7 BF: The calculated tip surface electric fluxes
        :param8 A: boundary element normals
        :param9 B: boundary element tangents
        :param10 A: The lefthand boundary element corners
        :param11 B: The righthand boundary element corners
        :returns: The phase space gradient vector

        """

        px, py, vx, vy = y
        Ex, Ey = BEM.calculate_field(px, py, BP, BF, Ne, Te, A, B)
        #Ex, Ey = pBEM.v_calculate_field(py, px, BP, BF, Ne, Te, A, B)

        dydt = [vx, vy, q*Ex/m, q*Ey/m]
        return dydt

    #dynamic breaker for trajectory integrator
    def solout(self, t, y):

        """

        dynamic breaker for the trajectory integrator. Trajectory integration terminated
        on ion intersecting detector or exiting domain

        :param1 t: The ion integration time
        :param2 y: The phase space vector
        :returns:
            -1 -- If termination condition reach
            0 -- If integration should continue

        """

        self.time_step.append(t)
        self.trajectory.append([*y])
        if self.in_domain(y) == False:
            return -1
        elif self.detector_intersect(y, self.detector) == True:
            return -1

        else:
            return 0

    def iterate(self, dt, BP, BF, Ne, Te, A, B, C):

        """

        Performs ion trajectory integration until termination condition is reached (see `solout`)

        :param1 self: The ion instance
        :param2 dt: The initial timestep value
        :param3 BP: The tip surface boundary element potentials
        :param4 BF: The tip surface boundary element fluxes
        :param5 A: The lefthand corner of the boundary elements
        :param6 B: The righthand corner of the boundary elements
        :param7 C: The boundary element centres
        :returns: 0

        """
        self.prev_pos = self.p
        self.time_step = []
        self.trajectory = []
        y0 = [self.p[0], self.p[1], 0.0, 0.0]
        self.trajectory.append(y0)
        integrator = sp.integrate.ode(self.newton).set_integrator('dopri5', rtol=1e-10, atol=1e-10)
        integrator.set_solout(self.solout)
        integrator.set_initial_value(y0, dt).set_f_params(self.m, self.q, BP, BF, Ne, Te, A, B)
        integrator.integrate(math.inf)

    def in_domain(self, y):

        """

        Checks if ion still remains within the integration domain

        :param1 self: The ion instance. Contains information regarding integration domain.
        :param2 y: The phase space vector (containing position information)
        :returns:
            False -- Ion exited domain
            True -- Ion romains in domain

        """
        if (self.domain[0] > y[0]) or (self.domain[1] < y[0]) or (self.domain[2] > y[1]) or (self.domain[3] < y[1]):
            return False
        else:
            return True

    def detector_intersect(self, y, detector):

        """

        Method for checking for detector intersection.
        If detector intersection has occured then the intersection position, i.e. event location, is recorded.

        :param1 self: The ion instance
        :param2 y: The phase space vector (contains ion position information)
        :param3 detector: The detector instance
        :returns:
            True -- Detection event has occurred
            False -- No detection event has occurred (yet)

        """

        if (detector.p0y > self.trajectory[-2][0:2][1]) * (detector.p1y < self.trajectory[-1][0:2][1]):
            #fit polynomial to final ion positions and interpolate
            x = [self.trajectory[-3][0], self.trajectory[-2][0], self.trajectory[-1][0]]
            y = [self.trajectory[-3][1], self.trajectory[-2][1], self.trajectory[-1][1]]

            param = np.polyfit(y, x, deg = 2)
            cx = np.polyval(param, detector.p0y)

            #Determine detector impact coordinate via interpolations
            self.trajectory[-1][0:2] = [cx, detector.p0y]

            #convert to detector coordinate
            self.hit = (cx - detector.p0x)/(detector.p1x - detector.p0x)
            return True
        else:
            return False


class detector:
    def __init__(self, width, angle, cx, cy, projection_space = [-21000.0, 21000.0, 0.0, 40000.0]):

        """
        Initialisation method for the detector object.

        Detector is currently fixed at a constant x position

        :param1 self: The detector object instance
        :param2 width: The detector width
        :param3 cx: The detector centre x-coordinate
        :param4 cy: The detector centre y-coordinate
        :param5 projection_space: The space over which ions will be projected
        :returns: 0

        """

        self.width = width
        self.cx = cx
        self.cy = cy
        self.projection_space = projection_space

        self.p0x = self.cy - 1/2.0*self.width
        self.p1x = self.cy + 1/2.0*self.width
        self.p0y = self.cx
        self.p1y = self.cx

        a = (self.p1y - self.p0y)/(self.p1x - self.p0x)
        b = -1.0
        c = self.p1y - a * (self.p1x)

        self.sign = lambda x: a*x[0] + b*x[1] + c

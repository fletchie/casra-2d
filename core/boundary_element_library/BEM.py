#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 30 13:52:24 2017

Functions for calculating electric field via the boundary element method

@author: Charles Fletcher
"""

import numpy as np

##construct flux integral over boundary elemnt influence coefficient - vectorised
def Fij(n, sa, sb, same):

    """
    Vectorised method calculating the singular F matrix components

    :param1 n: The normal distance components from boundary element i centre node to boundary element j
    :param2 sa: The parallel distance components from boundary element i centre node to boundary element j lefthand corner A
    :param3 sb: The parallel distance components from boundary element i centre node to boundary element j righthand corner B
    :returns:
        value -- The matrix values

    """

    value = -1.0/(2.0 * np.pi) * (np.arctan2(sb, abs(n)) - np.arctan2(sa, abs(n))) * (2.0 * (n >= 0.0) - 1.0) * (same == False)

    return value

##construct potential integral over boundary elemnt influence coefficient - vectorised
def Gij(n, sa, sb, same):

    """
    Vectorised method calculating the singular G matrix components

    :param1 n: The normal distance components from boundary element i centre node to boundary element j
    :param2 sa: The parallel distance components from boundary element i centre node to boundary element j lefthand corner A
    :param3 sb: The parallel distance components from boundary element i centre node to boundary element j righthand corner B
    :returns:
        value -- The matrix values

    """

    pos = 1.0/2.0 * sb * (np.log(n**2.0 + sb**2.0) - 2.0) + abs(n)*np.arctan2(sb, abs(n))
    neg = 1.0/2.0 * sa * (np.log(n**2.0 + sa**2.0) - 2.0) + abs(n)*np.arctan2(sa, abs(n))

    v1 = -1.0/(2.0 * np.pi) * (pos - neg) * (same == False)

    v2 = -1.0/(4.0 * np.pi) * (sb * (np.log(sb**2.0) - 2.0) - sa * (np.log(sa**2.0) - 2.0)) * (same == True)

    v2[np.isnan(v2) == True] = 0.0

    value = v1 + v2

    return value

###hypersingular integral coefficients
def Hij(n, sa, sb, nky, tky, nkx):

    """
    Vectorised method calculating the hypersingular H matrix components

    :param1 n: The normal distance component from boundary element i centre node to boundary element j
    :param2 sa: The parallel distance component from boundary element i centre node to boundary element j lefthand corner A
    :param3 sb: The parallel distance component from boundary element i centre node to boundary element j righthand corner B
    :param4 nky: The boundary element normal vectors (dim 2)
    :param5 tky: The boundary element tangental vectors (dim 2)
    :param5 nkx: The basis vector to resolve the calculated electric field along
    :returns: The matrix value

    """

    c1 = (sb/(n**2.0 + sb**2.0) - sa/(n**2.0 + sa**2.0)) * np.sum(nky * nkx, axis = 1)
    c2 = - (n/(n**2.0 + sb**2.0) - n/(n**2.0 + sa**2.0)) * np.sum(tky * nkx, axis = 1)
    return 1.0/(2.0 * np.pi) * (c1 + c2)

def Kij(n, sa, sb, nky, tky, nkx):

    """
    Vectorised method calculating the hypersingular K matrix components

    :param1 n: The normal distance component from boundary element i centre node to boundary element j
    :param2 sa: The parallel distance component from boundary element i centre node to boundary element j lefthand corner A
    :param3 sb: The parallel distance component from boundary element i centre node to boundary element j righthand corner B
    :param4 nky: The boundary element normal vectors (dim 2)
    :param5 tky: The boundary element tangental vectors (dim 2)
    :param5 nkx: The basis vector to resolve the calculated electric field along
    :returns: The matrix value

    """

    c1 = - (2.0 * (n >= 0.0) - 1.0) * (np.arctan2(sb, abs(n)) - np.arctan2(sa, abs(n))) * np.sum(nky * nkx, axis = 1)
    c2 = - 0.5 * np.log((n**2.0 + sb**2.0)/(n**2.0 + sa**2.0)) * np.sum(tky * nkx, axis = 1)
    return 1.0/(2.0 * np.pi) * (c1 + c2)

def construct_matrices(C, A, B):

    """
    Vectorised function for constructing BEM singular matrices.

    :param2 C: The central node of boundary elements
    :param3 A: THe lefthand corner of boundary elements
    :param4 B: The righthand corner of boundary elements
    :returns: The matrices

    """

    C_E = np.repeat(C, len(C), axis = 0)
    #ext = np.repeat(exterior, len(exterior), axis = 0)

    A_E = np.tile(A.T, len(C)).T
    B_E = np.tile(B.T, len(C)).T
    same = np.array([False] * len(C_E))
    same[::len(C) + 1] = True

    #transform to local polar coordinates
    n, sA, sB = coordinate_transform(C_E, A_E, B_E)

    #calculate array elements via numpy vectorisation
    F = Fij(n, sA, sB, same) #- 0.5 * same
    G = Gij(n, sA, sB, same)

    F = F.reshape((len(C), len(C)))

    for i in range(0, len(F)):
       F[i, i] = -np.sum(F[i, :]) + 1.0

    return F, G.reshape((len(C), len(C)))

#a vectorised form of coordinate transform
#transform boundary element cartesian coordinates to local polar coordinates
def coordinate_transform(O, A, B):

    """
    The vectorised coordinate transformation for boundary elements - local cartesian to polars.

    :param1 O: The point relative to the boundary element
    :param2 A: The lefthand boundary element corner
    :param3 B: The righthand boundary element corner
    :returns:
        n -- The normal distance component from O to the boundary element
        sa -- The parallel distance component from O to A
        sb -- The parallel distance component from O to B

    """

    AB = B - A
    OA = A - O
    OB = B - O

    norm = np.linalg.norm(AB, axis = 1)

    n = np.cross(-OA, AB, axisa = 1, axisb = 1, axisc = 1)/norm

    sA = np.sum(np.multiply(OA, AB), axis = 1)/norm
    sB = np.sum(np.multiply(OB, AB), axis = 1)/norm

    return n, sA, sB

#calculate potential at point x, y (within domain)
def calculate_potential(x, y, BP, BF, A, B):

    """
    The unvectorised method for calculating the potential at domain point (x, y)

    :param1 x: domain x coordinate
    :param2 y: domain y coordinate
    :param3 BP: The electric potentials defined at boundary element centre nodes C
    :param4 BF: The electric fluxes (calculated via `boundary_field`) defined at boundary element centre nodes C
    :param5 A: The lefthand boundary element corners
    :param6 B: The righthand boundary element corners
    :returns:
        pot -- The electric potential at domain point (x, y)

    """

    n, sa, sb = coordinate_transform([x, y], A, B)

    #modifier for if domain point lies on boundary
    pot1 = np.sum(Fij(n, sa, sb, np.array([False]) * len(BP)) * BP)
    pot2 = np.sum(Gij(n, sa, sb, np.array([False] * len(BP))) * BF)

    #print(pot1)

    return - pot1 + pot2


#A vectorised form of the electric field calculation at x, y domain point
def calculate_field(x, y, BP, BF, Ne, Te, A, B):

    """
    The vectorised method for calculating the electric field vector at domain position (x, y)

    :param1 x: domain x coordinate
    :param2 y: domain y coordinate
    :param3 BP: The electric potentials at boundary nodes C
    :param4 BF: The electric fluxes at boundary nodes C (calculated via `boundary_field`)
    :param5 A: The lefthand boundary element corners
    :param6 B: The righthand boundary element corners
    :returns:
        field_x -- The x flux component at (x, y)
        field_y -- The y flux component at (x, y)
    """

    #transform to local polar coordinates
    n, sA, sB = coordinate_transform([x, y], A, B)

    field_x = np.sum(Hij(n, sA, sB, Ne, Te, np.array([1.0, 0.0])) * BP) - np.sum(Kij(n, sA, sB, Ne, Te, np.array([1.0, 0.0])) * BF)
    field_y = np.sum(Hij(n, sA, sB, Ne, Te, np.array([0.0, 1.0])) * BP) - np.sum(Kij(n, sA, sB, Ne, Te, np.array([0.0, 1.0])) * BF)

    return - field_x, - field_y


def initial_shank_vector(boundary, ang_dist = 10):

    """

    estimate the initial shank angle vectors from a fixed number of boundary elements

    :param1 boundary: The inputted sample boundary extracted via marching squares.
    :param2 el_count: The number of elements from which the shank angle vectors are estimated.
    :returns:
        shank_vec_l -- the lefthand normalised shank angle vector
        shank_vec_r -- the righthand normalised shank angle vector

    """

    shank_vec_l = (boundary[0] - boundary[ang_dist - 1])/np.linalg.norm(boundary[0] - boundary[ang_dist - 1])
    shank_vec_r = (boundary[-1] - boundary[-ang_dist])/np.linalg.norm(boundary[-1] - boundary[-ang_dist])

    return shank_vec_l, shank_vec_r

#construct boundary elements from traced boundary
def prepare_BE(boundary, shank_vec_l = None, shank_vec_r = None, sh = 0, l = 0.0):

    """

    Prepare boundary elements from boundary extracted via marching squares.
    Adds on sh-shank elements of length l.
    Returns a periodic boundary such that the domain is closed i.e. boundary
    element over tip base.

    :param1 boundary: The tip boundary extracted from the level-set field via marching squares
    :param2 sh: The number of boundary elements making up the shank on each side
    :param3 l: The length of the shank boundary elements
    :returns:
        A -- The lefthand corners of the created boundary elements
        B -- The righthand corners of the created boundary elements
        C -- The central nodes of the created boundary elements

    """

    #solve boundary elements off grid cell center
    A = boundary[:-1]
    B = boundary[1:]

    #construct tip shank - number of elements used to estimate shank length
    if (sh > 0) and (l > 0.0):
        shank_vec_l = l * shank_vec_l
        shank_vec_r = l * shank_vec_r

        ext_A_l = np.outer(np.linspace(sh, 1, sh), shank_vec_l) + A[0]
        ext_B_l = np.outer(np.linspace(sh-1, 0, sh), shank_vec_l) + A[0]

        ext_A_r = np.outer(np.linspace(0, sh-1, sh), shank_vec_r) + B[-1]
        ext_B_r = np.outer(np.linspace(1, sh, sh), shank_vec_r) + B[-1]

        ext_A_l = ext_A_l[1:]

        d = (ext_B_r[-1][1] - ext_A_l[0][1])/shank_vec_l[1]

        ext_A_l = np.vstack((d * shank_vec_l + ext_A_l[0], ext_A_l))

        A = np.vstack((ext_A_l, A, ext_A_r))
        B = np.vstack((ext_B_l, B, ext_B_r))

        B = np.vstack((B, A[0]))
        A = np.vstack((A, B[-2]))

    C = (B + A)/2.0
    return A, B, C

def prepare_BE_unbounded(boundary, sh = 0, l = 0.0):

    """
    Prepare boundary elements from boundary extracted via marching squares.
    Adds on sh-shank elements of length l.
    Returns a periodic boundary such that the domain is closed i.e. boundary
    element over tip base.

    :param1 boundary: The tip boundary extracted from the level-set field via marching squares
    :param2 sh: The number of boundary elements making up the shank on each side
    :param3 l: The length of the shank boundary elements
    :returns:
        A -- The lefthand corners of the created boundary elements
        B -- The righthand corners of the created boundary elements
        C -- The central nodes of the created boundary elements

    """

    #solve boundary elements off grid cell center
    A = boundary[:-1]
    B = boundary[1:]

    #construct tip shank
    ang_dist = 30

    if (sh > 0) and (l > 0.0):
        shank_vec_l = l * (A[0] - A[ang_dist - 1])/ang_dist
        shank_vec_r = l * (B[-1] - B[-ang_dist])/ang_dist

        ext_A_l = np.outer(np.linspace(sh, 1, sh), shank_vec_l) + A[0]
        ext_B_l = np.outer(np.linspace(sh-1, 0, sh), shank_vec_l) + A[0]

        ext_A_r = np.outer(np.linspace(0, sh-1, sh), shank_vec_r) + B[-1]
        ext_B_r = np.outer(np.linspace(1, sh, sh), shank_vec_r) + B[-1]

        A = np.vstack((ext_A_l, A, ext_A_r))
        B = np.vstack((ext_B_l, B, ext_B_r))

    C = (B + A)/2.0
    return A, B, C

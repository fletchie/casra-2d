#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 11 11:56:21 2018

Output class for rendering geometric features i.e. different phases

@author: charlie
"""

class feature:
    def __init__(self, f_type, color, pattern, fmax, fmin):
        self.f_type = f_type
        self.color = color
        self.pattern = pattern
        self.fmax = fmax
        self.fmin = fmin
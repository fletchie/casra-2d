#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sept 27 13:52:24 2018

The cython module for constructing the FEM stiffness matrix.

@author: Charles Fletcher
"""

import cython

import numpy as np
cimport numpy as np

cdef extern from "math.h":
    double fabs(double v)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)

#Identify and return first FEM mesh nodes within particular circular region of mesh
def dirichlet_bc(double[::1, :] mesh_coors, long[::1, :] connectivity_matrix, double cx, double cy, double radius):

    """
    Identify FEM mesh nodes within particular circular region of mesh.
    Used to initialise the tip base fixed potential Dirichlet boundary condition.

    :param1 self: The level-set field object instance
    :param2 mesh_coors: The constructed FEM mesh coordinates
    :param3 connectivity_matrix: The FEM mesh connectivity_matrix
    :param4 centre: The centre for the circular region
    :param5 radus: The radius for the circular region
    :param6 exclusion: The indices of mesh nodes to exclude from the region i.e. mesh boundaries
    :returns: List of indices for mesh circular region/group corresponding to nodes in mesh_coors

    """

    group = []

    cdef Py_ssize_t x = connectivity_matrix.shape[0]
    cdef Py_ssize_t y = connectivity_matrix.shape[1]

    cdef Py_ssize_t r, e

    cdef int el

    for r in range(0, x):
        for e in range(0, y):
            el = connectivity_matrix[r, e]
            if ( (mesh_coors[el, 0] - cx)**2.0 + (mesh_coors[el, 1] - cy)**2.0 ) < radius**2.0:
                return el

def expand_matrix(double[:, ::1] mod_mat, long[::1] group, int d0, int d1):

    """

    Expands BEM matrices (inserts zeros) for use in the total BEM-FEM coupling equation
    i.e. Takes matrix defined over boundary nodes and inserts zeros to define matrix over all FEM domain nodes

    :param1 mod_mat: The stiffness matrix modifier (from BEM coupling)
    :param2 group: The ordered group of boundary node indexes used in the BEM coupling
    :param3 mesh_coors: The FEM mesh nodes
    :param4 stiffness_matrix: The problem's stiffness matrix
    :returns: The expanded coupled surface-flux matrix

    """
    cdef Py_ssize_t x = mod_mat.shape[0]
    cdef Py_ssize_t y = mod_mat.shape[1]

    expand_matrix = np.zeros((d0, d1), dtype = np.double)
    cdef double[:, ::1] expand_matrix_view = expand_matrix

    cdef Py_ssize_t i, j

    for i in range(0, x):
        for j in range(0, y):
            expand_matrix_view[group[i], group[j]] = mod_mat[i, j]

    return expand_matrix


def construct_global_stiffness_matrix(double[::1, :] mesh_coors, long[::1, :] connectivity_matrix, double[::1] c_constant):

  cdef Py_ssize_t n_nodes = mesh_coors.shape[0]
  cdef Py_ssize_t n_el = connectivity_matrix.shape[0]

  stiffness_matrix = np.zeros((n_nodes, n_nodes), dtype = np.double)
  cdef double[:, ::1] stiffness_matrix_view = stiffness_matrix

  cdef Py_ssize_t k, a, b
  cdef int i, j

  cdef long x0, x1, x2

  cdef (double, double, double, double, double, double, double, double, double) element_solution

  for k in range(0, n_el):

      x0 = connectivity_matrix[k, 0]
      x1 = connectivity_matrix[k, 1]
      x2 = connectivity_matrix[k, 2]

      element_solution = triangle_stiffness_matrix_laplace(mesh_coors[x0, 0], mesh_coors[x0, 1], mesh_coors[x1, 0], mesh_coors[x1, 1], mesh_coors[x2, 0], mesh_coors[x2, 1])

      for a in range(0, 3):
          for b in range(0, 3):

              i = connectivity_matrix[k, a]
              j = connectivity_matrix[k, b]

              stiffness_matrix_view[i, j] += c_constant[k] * element_solution[a + 3 * b]
              #x0[0], x0[1], x1[0], x1[1], x2[0], x2[1]

  return stiffness_matrix

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef (double, double, double, double, double, double, double, double, double) triangle_stiffness_matrix_laplaceold(double x10, double x11 , double x20, double x21, double x30, double x31):

      cdef double n00 = (x30 - x20) * (x30 - x20) + (x31 - x21) * (x31 - x21)
      cdef double n01 = (x20 - x30) * (x30 - x10) + (x21 - x31) * (x31 - x11)
      cdef double n02 = (x30 - x20) * (x20 - x10) + (x31 - x21) * (x21 - x11)
      cdef double n11 = (x30 - x10) * (x30 - x10) + (x31 - x11) * (x31 - x11)
      cdef double n12 = (x10 - x30) * (x20 - x10) + (x11 - x31) * (x21 - x11)
      cdef double n22 = (x20 - x10) * (x20 - x10) + (x21 - x11) * (x21 - x11)

      #calculate triangle area
      cdef double A = fabs(x10 * (x21 - x31) + x20 * (x31 - x11) + x30 * (x11 - x21))/2.0

      #calculate the coordinate transform Jacobian and its determinant
      cdef double J00 = x20 - x10
      cdef double J01 = x30 - x10
      cdef double J10 = x21 - x11
      cdef double J11 = x31 - x11

      cdef double J_det = J00 * J11 - J10 * J01

      cdef (double, double, double, double, double, double, double, double, double) M = (A * n00/J_det**2.0, A * n01/J_det**2.0, A * n02/J_det**2.0, A * n01/J_det**2.0, A * n11/J_det**2.0, A * n12/J_det**2.0, A * n02/J_det**2.0, A * n12/J_det**2.0, A * n22/J_det**2.0)

      return M

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef (double, double, double, double, double, double, double, double, double) triangle_stiffness_matrix_laplace(double x10, double x11 , double x20, double x21, double x30, double x31):

      cdef double n00 = (x30 - x20) * (x30 - x20) + (x31 - x21) * (x31 - x21)
      cdef double n01 = (x20 - x30) * (x30 - x10) + (x21 - x31) * (x31 - x11)
      cdef double n02 = (x30 - x20) * (x20 - x10) + (x31 - x21) * (x21 - x11)
      cdef double n11 = (x30 - x10) * (x30 - x10) + (x31 - x11) * (x31 - x11)
      cdef double n12 = (x10 - x30) * (x20 - x10) + (x11 - x31) * (x21 - x11)
      cdef double n22 = (x20 - x10) * (x20 - x10) + (x21 - x11) * (x21 - x11)

      #calculate triangle area
      cdef double A =  fabs(x10 * (x21 - x31) + x20 * (x31 - x11) + x30 * (x11 - x21))/2.0

      #calculate the coordinate transform Jacobian and its determinant
      cdef double J00 = x20 - x10
      cdef double J01 = x30 - x10
      cdef double J10 = x21 - x11
      cdef double J11 = x31 - x11

      cdef double J_det = J00 * J11 - J10 * J01

      cdef (double, double, double, double, double, double, double, double, double) M = (n00/(4.0 * A), n01/(4.0 * A), n02/(4.0 * A), n01/(4.0 * A), n11/(4.0 * A), n12/(4.0 * A), n02/(4.0 * A), n12/(4.0 * A), n22/(4.0 * A))

      return M

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26/09/18 19:07:12 2018

Optimised extension velocity construction through Cython
although not as sophisticated algorithmically as the fast marching method (higher complexity)

@author: charlie
"""

import cython

import numpy as np
cimport numpy as np

#external function
cdef extern from "math.h":
    double sqrt(double m)

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def signed_dist_field(double[::1] bv, double[:, ::1] boundary, int x, int y):

  cdef Py_ssize_t bmax = boundary.shape[0]
  cdef Py_ssize_t vmax = bv.shape[0]

  assert bmax, vmax

  ext_vel = np.zeros((x, y), dtype = np.double)
  cdef double[:, ::1] ext_vel_view = ext_vel

  cdef double dist1 = x*y*x*y
  cdef double dist2 =  x*y*x*y
  cdef double tdist = 0.0

  cdef double c1x = 0.0
  cdef double c1y = 0.0

  cdef double c2x = 0.0
  cdef double c2y = 0.0

  cdef double sbv1 = 0.0
  cdef double sbv2 = 0.0

  cdef Py_ssize_t i, j, v
  cdef Py_ssize_t xmax = x
  cdef Py_ssize_t ymax = y

  cdef double sb = 0.0
  cdef double sa = 0.0
  cdef double l = 0.0
  cdef double AB = 0.0

  for i in range(xmax):
    for j in range(ymax):

      dist1 = x * x * x + y * y * y
      sbv1 = 0.0

      for v in range(bmax):
        tdist = (i - boundary[v, 0])**2.0 + (j - boundary[v, 1])**2.0

        if tdist < dist1:
          dist2 = dist1
          dist1 = tdist

          sbv2 = sbv1
          sbv1 = bv[v]

          c2x = c1x
          c2y = c1y

          c1x = boundary[v, 0]
          c1y = boundary[v, 1]

        elif tdist < dist2:
          dist2 = tdist
          sbv2 = bv[v]

          c2x = boundary[v, 0]
          c2y = boundary[v, 1]

      AB = sqrt((c2x - c1x) ** 2.0 + (c2y - c1y) ** 2.0)

      sb = ( (c2x - i) * (c2x - c1x) + (c2y - j) * (c2y - c1y) )/AB
      sa = ( (c1x - i) * (c2x - c1x) + (c1y - j) * (c2y - c1y) )/AB

      l = (sb - sa)/AB

      if l * l >= 1.0:
        ext_vel_view[i, j] = sbv1
      else:
        ext_vel_view[i, j] =  sbv1 * l + sbv2 * (1.0 - l)


  return ext_vel

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sept 27 13:52:24 2018

The cython module for constructing the BEM matrices

@author: Charles Fletcher
"""

import cython

import numpy as np
cimport numpy as np
#from cython.parallel import prange

#external function
cdef extern from "math.h" nogil:
    double sqrt(double m)

cdef extern from "math.h" nogil:
    double atan2(double a, double b)

cdef extern from "math.h" nogil:
    double fabs(double v)

cdef extern from "math.h" nogil:
    double log(double v)

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef construct_matrices(double[:, ::1] C, double[:, ::1] A, double[:, ::1] B):

  """

  Constructs the BEM matrices for the constant element Direct BEM. The function
  makes use of parallel processing through prange in order to accelerate the matrix
  construction.

  :param1 C: The centre nodes of each boundary element
  :param2 A: The lefthand corner of each boundary element
  :param3 B: The righthand corner of each boundary element
  :returns:
    :F: The BEM F matrix
    :G: The BEM G matrix

  """

  cdef Py_ssize_t cmax = C.shape[0]

  F = np.zeros((cmax, cmax), dtype = np.double)
  cdef double[:, ::1] F_view = F

  G = np.zeros((cmax, cmax), dtype = np.double)
  cdef double[:, ::1] G_view = G

  cdef unsigned int same = 0

  cdef (double, double, double) coors = (0.0, 0.0, 0.0)

  cdef Py_ssize_t i, j

  for i in range(cmax):
    for j in range(cmax):

      coors = coordinate_transform(C[i, 0], C[i, 1], A[j, 0], A[j, 1], B[j, 0], B[j, 1])

      F_view[i, j] = Fij(coors, same)
      G_view[i, j] = Gij(coors, same)


  #calculate diagonal elements via flux condition
  cdef double total = 0.0

  for i in range(cmax):
      total = 1.0

      for j in range(cmax):
          total -= F_view[i, j]

      total += F_view[i, i]
      F_view[i, i] = total

  return F, G


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef calculate_field(double x, double y, double[::1] BP, double[::1] BF, double[:, ::1] Ne, double[:, ::1] Te, double[:, ::1] A, double[:, ::1] B):

    """

    Method for calculating the electric field vector at domain position (x, y)

    :param1 x: domain x coordinate
    :param2 y: domain y coordinate
    :param3 BP: The electric potentials at boundary nodes C
    :param4 BF: The electric fluxes at boundary nodes C (calculated via `boundary_field`)
    :param5 A: The lefthand boundary element corners
    :param6 B: The righthand boundary element corners
    :returns:
        field_x -- The x flux component at (x, y)
        field_y -- The y flux component at (x, y)

    """

    #transform to local polar coordinates
    cdef double field_x = 0.0
    cdef double field_y = 0.0

    cdef double[2] x_resolve = [1.0, 0.0]
    cdef double[2] y_resolve = [0.0, 1.0]

    cdef Py_ssize_t el_count = len(BP)
    cdef Py_ssize_t i = 0

    cdef (double, double, double) coors = (0.0, 0.0, 0.0)

    for i in range(el_count):

        coors = coordinate_transform(x, y, A[i, 0], A[i, 1], B[i, 0], B[i, 1])

        field_x += Hij(coors[0], coors[1], coors[2], Ne[i, 0], Ne[i, 1], Te[i, 0], Te[i, 1], 1.0, 0.0) * BP[i] - Kij(coors[0], coors[1], coors[2], Ne[i, 0], Ne[i, 1], Te[i, 0], Te[i, 1], 1.0, 0.0) * BF[i]
        field_y += Hij(coors[0], coors[1], coors[2], Ne[i, 0], Ne[i, 1], Te[i, 0], Te[i, 1], 0.0, 1.0) * BP[i] - Kij(coors[0], coors[1], coors[2], Ne[i, 0], Ne[i, 1], Te[i, 0], Te[i, 1], 0.0, 1.0) * BF[i]

    return -field_x, - field_y

@cython.cdivision(True)
@cython.boundscheck(False)
cdef inline (double, double, double) coordinate_transform(double cx, double cy, double ax, double ay, double dx, double dy) nogil:

  """
  The cython coordinate transformation for boundary elements - local cartesian to polars.

  :param1 O: The point relative to the boundary element
  :param2 A: The lefthand boundary element corner
  :param3 B: The righthand boundary element corner
  :returns:
      :coors: -- The normal distance component from O to the boundary element
      sa -- The parallel distance component from O to A
      sb -- The parallel distance component from O to B

  """


  cdef double adx = dx - ax
  cdef double ady = dy - ay

  cdef double cax = ax - cx
  cdef double cay = ay - cy

  cdef double cdx = dx - cx
  cdef double cdy = dy - cy

  cdef double norm = sqrt(adx**2.0 + ady**2.0)

  cdef double n = - (cax * ady - cay *adx)/norm
  cdef double sa = (cax * adx + cay * ady)/norm
  cdef double sb = (cdx * adx + cdy * ady)/norm

  cdef (double, double, double) coors = (n, sa, sb)

  return coors

@cython.cdivision(True)
@cython.boundscheck(False)
cdef double Fij((double, double, double) coors, unsigned int same) nogil:

  #coors[0] - n    coors[1] - sa     coors[2] - sb
  cdef double n = fabs(coors[0])
  cdef double value = 0.0


  if coors[0] > 0.0 and same == False:
      value = - 1.0/(2.0 * 3.14159265359) * (atan2(coors[2], n) - atan2(coors[1], n))
  if coors[0] <= 0.0 and same == False:
      value =  1.0/(2.0 * 3.14159265359) * (atan2(coors[2], n) - atan2(coors[1], n))

  return value

@cython.cdivision(True)
@cython.boundscheck(False)
cdef double Gij((double, double, double) coors, unsigned int same) nogil:

  #coors[0] - n    coors[1] - sa     coors[2] - sb
  cdef double n = fabs(coors[0])
  cdef long double pos = 0.0
  cdef long double neg = 0.0

  pos = (1.0/2.0 * coors[2] * (log(coors[0]**2.0 + coors[2]**2.0) - 2.0) + n*atan2(coors[2], n))
  neg = (1.0/2.0 * coors[1] * (log(coors[0]**2.0 + coors[1]**2.0) - 2.0) + n*atan2(coors[1], n))

  if same == 1:
    pos = (1.0/2.0 * coors[2] * (log(coors[0]**2.0 + coors[2]**2.0) - 2.0))
    neg = (1.0/2.0 * coors[1] * (log(coors[0]**2.0 + coors[1]**2.0) - 2.0))  

  return -1.0/(2.0 * 3.14159265359) * (pos - neg)

@cython.cdivision(True)
@cython.boundscheck(False)
cdef inline double Hij(double n, double sa, double sb, double nky0, double nky1, double tky0, double tky1, double nkx0, double nkx1) nogil:

    #coors[0] - n    coors[1] - sa     coors[2] - sb
    cdef double c1 = (sb/(n**2.0 + sb**2.0) - sa/(n**2.0 + sa**2.0)) * (nky0 * nkx0 + nky1 * nkx1)
    cdef double c2 = - (n/(n**2.0 + sb**2.0) - n/(n**2.0 + sa**2.0)) * (tky0 * nkx0 + tky1 * nkx1)

    return 1.0/(2.0* 3.14159265359) * (c1 + c2)

@cython.cdivision(True)
@cython.boundscheck(False)
cdef inline double Kij(double n, double sa, double sb, double nky0, double nky1, double tky0, double tky1, double nkx0, double nkx1) nogil:

    #coors[0] - n    coors[1] - sa     coors[2] - sb
    cdef double c1 = - ((n > 0) - (n < 0)) * ( atan2(sb, fabs(n)) - atan2(sa, fabs(n)) ) * (nky0 * nkx0 + nky1 * nkx1)
    cdef double c2 = - 0.5 * log((n**2.0 + sb**2.0)/(n**2.0 + sa**2.0)) * (tky0 * nkx0 + tky1 * nkx1)

    return 1.0/(2.0 * 3.14159265359) * (c1 + c2)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26/09/18 19:07:12 2018

Optimised extension velocity construction through Cython
although not as sophisticated algorithmically as the fast marching method (higher complexity)

@author: charlie
"""

import cython

import numpy as np
cimport numpy as np

#external function
cdef extern from "math.h":
    double sqrt(double m)

cdef extern from "math.h":
    int signbit(double m)

#removes error checking code from compiled cython for matrix index behaviour
#Calculates signed distance field from directional cross product
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def initialisation(double[:, ::1] boundary, int x, int y, int mode):

  cdef Py_ssize_t bmax = boundary.shape[0]

  field = np.zeros((x, y), dtype = np.double)
  cdef double[:, ::1] field_view = field

  cdef double dist1 = 2.0 * (x * x + y * y)
  cdef double dist2 =  2.0 * (x * x + y * y)
  cdef double tdist = 0.0

  cdef double c1x = 0.0
  cdef double c1y = 0.0

  cdef double c2x = 0.0
  cdef double c2y = 0.0

  cdef Py_ssize_t i, j, v
  cdef Py_ssize_t xmax = x
  cdef Py_ssize_t ymax = y

  cdef double sb = 0.0
  cdef double sa = 0.0
  cdef double l = 0.0
  cdef double AB = 0.0
  cdef double cp = 0.0
  cdef int flip = 1

  cdef int cind1 = 0
  cdef int cind2 = 0

  cdef double ndist = 0.0

  for i in range(xmax):
    for j in range(ymax):

      dist1 = 2.0 * (x * x + y * y)

      for v in range(bmax):
        tdist = (i - boundary[v, 0])**2.0 + (j - boundary[v, 1])**2.0

        if tdist < dist1:
          dist1 = tdist
          cind1 = v

      cind2 = cind1 - 1

      if cind2 == -1:
          cind2 = bmax - 1

      c1x = boundary[cind2, 0]
      c1y = boundary[cind2, 1]

      c2x = boundary[cind1, 0]
      c2y = boundary[cind1, 1]

      AB = sqrt((c2x - c1x) ** 2.0 + (c2y - c1y) ** 2.0)

      sa = ( (i - c1x) * (c2x - c1x) + (j - c1y) * (c2y - c1y) )/AB**2.0
      ndist = ((c2x - c1x) * (c1y - j) - (c2y - c1y) *(c1x - i))/AB

      if sa <= 1.0 and sa >= 0.0:
          field_view[i, j] = ndist
      else:

          cind2 = cind1 + 1

          if cind2 == bmax:
              cind2 = 0

          c1x = boundary[cind1, 0]
          c1y = boundary[cind1, 1]

          c2x = boundary[cind2, 0]
          c2y = boundary[cind2, 1]

          AB = sqrt((c2x - c1x) ** 2.0 + (c2y - c1y) ** 2.0)

          sa = ( (i - c1x) * (c2x - c1x) + (j - c1y) * (c2y - c1y) )/AB**2.0
          ndist = ((c2x - c1x) * (c1y - j) - (c2y - c1y) *(c1x - i))/AB

      if sa <= 1.0 and sa >= 0.0:
          field_view[i, j] = ndist
      else:
          field_view[i, j] = sqrt(dist1) * (1.0 - 2.0 * signbit(ndist))

  return field


#previous initialisation function - there was a bug regarding certain edge cases of field points close to the interface
#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def initialisationo(double[:, ::1] boundary, int x, int y, int mode):

  cdef Py_ssize_t bmax = boundary.shape[0]

  field = np.zeros((x, y), dtype = np.double)
  cdef double[:, ::1] field_view = field

  cdef double dist1 = 2.0 * (x * x + y * y)
  cdef double dist2 =  2.0 * (x * x + y * y)
  cdef double tdist = 0.0
  cdef int sign = -1

  cdef double c1x = 0.0
  cdef double c1y = 0.0

  cdef double c2x = 0.0
  cdef double c2y = 0.0

  cdef Py_ssize_t i, j, v
  cdef Py_ssize_t xmax = x
  cdef Py_ssize_t ymax = y

  cdef double sb = 0.0
  cdef double sa = 0.0
  cdef double l = 0.0
  cdef double AB = 0.0
  cdef double cp = 0.0
  cdef int flip = 1

  cdef int ivc1 = 0
  cdef int ivc2 = 0

  for i in range(xmax):
    for j in range(ymax):

      dist1 = 2.0 * (x * x + y * y)
      #sbv1 = 0.0

      for v in range(bmax):
        tdist = (i - boundary[v, 0])**2.0 + (j - boundary[v, 1])**2.0

        if tdist < dist1:
          dist2 = dist1
          dist1 = tdist

          c2x = c1x
          c2y = c1y

          ivc2 = ivc1
          ivc1 = v

          c1x = boundary[v, 0]
          c1y = boundary[v, 1]

        elif tdist < dist2:
          dist2 = tdist
          ivc2 = v

          c2x = boundary[v, 0]
          c2y = boundary[v, 1]

      AB = sqrt((c2x - c1x) ** 2.0 + (c2y - c1y) ** 2.0)

      sb = ( (c2x - i) * (c2x - c1x) + (c2y - j) * (c2y - c1y) )/AB
      sa = ( (c1x - i) * (c2x - c1x) + (c1y - j) * (c2y - c1y) )/AB

      l = (sb - sa)/AB

      flip = -1

      #is point inside or outside polygon?
      if mode == 1:
        for v in range(bmax):
          if ((boundary[v, 1] > (j + 1e-6)) != (boundary[(v + 1 % bmax), 1] > (j + 1e-6))) and (i < (boundary[(v + 1 % bmax), 0]-boundary[v, 0]) * (j-boundary[v, 1]) / (boundary[(v + 1 % bmax), 1] - boundary[v, 1]) + boundary[v, 0]):
            flip = - flip
      else:

        for v in range(bmax):
          if j > min(boundary[(v + 1) % bmax, 1], boundary[v, 1]):
            if j <= max(boundary[(v + 1) % bmax, 1], boundary[v, 1]):
              if i <= max(boundary[(v + 1) % bmax, 0], boundary[v, 0]):
                if (boundary[v, 1] != boundary[(v + 1) % bmax, 1]):
                  xints = (j - boundary[v, 1]) * (boundary[(v + 1) % bmax, 0] - boundary[v, 0])/(boundary[(v + 1) % bmax, 1] - boundary[v, 1]) + boundary[v, 0]
                if boundary[v, 0] == boundary[(v + 1) % bmax, 0] or i <= xints:
                  flip = - flip

      if l * l >= 1.0:
        field_view[i, j] = sqrt(dist1) * flip
      else:
        #field_view[i, j] =  abs(((c2x - c1x) * (c1y - j) - (c2y - c1y) *(c1x - i))/AB)  * flip
        field_view[i, j] =  (sqrt(dist1) * l + sqrt(dist2) * (1.0 - l)) * flip

  return field

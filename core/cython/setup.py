from distutils.core import setup
from Cython.Build import cythonize
import numpy
from distutils.extension import Extension

setup(
    name="extension_velocity",
    ext_modules=cythonize([Extension("core.extension_velocity", ["core/cython/extension_velocity.pyx"],)]),
    include_dirs=[numpy.get_include()]
)

setup(
    name="field_initialisation",
    ext_modules=cythonize([Extension("core.field_initialisation", ["core/cython/field_initialisation.pyx"],)]),
    include_dirs=[numpy.get_include()]
)

setup(
    name="BEM_matrix_construction",
    ext_modules=cythonize([Extension("core.boundary_element_library.BEM_matrix_construction", ["core/cython/boundary_element_library/BEM_matrix_construction.pyx"],)]),
    include_dirs=[numpy.get_include()]
)

setup(
    name="BEM_matrix_construction",
    ext_modules=cythonize([Extension("core.finite_element_library.stiffness_construction", ["core/cython/finite_element_library/stiffness_construction.pyx"],)]),
    include_dirs=[numpy.get_include()],
)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 11:46:22 2018

Initialises tip simulation geometry and simulation parameter values

@author: charlie
"""

import sys
import sqlite3
import numpy as np
from matplotlib.pyplot import imread

try:
     import pandas as pd
except ModuleNotFoundError:
    print("please install latest version of pandas")
    sys.exit()

try:
     from skimage import measure
except ModuleNotFoundError:
    print("please install latest version of skimage")
    sys.exit()


#loads global parameters
def load_global_parameters(global_parameters_filename):

    """
    Loads global parameters from tsv file in given directory. If no file or values are detected then default values are used.

    :param1 global_parameters_filename: The input filename for the global parameters tsv file
    :returns: A dictionary of global parameter values

    """

    global_parameters = dict()

    try:
        gp = pd.read_csv(global_parameters_filename, sep = '\t', comment = '#')
        gp['index_col'] = 0
        gp = gp[['index_col', 'parameter', 'value']].pivot(index = 'index_col', columns = 'parameter', values = 'value')

        try:
            global_parameters['beta'] = gp['beta'][0]
        except KeyError:
            print("No beta input value detected. Using default value of 4.0")
            global_parameters['beta'] = 4.0

        try:
            global_parameters['amplitude'] = gp['amplitude'][0]
        except KeyError:
            print("No amplitude input value detected. Using default value of 5e-4")
            global_parameters['amplitude'] = 5e-4

        try:
            global_parameters['tip_pot'] = float(gp['tip_pot'][0])
        except KeyError:
            print("No tip_pot input value detected. Using default value of -1.0 (In 2D, the ground is -inf)")
            global_parameters['tip_pot'] = -1.0

        try:
            global_parameters['detector_distance'] = float(gp['detector_distance'][0])
        except KeyError:
            print("No detector_distance input value detected. Using default value of 20000")
            global_parameters['detector_distance'] = 20000

        try:
            global_parameters['detector_width'] = float(gp['detector_width'][0])
        except KeyError:
            print("No detector_width input value detected. Using default value of 40000")
            global_parameters['detector_width'] = 40000

        try:
            global_parameters['gamma'] = gp['gamma'][0]
        except KeyError:
            print("No gamma input value detected. Using default value of 1.0")
            global_parameters['gamma'] = 1.0

        try:
            global_parameters['delta'] = gp['delta'][0]
        except KeyError:
            print("No delta input value detected. Using default value of 1.0")
            global_parameters['delta'] = 1.0

        try:
            global_parameters['frame'] = gp['frame']
        except KeyError:
            print("No frame input value detected. Using default value of 1000.0")
            global_parameters['delta'] = 1000.0

        try:
            global_parameters['element_size'] = gp['element_size']
        except KeyError:
            print("No element_size input value detected. Using default value of 25.0")
            global_parameters['element_size'] = 25.0

        try:
            global_parameters['element_angle'] = gp['element_angle']
        except KeyError:
            print("No element_angle input value detected. Using default value of 24.0")
            global_parameters['element_angle'] = 24.0

        try:
            global_parameters['mapping_update'] = gp['mapping_update']
        except KeyError:
            print("No mapping_update parameter value detected. If derive mapping set to true, using the default value of 5")
            global_parameters['mapping_update'] = 5

        try:
            global_parameters['projection_fraction'] = gp['projection_fraction']
        except KeyError:
            print("No projection_fraction parameter value detected. If derive mapping set to true, using the default value of 0.3")
            global_parameters['projection_fraction'] = 0.3

        try:
            global_parameters['termination_height'] = gp['termination_height']
        except KeyError:
            print("No termination_height parameter value detected. fUsing the default value of 50.0")
            global_parameters['termination_height'] = 50.0

        try:
            global_parameters['field_normal'] = gp['field_normal']
        except KeyError:
            print("No field_normal parameter mode detected. Using the default value of false")
            global_parameters['field_normal'] = 1

        try:
            global_parameters['mapping_resolution'] = gp['mapping_resolution']
        except KeyError:
            print("No mapping_resolution parameter mode detected. Using the default value of one ion projection per boundary element")
            global_parameters['mapping_resolution'] = 1

        try:
            global_parameters['projection_output'] = gp['projection_output']
        except KeyError:
            print("No projection_output parameter mode detected. Using the default value of False")
            global_parameters['mapping_resolution'] = 0

        try:
            global_parameters['output_frame'] = gp['output_frame']
        except KeyError:
            print("No output_frame parameter mode detected. Using the default value of False")
            global_parameters['output_frame'] = 0

        try:
            global_parameters['BEM'] = gp['BEM']
        except KeyError:
            print("No BEM parameter mode detected. Using the default value of 0")
            global_parameters['BEM'] = 0

        try:
            global_parameters['plot_projection'] = gp['plot_projection']
        except KeyError:
            print("No plot_projection parameter mode detected. Using the default value of True.")
            global_parameters['plot_projection'] = True

        try:
            global_parameters['plot_hitmap'] = gp['plot_hitmap']
        except KeyError:
            print("No plot_hitmap parameter mode detected. Using the default value of True.")
            global_parameters['plot_projection'] = True

        try:
            global_parameters['projection_separation'] = gp['projection_separation']
        except KeyError:
            print("No projection_separation parameter mode detected. Using the default value of 0.1.")
            global_parameters['projection_separation'] = 0.2

        try:
            law = gp['evaporation_law']
            if law[0] == 1:
                global_parameters['evaporation_law'] = "arrhenius"
            else:
                global_parameters['evaporation_law'] = "linear"
            print("evaporation law: " + global_parameters['evaporation_law'])
        except KeyError:
            print("No evaporation_law parameter mode detected. Defaulting to Ahhrenius law.")
            global_parameters['evaporation_law'] = "arrhenius"

        try:
            law = gp['solver_type']
            if law[0] == 1:
                global_parameters['solver_type'] = "iterative"
            else:
                global_parameters['solver_type'] = "direct"
            print("FEM-BEM solver: " + global_parameters['solver_type'])
        except KeyError:
            print("No FEM-BEM solver preference detected. Defaulting to direct.")
            global_parameters['solver_type'] = "direct"

        try:
            global_parameters['iterative_tolerance'] = gp['iterative_tolerance']
        except KeyError:
            print("No iterative FEM-BEM tolerance prefence detected: setting tolerance to 1e-8")
            global_parameters['iterative_tolerance'] = [1e-8]

        try:
            global_parameters['scale'] = gp['scale'][0]
        except KeyError:
            print("scale parameter detected: setting scale to 1.0")
            global_parameters['scale'] = 1.0

        try:
            global_parameters['initial_iterations'] = gp['initial_iterations']
        except KeyError:
            print("No initial iteration parameter detected: setting scale to 0")
            global_parameters['initial_iterations'] = 0

        try:
            global_parameters['n'] = gp['shank_el_num']
        except KeyError:
            print("No shank element number parameter detected: setting element number to 10")
            global_parameters['n'] = [10]

        try:
            global_parameters['l'] = gp['shank_el_length']
        except KeyError:
            print("No shank element length parameter detected: setting element length to 30.0")
            global_parameters['l'] = [30.0]

        try:
            global_parameters['CFL'] = gp['CFL'][0]
        except KeyError:
            print("No CFL parameter detected: setting CFL fraction to 0.2")
            global_parameters['CFL'] = 0.2

        try:
            global_parameters['dynamic_voltage'] = gp['dynamic_voltage'][0]
        except KeyError:
            print("No dynamic_voltage parameter detected: setting dynamic_voltage fraction to 1 (on)")
            global_parameters['dynamic_voltage'] = 1

        try:
            global_parameters['FR'] = gp['FR'][0]
        except KeyError:
            print("No field reduction parameter detected: setting field reduction parameter to 1.03 (0.03 greater than max flux)")
            global_parameters['FR'] = 1.03

        try:
            global_parameters['refinement'] = gp['refinement'][0]
        except KeyError:
            print("Mesh refinement surrounding phase boundaries: setting mesh refinement parameter to 0 (off)")
            global_parameters['refinement'] = 0

    except FileNotFoundError:
        print("file not found. Resorting to default global parameter values. See documentation for how to construct such a file.")
        global_parameters['beta'] = 4.0
        global_parameters['amplitude'] = 5e-4
        global_parameters['tip_pot'] = -1.0
        global_parameters['detector_distance'] = 20000
        global_parameters['detector_width'] = 40000
        global_parameters['gamma'] = 1.0
        global_parameters['delta'] = 1.0
        global_parameters['frame'] = 1000.0
        global_parameters['element_size'] = 25.0
        global_parameters['element_angle'] = 24.0
        global_parameters['mapping_update'] = 5
        global_parameters['projection_fraction'] = 0.3
        global_parameters['termination_height'] = 50.0
        global_parameters['field_normal'] = 1
        global_parameters['mapping_resolution'] = 1
        global_parameters['projection_output'] = 0
        global_parameters['output_frame'] = 0
        global_parameters['BEM'] = 0
        global_parameters['plot_hitmap'] = 1
        global_parameters['plot_projection'] = 1
        global_parameters['projection_separation'] = 0.2
        global_parameters['evaporation_law'] = "ahhrenius"
        global_parameters['epsilon_0'] = 1.0
        global_parameters['scale'] = 1.0
        global_parameters['initial_iterations'] = 0
        global_parameters['n'] = 10
        global_parameters['l'] = 30.0
        global_parameters['solver_type'] = "direct"
        global_parameters['iterative_tolerance'] = 1e-8
        global_parameters['CFL'] = 0.2
        global_parameters['dynamic_voltage'] = 1
        global_parameters['refinement'] = 0

    return global_parameters


def geom_init(tip_geometry_dir, materials_key_dir, materials_db_dir, global_parameters_filename, dielectric_mode, mag = 10):

    """
    Initialse the simulation tip geometry. This includes extracting and labelling
    the phases from the passed image via the materials mat_id-color key and constructing
    a grid of local evaporation field values and crystallographic anisotropies via
    look up within the materials database

    :param1 tip_geometry_dir: the images file for inputing the tip geometry
    :param2 materials_key_dir: A TSV file matching up rgb pixel colors within the input image to the db mat_id

    r   g   b   mat_id  grain_rot (clockwise from normal)
    255 255 255   0     45
    ... ... ...   ...   ...

    :param3 materials_db_dir: the directory of the sqlite materials database
    :returns: tip boundary, local field evap values, bool grid of amorphous, list of material_visualisation features, color_map for material_visualisation

    """

    tip_geom = imread(tip_geometry_dir)
    materials_key = pd.read_csv(materials_key_dir, sep = '\t', comment = '#')

    tip_geom_int = np.zeros(tip_geom.shape[0:2])
    two_dim_grain_map = np.zeros(tip_geom.shape[0:2])
    grain_dict = {}

    F0 = np.empty(tip_geom.shape[0:2]) + 0.001
    amorphous = np.empty(tip_geom.shape[0:2], dtype = bool)

    tip_geom_t = np.transpose(tip_geom, axes = (2, 0, 1))

    print("RGB pixel colors detected: ")
    print(np.unique(np.vstack((tip_geom[:, :, 0].flatten(), tip_geom[:, :, 1].flatten(), tip_geom[:, :, 2].flatten())), axis = 1).T)

    dielectric_constant = np.zeros(tip_geom.shape[0:2], dtype = float) + 1.0e5

    #generate field evaporation array
    conn = sqlite3.connect(materials_db_dir)
    c = conn.cursor()

    material_visualisation = []
    col_grain_map = []
    for i in range(0, len(materials_key)):

        tip_geom_int[(tip_geom_t[0] == materials_key['r'][i]) * (tip_geom_t[1] == materials_key['g'][i]) * (tip_geom_t[2] == materials_key['b'][i])] = materials_key['mat_id'][i]
        two_dim_grain_map[(tip_geom_t[0] == materials_key['r'][i]) * (tip_geom_t[1] == materials_key['g'][i]) * (tip_geom_t[2] == materials_key['b'][i])] = i
        col_grain_map.append(materials_key['color'][i])
        a = c.execute('SELECT nx, ny, facet_evaporation_field FROM crystallography where mat_id = ' + str(materials_key['mat_id'][i])).fetchall()

        if len(a) > 1:
            nx, ny, facet_evaporation_field = np.hsplit(np.array(a), 3)
            f = np.array([nx.flatten() * facet_evaporation_field.flatten(), ny.flatten() * facet_evaporation_field.flatten()]).T
            theta = materials_key['grain_rot'][i]
            cos, sin = np.cos(theta), np.sin(theta)
            R = np.matrix([[cos, -sin], [sin, cos]])
            rf = np.matmul(R, f.T).T
            amorph = False
        else:
            rf = np.array([])
            amorph = True

        grain_dict[i] = {'mat_id': materials_key['mat_id'][i], 'f': rf, 'grain_rot': materials_key['grain_rot'][i], 'amorphous': amorph}

    #construct color map for coloring phases within image output
    colors = ['green', 'blue', 'red']
    color_map = []
    unique_dc = set()
    mat_present = np.unique(tip_geom_int)
    mat_contours_present = []

    for i in range(0, len(materials_key)):
        if i != 0:
            try:
                mat_id, am, field_evap = c.execute('SELECT mat_id, amorphous, evaporation_field FROM main where mat_id = ' + str(materials_key['mat_id'][i])).fetchone()
            except:
                print("Cannot find particular material within database. Are you sure that materials_key is correct?? Perhaps check 'RGB pixel colors detected' with materials key pixels?")
                print("terminating...")

            if dielectric_mode > 0:
                d = c.execute('SELECT epsilon FROM dielectric where mat_id = ' + str(materials_key['mat_id'][i])).fetchone()

                if d == None:
                    print("Cannot find dielectric constant. Assuming conductor")
                    d = [1.0e19]
                unique_dc.add(d[0])

                if d != None:
                    dielectric_constant[tip_geom_int == mat_id] = d


            F0[tip_geom_int == mat_id] = field_evap
            amorphous[tip_geom_int == mat_id] = bool(am)

    #create plot for grain visualisation
    for ind in range(0, len(materials_key)):
        mat_cont = measure.find_contours(two_dim_grain_map == ind, 1 - 0.1)
        mat_contours_present.append(len(mat_cont))
        for cont in mat_cont:
            material_visualisation.append(cont/mag)
            if materials_key['color'][ind] != None:
                color_map.append(materials_key['color'][ind])#

    #reorder color_map according to features such that larger outside contour is drawn with lowest z-ordering i.e. at bottom
    color_map = [x for _, x in sorted(zip(material_visualisation, color_map), key = lambda s: -len(s[0]))]

    #reorder features such that larger outside contour is drawn with lowest z-ordering i.e. at bottom
    material_visualisation.sort(key = lambda s: -len(s))

    #extract external tip geometry
    boundary = np.array(measure.find_contours(tip_geom_int, 0))/mag

    material_visualisation[0] = boundary[0]

    if len(boundary) > 1:
        raise ValueError('Multiple boundaries detected. Please check input image for isolated pixels. \t This could also be an issue with the order of materials_key.txt!')

    global_parameters = load_global_parameters(global_parameters_filename)

    return boundary[0], F0[::mag, ::mag], amorphous[::mag, ::mag], material_visualisation, color_map, two_dim_grain_map[::mag, ::mag], grain_dict, global_parameters, dielectric_constant[::mag, ::mag], unique_dc, tip_geom_int[::mag, ::mag], col_grain_map

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 15:27:05 2017

Body method of tip simulation package - handles field evolution

@author: charlie
"""
import sys
import scipy as sp
import numpy as np

try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError:
    print("Please install the latest version of matplotlib (and all dependenciesl).")
    print('Program terminated!')
    sys.exit()


import math
import imp
import os
import subprocess
import tempfile
import shutil
import scipy.sparse
import copy as cp

try:
    import core.extension_velocity as fm
except ModuleNotFoundError:
    print("Cannot find compiled extension_velocity. Check .so has been properly compiled and is in the core directory.")
    print("terminating...")
    sys.exit()

try:
    from skimage import measure
except ModuleNotFoundError:
    print("Please install latest version of scikit-image (skimage).")
    sys.exit()


try:
    import core.boundary_element_library.BEM as BEM
except ModuleNotFoundError:
    print("Cannot find BEM ancillary routines in core.boundary_element_library. Check BEM.py file is here and otherwise redownload from repository.")
    print("terminating...")
    sys.exit()

try:
    import core.finite_element_library.FEM as FEM
except ModuleNotFoundError:
    print("Cannot find FEM ancillary routines in core.finite_element_library. Check FEM.py file is here and otherwise redownload from repository.")
    print("terminating...")
    sys.exit()

try:
    import core.mesh_gen.mesh_gen as mesh_gen
except ModuleNotFoundError:
    print("Cannot find mesh_gen routines in core.mesh_gen. Check mesh_gen.py file is here and otherwise redownload from repository.")
    print("terminating...")
    sys.exit()

try:
    import core.finite_element_library.stiffness_construction as FEM_cython
except ModuleNotFoundError:
    print("Cannot find compiled stiffness_construction. Check .so has been properly compiled and is in the core/finite_element_library directory.")
    print("terminating...")
    sys.exit()

try:
    import core.boundary_element_library.BEM_matrix_construction as cBEM_cython
except ModuleNotFoundError:
    print("Cannot find compiled BEM_matrix_construction. Check .so has been properly compiled and is in the core/boundary_element_library directory.")
    print("terminating...")
    sys.exit()


from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.filters import gaussian_filter1d

from scipy.interpolate import splprep
from scipy.interpolate import splev

from scipy.optimize import fsolve
from scipy.optimize import root
from scipy.optimize import minimize

class sim_space:

    def init_Efield(self, amp, beta, F0, dielectric_constants, tip_pot = 1.0):


        """
        method for initialising evaporation physics parameters

        :param1 amp: amplitude for evaporation law
        :param2 beta: temperature parameter for evaporation law
        :param3 F0: 2D grid array of local field evaporation values (phase dependent)
        :param4 tip_pot: Potential tip is held at.
        :returns: 0

        """

        #field evaporation parameters within Arrhenius Law
        self.amp = amp
        self.beta = beta
        self.F0 = F0
        self.tip_pot = tip_pot
        self.dc = dielectric_constants

    def init_anisotropy(self, gamma, delta, two_dim_grain_map, grain_dict):

        """
        method for intialising the anisotropy parameters

        :param1 gamma: global scaling parameter for angular dependent term for anisotropic materials
        :param2 delta: global scaling parameter for facet evaporation for anisotropic materials
        :param3 two_dim_grain_map: 2D grid mapping location of different grains
        :param4 grain_dict: dictionary containng grain dependent field evaporation parameters
        :returns: 0

        """

        #velocity function mixing parameters
        self.gamma = gamma #facet parameter
        self.delta = delta #curvature parameter
        self.two_dim_grain_map = two_dim_grain_map #2D grid of grain indices
        self.grain_dict = grain_dict #dictionary of crystallographic dictionaries

    #construction of 1st order backwards difference operator
    def backwards_grad(self, h):
        return (np.eye(self.x, self.y, k = 0) - np.eye(self.x, self.y, k = -1))/h

    #construction of 1st order forward difference operator
    def forwards_grad(self, h):
        return (np.eye(self.x, self.y, k = 1) - np.eye(self.x, self.y , k = 0))/h

    ###constructor for 1st order central difference operator
    def central_grad(self, h):
       return [-(1/2.0 * np.eye(self.y, self.y, k = 1) - 1/2.0 * np.eye(self.y, self.y, k = -1))/(1.0*h), (1/2.0 * np.eye(self.x, self.x, k = 1) - 1/2.0 * np.eye(self.x, self.x, k = -1))/(1.0*h)]

    def run_field_iterator(self, previous_field, dt = 0.5, a = 1.0, h = 1.0, sigma = 1.0, mod_field = 0.0, dynamic = True, dielectric_mode = 0, mesh_area = 20.0, mesh_angle = 24.0, unique_dc = set(), field_normal = 1, cBEMmode = 0, law = "arrhenius", uniform_evap_field = False, CFL = 0.2):

        """
        The method for performing an Euler field iteration. Higher order integrators can be performed via the mod_field argument.

        :param1 dt:  the passed in timestepper. This will only be used if dynamic = False
        :param2 h: the system spatial scaling factor
        :param3 sigma: the gradient/velocity blurring factor (gaussian smoothing var)
        :param4 mod_field: a modified field value to be passed onto the iterate function. This additional value allows for construction of higher order time integrators
        :returns: The iterated level-set field is returned along with the discreted tip representation (two_dim_grid), gradients and timestep.
        """

        #interpolate boundary - performed via marching squares algorithm
        #measure.find_contours returns array of multiple functions
        self.contours = np.array(measure.find_contours(self.two_dim_grid_field, 0.0))

        #find largest contour - assume this represents the tip
        ind = 0
        prev = 0
        for l in range(0, len(self.contours)):
            if len(self.contours[l]) > prev:
                prev = len(self.contours[l])
                ind = l

        #return largest contour found by marching squares

        self.interpolated_boundary = self.contours[ind]

        self.two_dim_grid = np.zeros(self.two_dim_grid.shape, dtype = int)
        self.two_dim_grid[self.two_dim_grid_field > 0] = 1

        ###perform boundary element surface flux calculation
        #create boundary elements
        if self.boundary_intersect == True:
            self.n = 6 #tip extension (shank)
            self.l = 30.0 #extension BE size
            #initially construct boundary elements from Marching Squares extracted boundary - append BE
            A, B, C = BEM.prepare_BE(self.interpolated_boundary[:-1], self.slv, self.slr, sh = self.n, l = self.l)

        else:
            self.n = 0
            self.l = 0.0
            #initially construct boundary elements from Marching Squares extracted boundary - append BE
            A, B, C = BEM.prepare_BE(self.interpolated_boundary, self.slv, self.slr, sh = self.n, l = self.l)


        self.A = A
        self.B = B
        self.C = C

        #if no surface dielectrics detected, run field calculation via BEM conductor mode
        Cx = np.hsplit(self.interpolated_boundary, 2)[0].flatten().astype(int)
        Cy = np.hsplit(self.interpolated_boundary, 2)[1].flatten().astype(int)

        #check if dielectric intersecting tip surface, if not then switch to faster BEM mode for field calculation
        if np.sum(self.dc[Cx, Cy] < 1e5) == 0 and dielectric_mode == 2:
              dielectric_mode = 0

        #if true, FEM-BEM coupled field calculation
        if dielectric_mode > 0:

            ###extract dielectric regions (i.e. precipitates and multilayers) via marching squares
            dielectric_contours = []
            lb = self.two_dim_grid_field >= 1.0

            for val in unique_dc:
                if val < 1e5:
                    ext = self.dc * lb == val
                    contours = measure.find_contours(ext, 1.0 - 0.5)

                    #remove final coordinate of contour so start/end node are not appended to mesh twice
                    open_contours = []

                    for c in contours:
                        open_contours.append(c[:-1])

                    #append open dielectric contours into list for mesh generation
                    dielectric_contours = dielectric_contours + open_contours

            #generate mesh
            if self.refinement == True:
                #print("refinement")
                file_directory, refined_boundary_mesh = mesh_gen.generate_mesh([A] + dielectric_contours, self.dc, self.n, mesh_area, mesh_angle)
            else:
                #print("no refinement")
                file_directory, refined_boundary_mesh = mesh_gen.generate_mesh([A], self.dc, self.n, mesh_area, mesh_angle)

            #redefine boundary elements from refined mesh
            A, B, C = BEM.prepare_BE(np.vstack((refined_boundary_mesh, refined_boundary_mesh[0])), self.slv, self.slr, sh = 0, l = 0)

            self.A = A
            self.B = B
            self.C = C

            #import outputed triangle mesh
            self.mesh_coors = FEM.obtain_mesh_coords(directory = file_directory + '/tip.1.node')
            self.scaled_mesh_coords =  self.mesh_coors * self.scale
            self.connectivity_matrix = FEM.obtain_connectivity_Matrix(directory = file_directory + '/tip.1.ele')

            ob1, ob2 = FEM.obtain_boundary_indices_ordered(directory = file_directory + '/tip.1.poly')

            ###check if boundary nodes are anticlockwise, otherwise reverse
            ob1s = np.cross(self.mesh_coors[ob1[40]] - self.mesh_coors[ob1[20]], self.mesh_coors[ob1[20]] - self.mesh_coors[ob1[0]])
            if (np.sign(ob1s)) < 0.0:
                ob1 = np.ascontiguousarray(ob1[::-1])
                ob2 = np.ascontiguousarray(ob2[::-1])

            self.c_constant = self.define_permittivity(self.mesh_coors, self.connectivity_matrix)

            edge_matrix = np.stack((ob1, ob2), axis = 0).T

            #remove temporary directory for storing mesh objects
            shutil.rmtree(file_directory)

            #place down conductor centre
            dirichlet_centre = np.array([self.x/2.0, 0.0 - (self.n - 1) * self.l])
            dirichlet_radius = self.l/3.0

            #construct problem stiffness matrix
            stiffness = FEM_cython.construct_global_stiffness_matrix(self.scaled_mesh_coords, self.connectivity_matrix, np.array(self.c_constant))

            ###uncomment for numpy version - ***currently inactive***
            #stiffness = FEM.construct_global_stiffness_matrix(self.mesh_coors, self.connectivity_matrix, np.array(self.c_constant))

            #impose dirichlet boundary condition to d_group nodes
            d_group = [FEM_cython.dirichlet_bc(self.mesh_coors, self.connectivity_matrix, dirichlet_centre[0], dirichlet_centre[1], dirichlet_radius)]

            d_vals = np.array([self.tip_pot] * len(d_group))
            b_vector1, stiffness = FEM.dirichlet_construction(d_group, d_vals, stiffness)

            #impose neumann boundary condiion
            n_group = ob1

            alpha = 0.0

            #Is coupling via constant BEM
            self.mode = 0

            if self.solver_type == "direct":

                """
                This is the direct FEM-BEM solver implementation
                """

                C_Matrix, neumann_matrix, stiffness = FEM.neumann_construction_edges_matrix(n_group, edge_matrix, self.scaled_mesh_coords, stiffness)

                ###Construct node-edge transfer matrix (clockwise)
                transfer_matrix_forward = 1.0/2.0 * (np.eye(len(n_group), dtype=int) + np.eye(len(n_group), k=1, dtype=int) + np.eye(len(n_group), k=-(len(n_group) - 1), dtype=int))
                #transfer_matrix_forward = 1.0/2.0 * (np.eye(len(n_group), dtype=int) + np.eye(len(n_group), k=-1, dtype=int) + np.eye(len(n_group), k=(len(n_group) - 1), dtype=int))


                A, B, C = BEM.prepare_BE(np.vstack((self.mesh_coors[ob1], self.mesh_coors[ob1][0])), self.slv, self.slr, l = 0, sh = 0)

                self.A = A
                self.B = B
                self.C = C

                #Calculate matrices required for BEM component of problem (neumann BCs)
                F, G = cBEM_cython.construct_matrices(C*self.scale, A*self.scale, B*self.scale)

                Ginv = np.linalg.inv(G)

                #print(neumann_matrix.shape, A.shape, transfer_matrix_forward.shape, F.shape)
                mod_mat = np.matmul(neumann_matrix, np.matmul(Ginv, np.matmul(F, transfer_matrix_forward)))
                n_matrix = FEM_cython.expand_matrix(mod_mat, n_group, stiffness.shape[0], stiffness.shape[1])

                #construct problem load vector
                load_vector = b_vector1

                #append Neumann coupling onto FEM stiffness matrix
                stiffness += n_matrix

                #Solve coupled FEM-BEM problem
                sparse_stiffness = scipy.sparse.csc_matrix(stiffness)
                u = scipy.sparse.linalg.spsolve(sparse_stiffness, load_vector)


                self.BP = np.matmul(transfer_matrix_forward, u[ob1])

                #Calculate normal boundary fluxes via BEM
                self.BF = np.matmul(Ginv, np.matmul(F, self.BP))

                BF = cp.deepcopy(self.BF)

                #######uncomment to plot the dielectric permittivity per iteration#######
                #self.plot_permittivity(self.mesh_coors, self.connectivity_matrix, np.array(self.c_constant), demo_directory = 'output')

                #######uncomment to plot the electric field solution per iteration - note due to domain field calculations this will seriously slow the program#######
                #self.plot_electric_field_solution(self.mesh_coors, self.connectivity_matrix, u, self.BP, BF, A*self.scale, B*self.scale, contours = [29997, 29995, 29992, 29990, 29960, 29900], demo_directory = 'output')

            else:

                """
                This is the iterative FEM-BEM solver implementation

                Known bugs exist with this currently - will be resolved

                """

                C_Matrix, neumann_matrix, stiffness = FEM.neumann_construction_edges_matrix(n_group, edge_matrix, self.scaled_mesh_coords, stiffness)

                A, B, C = BEM.prepare_BE(np.vstack((self.scaled_mesh_coords[ob1], self.scaled_mesh_coords[ob1][0])), self.slv, self.slr, l = 0, sh=0)

                #Calculate matrices required for BEM component of problem (neumann BCs)
                F, G = cBEM_cython.construct_matrices(C * self.scale, A*self.scale, B*self.scale)

                Ginv = np.linalg.inv(G)
                stiffnessinv = scipy.linalg.inv(stiffness)

                error = 10.0
                pbit = np.array([-1.0] * len(C))
                pfit = np.array([-1.0] * len(self.mesh_coors))
                qit = np.array([0.0] * len(C))
                pbit_prev = np.array([-10.0] * len(C))
                neumann_bc = np.array([0.0] * len(b_vector1))
                norms = np.linalg.norm(B - A, axis = 1)
                norms_roll = np.roll(norms, -1)

                while error > self.iterative_tol:
                    qit[:] = np.matmul(Ginv, np.matmul(F, pbit))
                    neumann_bc[:] = 0.0
                    neumann_bc[ob1] = - 0.5 * norms_roll * np.roll(qit, -1) - 0.5 * norms * qit
                    pfit[:] = np.matmul(stiffnessinv, b_vector1 + neumann_bc)
                    pbit_prev[:] = pbit
                    pbit[:] = 0.5 * (pfit[ob1] + np.roll(pfit[ob1], -1))
                    error = np.linalg.norm(abs(pbit - pbit_prev))/len(C)

                self.BF = np.matmul(Ginv, np.matmul(F, pbit))

                BF = self.BF
                self.BP = pbit
                self.fpot = pfit

        else:

            self.mode = 0

            self.BP = np.repeat(self.tip_pot, len(C))

            self.A = A
            self.B = B
            self.C = C

            F, G = cBEM_cython.construct_matrices(C*  self.scale, A*  self.scale, B*  self.scale)

            ###uncomment for numpy version - ***currently inactive***
            #F, G = BEM.construct_matrices(C, A, B)


            #iterative GMRES solver faster than default direct solver - uncomment for use
            #self.BF = scipy.sparse.linalg.gmres(G, np.matmul(F, self.BP))[0]
            self.BF = np.linalg.solve(G, np.matmul(F, self.BP))

            #remove calculated fluxes from tip shank extension
            if self.n > 0:
                A = A[self.n:-(self.n + 1)]
                B = B[self.n:-(self.n + 1)]
                C = C[self.n:-(self.n + 1)]
                BF = self.BF[self.n:-(self.n + 1)]
            else:
                BF = self.BF

        cx = C[:, 0]
        cy = C[:, 1]

        #plot the flux
        #plt.plot(BF)
        #plt.savefig("flux.png")
        #plt.close()

        #calculate norms of boundary elements - for faceted evaporation
        #second order tangent definition
        tangent = B - A
        norms = np.matmul(np.array([[0.0, -1.0], [1.0, 0.0]]), tangent.T).T
        norms = gaussian_filter1d(norms, 1.0, axis = 0)
        norms = norms/np.linalg.norm(norms, axis = 1)[:, None]

        #derive local work function on boundary
        coordsx = np.round(cx).astype(int)
        coordsy = np.round(cy).astype(int)

        coordsx[coordsx < 0] = 0
        coordsx[coordsx >= self.x] = 0

        coordsy[coordsy < 0] = 0
        coordsy[coordsy >= self.y] = 0

        if uniform_evap_field == False:
        #Modify material field evaporation parameters according to their specific faceting function
            self.F0b = self.anisotropy(self.F0[coordsx, coordsy].flatten(), norms, self.gamma, self.delta, self.two_dim_grain_map[coordsx, coordsy].flatten()).reshape(-1)
        else:
            self.F0b = np.array([0.001] * len(BF))

        #boundary velocity defined by Ahrenius' Law
        self.F0b[cy <= 0.0] = 1e20

        BF[BF < 0.0] = 0.0

        ####remove boundary fluxes of any appended shank base (assuming along y-axis)
        min_c = np.min(C[:, 1])
        if min_c < 0.0:
            BF[C[:, 1] < (min_c + 0.0) * 0.5] = 0.0

        #medium filter required to remove constant BEM flux spikes - if running in dynamic voltage mode
        #higher order collocation BEM/Galerkin methods could resolve this
        sBF = scipy.signal.medfilt(BF, 5)

        #plot the smoothed boundary flux (used for surface evolution calculation for dynamic voltage mode)

        if law == "arrhenius":
            if self.dynamic_voltage == True:
                flux_scale = np.max(sBF/self.F0b)
                self.voltage_curve.append(self.tip_pot/flux_scale)
                field_reduction = 1.0 - self.FR * (BF/self.F0b)/flux_scale
            else:
                field_reduction = 1.0 - (BF/self.F0b)

            self.BV = - self.amp * np.exp(-self.beta * (field_reduction)).reshape(len(BF))
        elif law == "linear":
            self.BV = - (self.amp * (BF)/self.F0b).reshape(len(BF))
        elif law == "constant":
            self.BV = - np.array([1.0] * len(BF))


        ###uncomment to plot the surface field strength vs normal flux per iteration
        #self.plot_surface_dielectric(self.BP * 25000.0/np.max(self.BP), self.BF * 25000.0/np.max(self.BP), self.A, self.B, self.C, output_directory = 'output')

        #remove velocity discontinuities - possible due to collocation method
        self.BV[self.BV == -math.inf] = min(self.BV[self.BV != -math.inf]) * 0.99

        #construct extension velocity for level-set field evolution
        self.extension_velocity = fm.signed_dist_field(self.BV, C, self.x, self.y)

        #smooth velocity field - dampen flux spikes arising from constant panel collocation
        self.extension_velocity = gaussian_filter(self.extension_velocity, 1.0)

        ###perform level-set field iteration under calculated extension_velocity
        output = self.iterate(previous_field, dt, a, h, self.extension_velocity, sigma = sigma, mod_field = mod_field, dynamic = dynamic, shank = self.boundary_intersect, CFL = CFL)

        two_dim_grid_field = output[0]

        #plt.imshow(two_dim_grid_field)
        #plt.savefig("mod_flux.png")
        #plt.close()
        #print(CFL)

        self.curvature = output[1]

        two_dim_grid = np.zeros((self.x, self.y))
        two_dim_grid[two_dim_grid_field > 0.0] = 1.0

        return two_dim_grid_field, two_dim_grid, output[1], output[2], output[3], output[4]

    #define permittivity of mesh finite elements
    def define_permittivity(self, mesh_coors, connectivity_matrix):

        """
        Define permittivity of mesh finite elements
        permittivity chosen for particular finite element based of finite element center

        :param1 self: The level-set field object instance
        :param2 mesh_coors: The constructed FEM mesh coordinates
        :param3 connectivity_matrix: The FEM mesh connectivity_matrix
        :returns: list of finite element permittivities at indices corresponding to the connectivity_matrix indices

        """

        c_constant = []

        for row in connectivity_matrix:
            triangle_centre = (mesh_coors[row[0]] + mesh_coors[row[1]] + mesh_coors[row[2]])/3.0
            ix = int(round(triangle_centre[0]))
            iy = int(round(triangle_centre[1]))

            if ix >= 0 and ix < self.x and iy >= 0 and iy < self.y:
                c_constant.append(self.dc[ix, iy])
            else:
                c_constant.append(1.0e19)

        return c_constant

    #Identify FEM mesh nodes within particular circular region of mesh
    def dirichlet_group(self, mesh_coors, connectivity_matrix, centre, radius, exclusion = []):

        """
        Identify FEM mesh nodes within particular circular region of mesh.
        Used to initialise the tip base fixed potential Dirichlet boundary condition.

        :param1 self: The level-set field object instance
        :param2 mesh_coors: The constructed FEM mesh coordinates
        :param3 connectivity_matrix: The FEM mesh connectivity_matrix
        :param4 centre: The centre for the circular region
        :param5 radus: The radius for the circular region
        :param6 exclusion: The indices of mesh nodes to exclude from the region i.e. mesh boundaries
        :returns: List of indices for mesh circular region/group corresponding to nodes in mesh_coors

        """

        group = []

        for row in connectivity_matrix:
            for el in row:
                if np.linalg.norm(mesh_coors[el] - centre) < radius and (el not in group) and (el not in exclusion):
                    group.append(el)
                    return group

    def plot_surface(self, features = [], demo_directory = '', detached = False):

        """
        Plotting function for outputing 2D tip geometry.

        Created image is saved to directory provided 'demo_directory'.

        :param1 self: the level_set space object
        :param2 features: list of 'feature' objects for drawing output
        :param3 demo_directory: image output directory
        :returns: 0

        """

        fig2 = plt.figure(figsize = (15, 15))
        ax = plt.gca()

        plt.xlabel("nm", fontsize = "52")
        plt.ylabel("nm", fontsize = "52")

        plt.xticks(fontsize = 36)
        plt.yticks(fontsize = 36)

        for f in features:
            if f.f_type == "vertical_layer":
                ax.fill_between(np.hsplit(self.interpolated_boundary, 2)[0].flatten(), np.hsplit(self.interpolated_boundary, 2)[1].flatten(), where = (np.hsplit(self.interpolated_boundary, 2)[0].flatten() >= f.fmin) & (np.hsplit(self.interpolated_boundary, 2)[0].flatten() <= f.fmax), facecolor = f.color, hatch = f.pattern)
            elif f.f_type == "horizontal_layer":
                ax.fill_betweenx(np.hsplit(self.interpolated_boundary, 2)[0].flatten(), np.hsplit(self.interpolated_boundary, 2)[1].flatten(), where = (np.hsplit(self.interpolated_boundary, 2)[1].flatten() >= f.fmin) & (np.hsplit(self.interpolated_boundary, 2)[1].flatten() <= f.fmax), facecolor = f.color, hatch = f.pattern, zorder = 10)
            elif f.f_type == "precipitate":
                ax.fill(f.fmin, f.fmax, color = f.color, hatch = f.pattern)

        if detached == False:
            ax.fill_between(np.concatenate(([0.0], np.hsplit(self.interpolated_boundary, 2)[0].flatten(), [self.x])), np.concatenate(([0.0], np.hsplit(self.interpolated_boundary, 2)[1].flatten(), [0.0])), self.y, facecolor = "black", zorder=math.inf)
            plt.xlim((0, self.x))
            plt.ylim((0, self.y))

        else:
            import matplotlib.patches as mpatches
            import matplotlib.path as mpath

            plt.xlim((0, self.x))
            plt.ylim((0, self.y))

            bound_verts = [(0, 0), (0, self.y), (self.x, self.y), (self.x, 0), (0, 0)]

            bound_codes = [mpath.Path.MOVETO] + (len(bound_verts) - 1) * [mpath.Path.LINETO]
            poly_codes = [mpath.Path.MOVETO] + (len(self.interpolated_boundary) - 1) * [mpath.Path.LINETO]

            path = mpath.Path(bound_verts + self.interpolated_boundary[::-1].tolist(), bound_codes + poly_codes)
            patch = mpatches.PathPatch(path, facecolor='black', edgecolor='none')
            patch = ax.add_patch(patch)

        plt.savefig(demo_directory + "/evap_frame_" + str(self.output_frm) + ".png")
        plt.close(fig2)

    def plot_surface_dielectric(self, BP, BF, A, B, C, output_directory):

        """
        plot field solution over boundary elements

        """

        R = np.array([[0.0, -1.0], [1.0, 0.0]])

        Ne = np.matmul(R, (B - A).T).T
        Te = (B - A)

        Ne = Ne/(np.linalg.norm(Ne, axis = 1))[:, None]
        Te = Te/(np.linalg.norm(Te, axis = 1))[:, None]

        Ne = np.ascontiguousarray(Ne)
        Te = np.ascontiguousarray(Te)

        surf_mag = []
        #BFc = scipy.signal.medfilt(BF, 1)

        for a in range(0, len(C)):
            x = C[a, 0] + Ne[a, 0]  * 0.005
            y = C[a, 1] + Ne[a, 1]  * 0.005

            Ex, Ey = cBEM_cython.calculate_field(x * self.scale , y * self.scale, BP, BF, Ne * self.scale, Te * self.scale, A * self.scale, B * self.scale)
            surf_mag.append(np.sqrt(Ex**2.0 + Ey**2.0)/self.scale)

        surf_mag = np.array(surf_mag)

        plt.figure(figsize = (10, 10))
        plt.plot(BF[100:340] * self.scale, label = "surface normal flux")
        plt.plot(surf_mag[100:340] * self.scale, label = "surface field magnitude")
        plt.ylim((0.0, (np.max(BF[100:340])  * self.scale) * 1.1))
        plt.xlabel("surface position parameter (a.u.)", fontsize = "28")
        plt.ylabel(r"field flux/magnitude ($V nm^{-1}$)", fontsize = "28")
        legend = plt.legend(fontsize=28, loc='lower left')
        plt.setp(legend.get_title(),fontsize=28)
        plt.savefig(output_directory + "/surf_flux.png")
        plt.close()

    def plot_electric_field_solution(self, mesh_coors, connectivity_matrix, f_pot, s_pot, flux, A, B, contours = [], demo_directory = ''):

        """
        Function for plotting the electric field and potential from FEM-BEM coupling

        Created image is saved to directory provided 'demo_directory'.

        :param1 self: the level_set space object
        :param2 mesh_coors: list of FEM mesh coordinates
        :param3 connectivity_matrix: array of indices for triangles
        :param4 f_pot: FEM potentials (at mesh_coors)
        :param4 s_pot: surface potentials (for BEM)
        :param4 flux: surface normal flux (for BEM)
        :param4 A: lefthand corner of BEM elements
        :param4 B: righthand corner of BEM elements
        :param4 contours: potential contours
        :returns: 0

        """

        import matplotlib.tri
        import scipy.interpolate

        mg = 5.0

        print("Generating potential plot")

        grid_x, grid_y = np.mgrid[(0):self.x:(1.0j*int(self.x/mg)), 0:self.y:(1.0j*int(self.y/mg))]
        data = scipy.interpolate.griddata(mesh_coors, f_pot, (grid_x, grid_y))


        #calculate boundary element normals and tangents
        R = np.array([[0.0, -1.0], [1.0, 0.0]])

        Ne = np.matmul(R, (B - A).T).T
        Te = (B - A)

        Ne = Ne/(np.linalg.norm(Ne, axis = 1))[:, None]
        Te = Te/(np.linalg.norm(Te, axis = 1))[:, None]

        Ne = np.ascontiguousarray(Ne)
        Te = np.ascontiguousarray(Te)

        grid_x2, grid_y2 = np.mgrid[0:self.x:(1.0j*int(self.x * mg)), 0:self.y:(1.0j*int(self.y * mg))]
        data = scipy.interpolate.griddata(mesh_coors, f_pot, (grid_x2, grid_y2))

        Ex = -np.gradient(data)[0]
        Ey = -np.gradient(data)[1]

        for i in range(0, data.shape[0]):
            for j in range(0, data.shape[1]):
                if np.isnan(data[i, j]):
                    x = grid_x2[i, j]
                    y = grid_y2[i, j]
                    data[i, j] =  BEM.calculate_potential(x * self.scale, y * self.scale, s_pot, flux, A, B)
                    Ex[i, j], Ey[i, j] = cBEM_cython.calculate_field(x * self.scale, y * self.scale, s_pot, flux, Ne, Te, A, B)

        #output calculated field
        plt.figure(figsize = (10, 10))
        plt.imshow(np.log(data.T), extent=[0, self.x, 0, self.y], origin='lower')
        #plt.quiver(grid_x2, grid_y2, 4.0 * Ex/(Ex**2.0+ Ey**2.0)**0.5, 4.0 * Ey/(Ex**2.0+ Ey**2.0)**0.5, alpha = 0.7, scale = 500.0)

        #plot potential contours
        for c in contours:
            con_ext = measure.find_contours(data, c - 0.1)
            if len(con_ext) > 0:
                c1 = max(con_ext, key = len)
                plt.plot(np.hsplit(c1, 2)[0] / mg, np.hsplit(c1, 2)[1] / mg, c = 'red')

        cp = np.mean(mesh_coors[connectivity_matrix], axis = 1)

        triangulation = matplotlib.tri.Triangulation(np.hsplit(mesh_coors, 2)[0].flatten(), np.hsplit(mesh_coors, 2)[1].flatten(), connectivity_matrix[cp[:, 1] > -5.0])

        plt.triplot(triangulation, alpha = 0.5)
        print("potential solution")
        plt.xlim((0.0, self.x))
        plt.ylim((np.min(mesh_coors[:, 1]), self.y))
        plt.xlim((60.0, self.x))
        plt.ylim((100.0, self.y))

        #uncomment for zoomed in field solution
        #plt.xlim((25.0, self.x - 25.0))
        #plt.ylim((50.0, self.y))

        plt.xlabel("nm", fontsize = "30")
        plt.ylabel("nm", fontsize = "30")

        plt.xticks(fontsize = 24)
        plt.yticks(fontsize = 24)

        plt.savefig(demo_directory + "/potential_solution" + str(self.i) + ".png")
        plt.close()

        plt.plot(data[128, :])
        plt.savefig("pcs.png")
        plt.close()

    def plot_permittivity(self, mesh_coors, connectivity_matrix, c_constant, demo_directory = ''):

        """
        Plotting function for outputing 2D FEM mesh with element dielectric values.

        Created image is saved to directory provided 'demo_directory'.

        :param1 self: the level_set space object
        :param2 mesh_coors: FEM mesh coordinates
        :param3 connectivity_matrix: The mesh connectivity matrix
        :param4 c_constant: list of element dielectric values
        :returns: 0

        """

        c_constant = np.array(c_constant)
        c_constant[c_constant > 1e3] = 1e6
        fig2 = plt.figure(figsize = (8, 8))
        x = np.hsplit(mesh_coors, 2)[0].flatten()
        y = np.hsplit(mesh_coors, 2)[1].flatten()
        plt.tripcolor(x, y, connectivity_matrix, facecolors = np.array(c_constant), edgecolors='k')
        plt.xlim((0.0, self.x))
        plt.ylim((5.0, self.y))
        plt.savefig(demo_directory + "/dielectric_constant_" + str(self.output_frm) + ".png")
        plt.close(fig2)

    def plot_projection(self, features = [], demo_directory = ''):

        """
        Plotting function for outputing 2D simulation geometry with ion trajectories onto detector.l
        Created image is saved to directory provided 'demo_directory'.

        :param1 self: the level_set space object
        :param2 features: list of 'feature' objects for drawing output
        :param3 demo_directory: image output directory
        :returns: 0

        """

        fig2 = plt.figure(figsize = (50, 50))
        for f in features:
            if f.f_type == "vertical_layer":
                plt.fill_between(np.hsplit(self.interpolated_boundary, 2)[0].flatten(), np.hsplit(self.interpolated_boundary, 2)[1].flatten(), where = (np.hsplit(self.interpolated_boundary, 2)[0].flatten() >= f.fmin) & (np.hsplit(self.interpolated_boundary, 2)[0].flatten() <= f.fmax), facecolor = f.color, hatch = f.pattern)
            elif f.f_type == "horizontal_layer":
                plt.fill_betweenx(np.hsplit(self.interpolated_boundary, 2)[0].flatten(), np.hsplit(self.interpolated_boundary, 2)[1].flatten(), where = (np.hsplit(self.interpolated_boundary, 2)[1].flatten() >= f.fmin) & (np.hsplit(self.interpolated_boundary, 2)[1].flatten() <= f.fmax), facecolor = f.color, hatch = f.pattern, zorder = 10)
            elif f.f_type == "precipitate":
                plt.fill(f.fmin, f.fmax, color = f.color, hatch = f.pattern)
        plt.fill_between(np.concatenate(([0.0], np.hsplit(self.interpolated_boundary, 2)[0].flatten(), [self.x])), np.concatenate(([0.0], np.hsplit(self.interpolated_boundary, 2)[1].flatten(), [0.0])), self.y, facecolor = "black", zorder=math.inf)

        #plot 1D detector
        plt.plot([self.detector.p0x / self.scale, self.detector.p1x / self.scale], [self.detector.p0y / self.scale, self.detector.p1y / self.scale], linewidth=2)

        for ion in self.detector.projections:
            plt.plot(np.hsplit(ion.positions, 2)[0], np.hsplit(ion.positions, 2)[1])
        plt.savefig(demo_directory + "/ion_projection" + ".png")
        plt.close(fig2)

    def plot_projection_trunc(self, trajectories, phase, color_map, features = [], output_directory = '', steps = 10):

        """
        Plotting function for outputing 2D tip geometry with ion trajectories onto detector.
        Unlike plot_projection the method only plots the first few iterations of ion trajectories
        and ignores the detector such that the plot produced is much more local to the tip.

        Created image is saved to directory provided 'demo_directory'.

        :param1 self: the level_set space object
        :param2 features: list of 'feature' objects for drawing output
        :param3 demo_directory: image output directory
        :param4 n: The number of ion trajectory points to plot - remember trajectory calculation is performed via an adaptive integrator.
        :returns: 0

        """

        fig2 = plt.figure(figsize = (15, 15))
        plt.axis('equal')

        plt.xlim(0.0, self.x)
        plt.ylim(0.0, self.y)

        plt.xlabel("nm", fontsize = "36")
        plt.ylabel("nm", fontsize = "36")

        plt.xticks(fontsize = 28)
        plt.yticks(fontsize = 28)

        for f in features:
            if f.f_type == "vertical_layer":
                plt.fill_between(np.hsplit(self.interpolated_boundary, 2)[0].flatten(), np.hsplit(self.interpolated_boundary, 2)[1].flatten(), where = (np.hsplit(self.interpolated_boundary, 2)[0].flatten() >= f.fmin) & (np.hsplit(self.interpolated_boundary, 2)[0].flatten() <= f.fmax), facecolor = f.color, hatch = f.pattern)
            elif f.f_type == "horizontal_layer":
                plt.fill_betweenx(np.hsplit(self.interpolated_boundary, 2)[0].flatten(), np.hsplit(self.interpolated_boundary, 2)[1].flatten(), where = (np.hsplit(self.interpolated_boundary, 2)[1].flatten() >= f.fmin) & (np.hsplit(self.interpolated_boundary, 2)[1].flatten() <= f.fmax), facecolor = f.color, hatch = f.pattern, zorder = 10)
            elif f.f_type == "precipitate":
                plt.fill(f.fmin, f.fmax, color = f.color, hatch = f.pattern)
        plt.fill_between(np.concatenate(([0.0], np.hsplit(self.interpolated_boundary, 2)[0].flatten(), [self.x])), np.concatenate(([0.0], np.hsplit(self.interpolated_boundary, 2)[1].flatten(), [0.0])), self.y, facecolor = "black", zorder=20)

        #plot 1D detector
        plt.plot([self.detector.p0x / self.scale, self.detector.p1x / self.scale], [self.detector.p0y / self.scale, self.detector.p1y / self.scale], linewidth=2)
        for t in range(0, len(trajectories)):
            if phase[t] != 0:
                plt.plot(np.hsplit(trajectories[t], 2)[0], np.hsplit(trajectories[t], 2)[1], zorder = 30, c = color_map[phase[t]])
                #plt.plot(np.hsplit(trajectories[t], 2)[0], np.hsplit(trajectories[t], 2)[1], zorder = 30, c = "white")
        plt.savefig(output_directory)
        plt.close(fig2)

    ###anisotropy function modifying evaporation field
    def anisotropy(self, F0, norms, gamma, delta, grain_ind):

        """
        Anisotropy function for modifying F0 to handle anisotropic material

        :param1 F0: the 'isotropic' field evaporation value
        :param2 norms: the tip surface normals
        :param3 gamma: the anisotropic weighting factor
        :param4 delta: the isotropic weighting factor
        :param5 grain_ind: A dictionary of dictionaries associated with different crystal grains
        :returns: anisotropic field evaporation values at tip surface

        """

        #must identify the closest fundamental lattice plane
        F0m = np.array([])
        for el in range(0, len(grain_ind)):

            grain_id = grain_ind[el]
            grain = self.grain_dict[grain_id]

            if grain['amorphous'] == False:
                anisotropic_field_evap = grain['f']

                #calculate angles to different facets
                ang = np.arccos(np.dot(anisotropic_field_evap, norms[el])/np.linalg.norm(anisotropic_field_evap, axis = 1))

                F0m = np.append(F0m, delta * (abs(np.tan(np.min(ang))) * F0[el]) + gamma * np.linalg.norm(anisotropic_field_evap, axis = 1)[np.argmin(ang)])
            else:
                F0m = np.append(F0m, delta * F0[el])

        return F0m

    #perform iteration - mod_field is included to allow for higher order schemes
    def iterate(self, previous_field, dt, a, h, extension_velocity, sigma = 1.0, mod_field = 0.0, dynamic = True, shank = True, CFL = 0.2):


        """
        Performs single field iteration via a 1st order Godunov's scheme.

        param1 dt: the timestep. only used if dynamic = False.
        param2 h: the spatial scaling factor.
        param3 extension_velocity: The constructed velocity field V honoring the condition del[phi] \cdot del[V] = 0.
        param4 sigma: the var for the gaussian filtering step.
        param5 mod_field: a field addend for implementing higher order time integrators.
        param5 dymamic: Whether the timestep should be calculated dynamically. If true then dt is ignored.
        :returns: list containing - the iterated level-set field, extension_velocity, for_grad, bac_grad, dt

        """

        #implement upwind scheme
        forward_x = np.dot(self.for_op, self.two_dim_grid_field.T).T
        forward_y = np.dot(self.for_op, self.two_dim_grid_field)
        backward_x = np.dot(self.back_op, self.two_dim_grid_field.T).T
        backward_y = np.dot(self.back_op, self.two_dim_grid_field)

        #approximate forward gradient and backward gradient via
        bac_grad = ((np.maximum(forward_x, 0.0)**2.0  + np.minimum(backward_x, 0.0)**2.0) + (np.maximum(forward_y, 0.0)**2.0  + np.minimum(backward_y, 0.0)**2.0))**0.5
        for_grad = ((np.maximum(backward_x, 0.0)**2.0 +  np.minimum(forward_x, 0.0)**2.0) + (np.maximum(backward_y, 0.0)**2.0 +  np.minimum(forward_y, 0.0)**2.0))**0.5

        #for_grad = gaussian_filter(for_grad, sigma = 2.0)
        #bac_grad = gaussian_filter(bac_grad, sigma = 2.0)

        #define velocity function (from level-set definition) - first order upwind scheme
        dphi = np.multiply(np.maximum(extension_velocity, 0.0), bac_grad) + np.multiply(np.minimum(extension_velocity, 0.0), for_grad)

        self.dphi = dphi + mod_field
        self.for_grad = for_grad
        self.back_grad = bac_grad

        #calculate dynamic timestep - calculated via fraction of the CFL condition
        if dynamic == True:
            dt = h/np.max(abs(extension_velocity)) * CFL #CFL condition over narrowband
            if self.time + dt >= self.fr_time * self.output_frm:
                dt = self.fr_time * self.output_frm - self.time
                self.output_frame = True

        #perform iteration
        grid_field_it = sp.zeros((self.x, self.y))

        #iterate the level-set field
        grid_field_it = previous_field + 1.0 * a * dt * self.dphi

        return [grid_field_it, extension_velocity, for_grad, bac_grad, dt]

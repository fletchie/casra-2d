#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 19:07:12 2018

Otimised extension velocity construction that uses vectorised functions 
although not as sophisticated algorithmically as the fast marching method (higher complexity)

@author: charlie
"""

import numpy as np

def ext_vel(space, bv, boundary, two_dim_grid, h = 1):
    
    """
    Extension velocity construction based off calculating the velocity at the closest point on the front.
    This makes use of the result del[phi] . del[ext] = 0 to ensure the maintanance of the signed distance field.
    This method only returns the closest point velocity and does not interpolate the velocity over the front - see `interp_ext_vel`.
    
    :param1 space: the level-set space object
    :param2 bv: the boundary velocity
    :param3 boundary: The interpolated boundary coordinates (returned by marching squares)
    :param4 two_dim_grid: The discretised tip (based of local level-set field sign value)
    :param5 h: simulation spatial scale parameter
    :returns: the uninterpolated extension velocity field
    
    """
    
    #perform vectorised distance calculation
    coords = np.vstack((np.tile(np.arange(0, space.x, 1), space.y), np.repeat(np.arange(0, space.y, 1), space.x))).T
        
    coords_tiled = np.transpose(np.repeat(coords.reshape(1, coords.shape[0], coords.shape[1]), len(boundary), axis = 0), axes = (1, 0, 2))
    bem_repeat = np.repeat(boundary.reshape(1, boundary.shape[0], boundary.shape[1]), len(coords), axis = 0)
        
    dist_mat = np.linalg.norm(coords_tiled - bem_repeat, axis = 2)
    
    #this is currently an approximation - interpolation is required between two closest boundary edges
    vals = np.argmin(dist_mat, axis = 1) 
    
    #estimate extension velocity considering that del[phi] . del[ext] = 0
    ext_vel = bv[vals].reshape((space.x, space.y)).T
    
    return ext_vel

#extension velocity construction including velocity interpolation across the front
def interp_ext_vel(space, bv, boundary, h = 1):
    
    """
    Extension velocity construction based off calculating the velocity at the closest point on the front.
    This makes use of the result del[phi] o del[ext] = 0 to ensure the maintanance of the signed distance field.
    This method interpolates the velocity over the front, ensuring a stricter adherence to del[phi] o del[ext] = 0.
    
    :param1 space: the level-set space object
    :param2 bv: the boundary velocity
    :param3 boundary: The interpolated boundary coordinates (returned by marching squares)
    :param4 two_dim_grid: The discretised tip (based of local level-set field sign value
    :param5 h: simulation spatial scale parameter
    :returns: the interpolated extension velocity field
    
    """
    
    #perform vectorised distance calculation
    coords = np.vstack((np.tile(np.arange(0, space.x, 1), space.y), np.repeat(np.arange(0, space.y, 1), space.x))).T
        
    coords_tiled = np.transpose(np.repeat(coords.reshape(1, coords.shape[0], coords.shape[1]), len(boundary), axis = 0), axes = (1, 0, 2))
    bem_repeat = np.repeat(boundary.reshape(1, boundary.shape[0], boundary.shape[1]), len(coords), axis = 0)
        
    dist_mat = np.linalg.norm(coords_tiled - bem_repeat, axis = 2)
    
    #this is currently an approximation - interpolation is required between two closest boundary edges
    #vmax - closest boundary index
    #vmin - second closest boundary index
    vmin, vmax = np.hsplit(np.argpartition(dist_mat, kth = 2, axis = 1)[:,1::-1], 2)
    
    vmax = vmax.flatten()
    vmin = vmin.flatten()
    
    vb = (vmax > vmin) * vmax + (vmax < vmin) * vmin
    va = (vmax < vmin) * vmax + (vmax > vmin) * vmin
    
    B = boundary[vb]
    A = boundary[va]
    
    AB = B - A
    OB = B - coords
    OA = A - coords

    sb = np.sum(np.multiply(AB, OB), axis = 1)/np.linalg.norm(AB, axis = 1)
    sa = np.sum(np.multiply(AB, OA), axis = 1)/np.linalg.norm(AB, axis = 1)
    
    l = sb - sa
    
    #estimate extension velocity considering that del[phi] . del[ext] = 0
    ext_vel = bv[vmax] * ((abs(l) >= 1.0)) + (bv[vb] * l + bv[va] * (1.0 - l)) * ((abs(l) < 1.0))
 
    ext_vel = ext_vel.reshape((space.x, space.y)).T
    
    return ext_vel
    
    
    

    
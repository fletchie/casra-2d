import csv
import argparse
import matplotlib.pyplot as plt
import scipy.special
import numpy as np

#simulation parameter initialisation
parser = argparse.ArgumentParser(description='Output collapse test metrics.')

parser.add_argument('--input', metavar='input', type=str, default = 'output/collapse_test.csv',
                    help='The input collapse test data - space separated.')

parser.add_argument('--timesteps', metavar='timesteps', type=str, default = '0.5',
                    help='The list of corresponding timesteps - space separated.')

args = parser.parse_args()

filenames = args.input.split(",")
timesteps = args.timesteps.split(",")

ig, ax = plt.subplots()

for i in range(0, len(filenames)):
    timestamp = []
    area = []
    surface_field = []
    surface_velocity = []
    variance = []

    with open(filenames[i]) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            timestamp.append(float(row[0]))
            variance.append(float(row[4]))

    h, = ax.plot(timestamp, np.log(variance), label = timesteps[i])

ax.ticklabel_format(useOffset=False)

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
plt.xlabel("collapse time (au)")
plt.ylabel("variance from circle (log)")
plt.savefig("radius_variance.png")

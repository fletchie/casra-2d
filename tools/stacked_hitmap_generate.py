import csv
import argparse
import matplotlib.pyplot as plt
import scipy.special
import numpy as np
import sys

try:
    from colour import Color
except ModuleNotFoundError:
    print("Cannot find colour package. Please install latest version.")
    print('Program terminated!')
    sys.exit()


#simulation parameter initialisation
parser = argparse.ArgumentParser(description='Hit-map generator script.')

parser.add_argument('--input', metavar='input', type=str, default = 'output/collapse_test.csv',
                    help='The input projection data - comma separated.')

parser.add_argument('--bins', metavar='bins', type=int, default = 100,
                    help='Number of histogram bins')

parser.add_argument('--colors', metavar='colors', type=str, default = "red",
                    help='Phase colors - comma separated')

parser.add_argument('--height', metavar='height', type=float, default = 50.0,
                    help='hitmap height')

parser.add_argument('--output', metavar='output', type=str, default = 'output',
                    help='The output directory to save the generated hitmap')

args = parser.parse_args()

filenames = args.input.split(",")
bin_num = args.bins
colors = args.colors.split(",")
height = args.height
output = args.output

for i in range(0, len(filenames)):

    hits = []
    species = []

    with open(filenames[i]) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            hits.append(float(row[0]))
            species.append(float(row[1]))

    hits = np.array(hits)
    species = np.array(species)
    species_types = np.unique(species)
    species_bins = []
    accumulate = []

    for s in range(0, len(species_types)):
        accumulate.append(hits[species == species_types[s]])
    print(len(hits))
    plt.figure(figsize = (10, 10))
    N, bins, patches = plt.hist(accumulate, stacked = True, color = colors, range = (0.0, 1.0), bins = bin_num)
    plt.xlabel("detector coordinates (normalised)", fontsize = 36)
    plt.ylabel("hit count", fontsize = 36)
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, height)
    plt.xticks(fontsize=28, rotation=0)
    plt.yticks(fontsize=28, rotation=0)
    plt.savefig(output + "/hitmap" + str(i) + ".png")

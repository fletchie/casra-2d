#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sept 12 2018

Method for opening and output reconstruction mapping

@author: charlie
"""

import argparse
import pickle
import numpy as np
import sys
import scipy
import os
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.interpolate import splev

parser = argparse.ArgumentParser(description='Open reconstruction mapping.')

parser.add_argument('--input', metavar='input', type=str, default = 'output/mapping.p',
                    help='The input pickled mapping dictionary object')

parser.add_argument('--out', metavar='output', type=str, default = 'output',
                    help='The output directory')

parser.add_argument('--FOVmin', metavar='FOV left', type=float, default = 0.3,
                    help='The detector parameter value (based of width) where the field of view (FOV) starts.')

parser.add_argument('--FOVmax', metavar='FOV right', type=float, default = 0.7,
                    help='The detector parameter value (based of width) where the field of view (FOV) ends.')

parser.add_argument('--isonum', metavar='output', type=int, default = 30,
                    help='The number of isolines to plot')


args = parser.parse_args()

#parameter definition
mapping_filename = args.input
output_directory = args.out
isonum = args.isonum
fovmin = args.FOVmin
fovmax = args.FOVmax

#create output directory if does not exist
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

print("output directory: " + os.getcwd() +  "/" + output_directory)

mapping = pickle.load(open(mapping_filename, "rb"))

def plot_detector_flowlines(mapping, sample_points, demo_directory = ''):

    """
    Generates and plots detector isolines (constant detector coordinates) for the evolution of the reconstruction mapping.

    :param1 self: The level-set space object instance
    :param2 sample_points: The detector coordinates at which to take the isolines
    :param3 demo_directory: The output demo_directory
    :returns: 0

    """

    fig3 = plt.figure(figsize = (20, 20))


    flowline_list = [[] for _ in range(len(sample_points))]

    for m_ind in mapping:
        sample_space = mapping[m_ind]['sample_space']
        det_space = mapping[m_ind]['detector_space']

        vx = []
        vy = []

        for sp in sample_points:

            #find closest 3 points to sample point
            sample_space = np.array(sample_space)
            det_space = np.array(det_space)

            ind = np.argsort(sp - det_space)
            ind1 = ind[(sp - det_space)[ind] >= 0.0][0]

            ind3 = ind[(sp - det_space)[ind] >= 0.0][1]
            ind2 = ind[(sp - det_space)[ind] < 0.0][-1]
            ind4 = ind[(sp - det_space)[ind] < 0.0][-2]

            ind = [ind1, ind2, ind3, ind4]

            pp = np.polyfit(det_space[ind], sample_space[ind], deg = 1)
            val = np.polyval(pp, sp)

            vx.append(val[0])
            vy.append(val[1])

        tip_surface = mapping[m_ind]['tip_boundary']

        for i in range(0, len(sample_points)):
            pos = np.array([vx[i], vy[i]])
            flowline_list[i].append(pos)

        #plot surfaces
        plt.plot(np.hsplit(tip_surface, 2)[0], np.hsplit(tip_surface, 2)[1], c = 'blue', linewidth = 3.0)

    #plot detector isolines
    for i in range(0, len(flowline_list)):
        #print(len(np.hsplit(np.array(flowline_list[i]), 2)[0].flatten()), len(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()))
        f = scipy.interpolate.interp1d(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten(), np.hsplit(np.array(flowline_list[i]), 2)[0].flatten(), kind='linear')
        plt.plot(f(np.linspace(np.max(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), np.min(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), 1000)), np.linspace(np.max(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), np.min(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), 1000), c = 'red', linewidth = 3.0)

	

        #tck = scipy.interpolate.splrep(np.hsplit(np.array(flowline_list[i]), 2)[0].flatten(), np.hsplit(np.array(flowline_list[i]), 2)[1].flatten(), k = 3, per = False, s = 2.0)
        #yint = scipy.interpolate.splev(np.hsplit(np.array(flowline_list[i]), 2)[0].flatten(), tck, der=0, ext=0)

        #spl = scipy.interpolate.UnivariateSpline(-np.hsplit(np.array(flowline_list[i]), 2)[1].flatten(), np.hsplit(np.array(flowline_list[i]), 2)[0].flatten(), s = 0.0)
        #plt.plot(spl(-np.linspace(np.max(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), np.min(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), 1000)), np.linspace(np.max(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), np.min(np.hsplit(np.array(flowline_list[i]), 2)[1].flatten()), 1000), c = 'red')
    
    plt.xlim(41.0, 86.0)
    plt.ylim(84.0, 124.0)
    plt.xlabel("nm", fontsize = 56)
    plt.ylabel("nm", fontsize = 56)   
    plt.xticks(fontsize=40, rotation=0)
    plt.yticks(fontsize=40, rotation=0)
    plt.savefig(output_directory + "/mapping" + ".png")
    plt.close(fig3)

#output flowline map
sample = isonum
sample_points = np.linspace(fovmin, fovmax, sample)
plot_detector_flowlines(mapping, sample_points, demo_directory = 'output')

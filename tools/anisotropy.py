#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 11:40:26 2018

Tool for plotting particular grain anisotropy functions given the materials_db and materials_key

@author: charlie
"""

import numpy as np
import matplotlib.pyplot as plt
import sys
from math import log10, floor
import argparse
import pandas as pd
import sqlite3
import matplotlib.ticker as ticker

parser = argparse.ArgumentParser(description='plot anisotropy functions.')

parser.add_argument('--mat_db', metavar='mat_db', type=str, default = 'materials_db/materials',
                    help='The inputted materials database file')

parser.add_argument('--mat_key', metavar='mat_key', type=str, default = 'input/materials_key',
                    help='The inputted materials key file')

parser.add_argument('--labels', metavar='labels', type=str, default = '',
                    help='legend labels')

parser.add_argument('--out', metavar='output', type=str, default = 'output',
                    help='The output directory')

args = parser.parse_args()

#parameter definition
mat_db = args.mat_db
mat_key = args.mat_key
output = args.out
leg_labels = args.labels
if len(leg_labels) != 0:
    leg_labels = leg_labels.split(",")
else:
    leg_labels = [""]
def round_sig(x, sig=2):
    return round(x, sig-int(floor(log10(abs(x))))-1)

###anisotropy function modifying evaporation field
def plot_anisotropy(ax, F0, facets = [[1.0, 0.0, 1.0], [-1.0, 0.0, 1.0], [0.0, 1.0, 1.0], [0.0, -1.0, 1.0], [1.0, 1.0, 1.0], [1.0, -1.0, 1.0], [-1.0, 1.0, 1.0],
[-1.0, -1.0, 1.0]], color = "green", ax_control = True):

    """

    Anisotropy function for modifying F0 to handle anisotropic material

    :param1 F0: the 'isotropic' field evaporation value
    :param2 norms: the tip surface normals
    :param3 gamma: the anisotropic weighting factor
    :param4 delta: the isotropic weighting factor
    :param5 grain_ind: A dictionary of dictionaries associated with different crystal grains
    :returns: anisotropic field evaporation values at tip surface

    """

    #must identify the closest fundamental lattice plane
    F0m = np.array([])

    angles = np.linspace(0.0, 2.0 * np.pi, 1000, endpoint = False)

    angng = []

    for ang in angles:

        #calculate angles to different facets
        a = []

        for f in facets:
            v1 = (np.arctan2(f[0], f[1]) + 2.0 * np.pi) % (2.0 * np.pi)
            v2 = ((v1 - ang) + np.pi) % (2.0 * np.pi) - np.pi
            a.append(v2)

        l = np.min(abs(np.array(a)))
        evap = facets[np.argmin(abs(np.array(a)))][2]
        angng.append(l)

        F0m = np.append(F0m, (F0 * (abs(np.tan(l))) + evap) * 1e-9)

    ax.plot(angles, F0m, color = color)
    ax.grid(True)
    if ax_control == True:
        ax.set_rmax(np.max(F0m) * 1.12)
        ax.set_rmin(np.min(F0m) * 0.9)
        ax.set_rticks([round_sig(np.min(F0m), 2), round_sig((np.min(F0m) + np.max(F0m))/2.0, 2), round_sig(np.max(F0m), 2)])  # less radial ticks
        ax.set_rlabel_position(-22.5)  # get radial labels away from plotted line
        plt.rcParams["font.weight"] = "bold"
        plt.xticks(fontsize=30, rotation=0)
        plt.yticks(fontsize=26, rotation=0)

    frmtr = ticker.FormatStrFormatter('%d $V nm^{-1}$')
    ax.yaxis.set_major_formatter(frmtr)


    return angng, F0m

#connect to SQLite database
conn = sqlite3.connect(mat_db)
c = conn.cursor()

#read materials Key
materials_key = pd.read_csv(mat_key, sep = '\t', comment = '#')

plt.figure(figsize = (10, 10))
ax = plt.subplot(111, projection='polar')

for i in range(0, len(materials_key)):
    facets = c.execute('SELECT nx, ny, facet_evaporation_field FROM crystallography where mat_id = ' + str(materials_key['mat_id'][i])).fetchall()
    if len(facets) != 0:
        facets = np.array(facets)
        field_evap = c.execute('SELECT evaporation_field FROM main where mat_id = ' + str(materials_key['mat_id'][i])).fetchone()[0]
        angle = materials_key['grain_rot'][i]
        facets_rot = np.hstack((np.matmul(np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]]), facets[:, :2].T).T, np.array([facets[:, 2]]).T))
        angng, F0m = plot_anisotropy(ax, field_evap, facets_rot, color = materials_key['color'][i], ax_control = True)

plt.savefig(output + "/anisotropy.png")
plt.close()

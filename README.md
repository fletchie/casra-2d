# Continuum APT Simulation and Reconstruction Aid (CASRA)

CASRA is a 2D simulation tool for modelling sample field evaporation in Atom Probe Tomography (APT). It also serves as a proof-of-concept for how a continuum model could drive a new reconstruction algorithm capable of correcting many of the distortions currently observed in APT reconstructions.

# Initial Setup and Current System Compatibility

This tool is best suited to running on a Unix-based system, with the setup process outlined below assuming Ubuntu. The simulation tool has also been successfully compiled using the GNU g++ compiler and tested on the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about), although an xserver is required for rendering the simulation graphical outputs (e.g. [xming](https://sourceforge.net/projects/xming/)).

Testing CASRA on Windows using the Microsoft Visual Studio c++ compiler resulted in surface instabilities appearing in the simulated tip evaporations. **Therefore as of current the simulation tool appears to fail on Windows systems**. The aim is to track down and resolve these instability issues in future updates.  

## Python setup
In order to run the program, Python 3.6 or above must be installed. In order to setup the tool a number of prerequisite packages must be installed. These include:

* scipy
* pandas
* skimage
* matplotlib
* cython
* color
* sqlite3

Such packages can be installed via the pip package manager. Finally, in order to run dielectric simulations the command line program `Triangle` must be installed in order to construct the internal mesh. This can be obtained from https://www.cs.cmu.edu/~quake/triangle.html or via a package manager. This is most straightforward when using Unix based system. For Ubuntu for example...

```{bash}
sudo apt install triangle-bin
```

## Cython Compilation

By default the Cython has not been previously compiled (As the exact compilation varies between machine and OS). In order to achieve the compiled accelerations reported, the Cython scripts will require compilation. Again this will be most straightforward when using a system such as Ubuntu. This compilation is achieved by running the setup.py file when in the python/level_set_model directory (see command below).  

```{bash}
python3 core/cython/setup.py build_ext --inplace
```

This command will generate the compiled Cython within the correct directories.

**Note** that currently these Cython components must be compiled for the tool to run at all. However, alternative numpy methods already exist in BEM.py and FEM.py. Future versions of this tools will automatically switch to use these if the compiled Cython cannot be detected. 

## Sample Geometry

The sample geometry is inputted as an image file. The exact formats allowed are determined by the image loading function scipy.imread. Different phases within the sample geometry are color coded and are matched to different phases/grains defined within the [materials colour key file](#materials-colour-key). Finally, two different operation modes exist depending on the exact geometry initialised:

* Shank Mode - where the input sample geometry intersects the image edge. The exact shank geometry appended depends on the [shank parameters](#global-parameters).
* Detached Mode - where the input sample geometry is a closed form detached from the image edge.   

The image input file is passed via the [-tip_bm](#command-line-arguments) command line argument.

## Command Line Arguments

A number of program arguments are passed via the command line. The possible arguments, their function, and their default values are listed below:

```{bash}

optional arguments:
  -h, --help            show this help message and exit
  --tip_bm geom         The input tip geometry bitmap filename
  --bm_key bm_key       The materials key filename
  --mat_db db           The materials db filename
  --global_parameters global
                        The filename for a TSV file containing global
                        simulation parameters relating to physics and geometry
  --res res             Factor by which to scale down initialised tip geometry
                        for simulation. Allows sub-grid interpolation on initialisation.
  --out out             The output directory
  --it it               Number of level-set field iterations to perform before termination
  --frm frm             Number of output frames to perform before termination
  --reinit reinit       Number of iterations after which to perform field
                        reinitialisation. Default is never.
  --proj proj           Perform ion projection
  --dielectric_mode dielectric_mode
                        Simulate dielectric materials. If "dynamic" selected,
                        iterator will select BEM or FEM-BEM based on
                        intersecting dielectric material with tip surface.
  --mesh_output mesh_output
                        Output the finite element mesh with element dielectric
                        values. Only outputs if dielectric_mode is true.
  --profiler profiler   Profile the level-set iteration step. Should return an
                        output file that can be viewed using Snakeviz.
  --proj_frame profiler
                        Project ions at this frame and output to filename
                        "ion_projection_frm.png" within the output directory.
  --area_estimate area estimate
                        Estimate area per frame and output with time to
                        collapse.tsv within the output directory.
  --cbem_unit_test Constant BEM unit test
                        Execute unit test for constant BEM implementation on
                        2D circle geometry. Compare calculated surface field
                        to theoretical solution.
  --lbem_unit_test Linear BEM unit test
                        Execute unit test for linear BEM implementation on 2D
                        circle geometry. Compare calculated surface field to
                        theoretical solution.
  --integrator Time integrator
                        Time integrator for level-set. Choices are "Heun" or
                        "Euler".
  --proj_area Projection Area Fraction
                        The amount of simulated sample area evaporated between
                        ion projections


```

*note: Huen integrator currently inactive*

## Materials Colour Key

The key matching materials within the [materials database](#materials-database) to the sample input (passed via [-tip_bm](#command-linear-aguments)) is contained within the global parameters file (passed via the [-bm_key](#command-linear-aguments) argument). Such a file has tsv structure.

| r        |      g        |  b    | mat_id| grain_rot| color |
|----------|:-------------:|------:|------:|---------:|------:|
|red channel value for input grain|green channel for input grain|blue channel for input grain|The corresponding [material id](#materials-database) for a grain with such (r, g, b) channel values|Clockwise rotation of any associated crystallographic structure. Default [1 0] orientation is simulation y-axis. |output color for particular grains| 

An example input is given below:

```{bash}  

#This is a tsv file matching SQL material keys and its grain rotation to (r, g, b) colors
#Due to how the visualisaion works, any embedded structures must come after the definition of the bulk within the table
#grain rotation is defined in degrees

r	g	b	mat_id	grain_rot	color
255	255	255	0	0	white
0	0	0	1	0	green
91	155	213	2	22.5 	blue

```
## Global Parameters 

Additional simulation parameters are contained within a global parameters tsv structured file (passed via [-global_parameters](#command-line-arguments)). A possible file structure, containing all possible global arguments and a brief description, is given below:  

```{bash}
#This is a file defining simulation parameters. Undefined values will resort to default values.

parameter	value	description

#global physics parameters
beta	40.0	temperature parameter in field evaporation law
amplitude	5e-2	amplitude parameter in field evaporation law
tip_pot	30000.0	The tip electric potential - in 2D values should be negative 
gamma	1.0	The angular dependent scaling constant within the anisotropic law. In future versions this will be temperature dependent
delta	1.0	The facet dependent scaling constant within the anisotropic law
field_normal	1	Base evaporation physics of electric field normal surface component (rather than field magnitude)
evaporation_law	1	law selection (0 - linear, 1 - Ahhrenius)
FR	1.03	The field reduction fraction
dynamic_voltage	1	Whether to use a floating or fixed electrode voltage
scale	1e-9	level-set grid cell width (simulation lengthscale)

#simulation parameters
CFL	0.05	The dynamic timestep faction
refinement	1	FEM mesh refinement (match to boundaries)

#initialisation
initial_iterations	0	initial iterations to smooth front

#output parameters
frame	15	The time constituting one simulation frame
output_frame	1	Output the current simulation geometry to .png

#instrument geometry parameters
detector_distance	500000000.0	The relative distance of the detector from the tip
detector_width	1000000000.0	The relative width of the detector

#numerical methods
BEM	0	The BEM implementation used in the FEM-BEM coupling (1 - linear bem, 0 - constant BEM) 

#mesh parameters
element_size	6.0	The maximum size of the FEM mesh elements 
element_angle	20.0	The minimum angle of FEM mesh corners

#projection parameters
mapping_update	5	The number of frames (see frame) after which the tip-detector mapping will be updated
projection_fraction	0.15	The fraction of the tip height above which ions will be projected
mapping_resolution	0.75	The number of ions projected per boundary element
projection_output	1	(0 or 1) Whether ion trajectories should be outputed (into parsed output directory)
plot_projection	1	whether to plot the ion trajectories from projection
plot_hitmap	1	whether to generate and plot hitmap from ion projection
projection_separation	0.5	The distance between ion projections

#termination parameters
termination_height	60.0	The maximum sample height at which the simulation will terminate, regardless of remaining iterations
```

## Materials Database 

The materials database contains the program's possible material phase inputs. This includes information regarding evaporation fields, crystallographic anisotropy, and dielectric parameters - all of which are required inputs for the program. The database structure is outlined below:

![database structure](https://gitlab.com/fletchie/casra/raw/master/wiki/db.jpg)

New materials can be added to the database by modifying the sqlite file through an appropriate editor e.g. sqlitestudio for Windows or SQLbrowser for Ubuntu. 

Future implementations might shift towards using a simpler text file-based phase parameter definitions.

# Running the Program

The program is run by calling `tip_simulation.py`. The program is executed via the following format:

```{bash}

python3 tip_simulation.py -argument1 arg1value -argument2 arg2value ...

```

In order to more quickly gain familiarity with the program, a number of different examples demonstrating the configuration procedure and various functionalities of this simulation tool are given below. These use the example geometries and databases stored in \input.

When running these examples the system directory should be set to python/level_set_model.

## Example 1 - Circle Collapse Test

The first test to perform is the collapse of a homogeneous circle geometry under its surface electrostatic field. This collapse can occur under two different collapse laws: a linear law or an exponential law. 

### linear collapse test

```{bash}
sh examples/circle_collapse_test/run_linear.sh
``` 

### exponential collapse test

**Note** The first few frames can take a while smoothing corners (due to very small timesteps)

```{bash}
sh examples/circle_collapse_test/run_exp.sh
``` 

The resulting area-time plots for the level-set and numerical solutions are outputed in examples/circle_collapse_test/collapse_test_linear.png or examples/circle_collapse_test/collapse_test_exp.png depending on the selected evaporation law. Such tests ensure that the program is correctly configured and performing the expected evaporation behaviour.   

Note that there is increased drift from the problem solution for the exponential collapse law. However, for the larger tip geometry such drift will be minimal and acceptable considering the continuum approximation.

## Example 2 - homogeneous tip collapse w. reconstruction mapping

Run the following command to perform a homogeneous tip collapse under an Ahhrenius exponential evaporation law. Run the following bash script in the terminal:

```{bash}
sh examples/homogeneous_tip_collapse/run.sh 
```

where run.sh contains the required command line arguments. The demo should collapse a homogeneous tip and project ions at regular intervals. From this ion projection information a reconstruction mapping will be produced and stored in a pickled python object. Finally, calling the following script will generate the reconstruction mapping at examples/homogeneous_tip_collapse/output/map.png.  

```{bash}
python3 tools/reconstruction_mapping.py --input examples/homogeneous_tip_collapse/output/mapping.p
```

## Example 3 - heterogeneous evaporation

This demo runs a low evaporation field multilayer and high evaporation field precipitate. Unlike the previous examples the simulation termination condition is set to a specific tip height (see [global parameters](../level_set_model/tool-setup/#global-parameters)). In this example ion projection has been disabled. 

```{bash}
sh examples/heterogeneous_tip_collapse/run.sh 
```

The frames of the simulated evaporation are outputted into examples/heterogeneous_tip_evaporation/output.

## Example 4 - faceted evaporation w. ion projection and hitmap

This demo simulates the evaporation of a homogeneous crystalline material w. ion projection at simulation frame 50 (see the [--proj_frame](../level_set_model/tool-setup/#command-line-arguments)) command line argument). From this projection data, a simulated 1D hit-map can be generated based on the detector position by calling the additional script python/level_set_model/stacked_hitmap_generate.py.

Initially run the simulation preset: 

```{bash}
sh examples/faceted_tip_collapse/run.sh
```

On completion this should return examples/faceted_tip_collapse/output/hit_map_data_50.csv containing the simulated detector hit data required to generate the hitmap. This can be passed to stacked_hitmap_generate.py in order to generate the hitmap.  


```{bash}
python3 tools/stacked_hitmap_generate.py --input examples/faceted_tip_collapse/output/hit_map_data_50.csv --output examples/faceted_tip_collapse/output --colors green --bins 40
```

The ion projection density over elements can be adjusted by changing the [mapping_resolution](../level_set_model/tool-setup/#global-parameters) global parameter. The [projection_fraction](../level_set_model/tool-setup/#global-parameters) changes the tip height fraction above which ion projection is performed. Global parameters are located in /examples/faceted_tip_collapse/global_parameters.


The anisotropy function of the phase can be plotted via tool/anisotropy.py by passing the materials key and materials database. To run the script:

```{bash}

python3 tools/anisotropy.py --mat_db input/materials --mat_key examples/faceted_tip_collapse/materials_key --out examples/faceted_tip_collapse/output

```

## Example 5 - grain boundary simulation

This example simulates a diagonally oriented grain boundary. The two grains are of the same crystal structure with a misorientation of 22.5 degrees. The simulation configuration is contained within examples/grain_boundary.

In order to run the simulation preset:

```{bash}
sh examples/grain_boundary/run.sh
```

Different misorientations can be investigated by changing the [grain_rot](../level_set_model/Tool-Setup#materials-colour-key) within examples/grain_boundary/materials_key. Note that only the mat_id = 4 has a defined crystalline structure.  

Again the anisotropy functions for the two grains can be plotted via tool/anisotropy.py by passing the materials key and materials database. To run the script:

```{bash}

python3 tools/anisotropy.py --mat_db input/materials --mat_key examples/grain_boundary/materials_key --out examples/grain_boundary/output

```

## Example 6 - dielectric simulation of a precipitate

This example demonstrates dielectric simulation via the FEM-BEM coupling. The simulation is run in dynamic mode [--dielectric_parameter](../level_set_model/Tool-Setup#command-line-arguments) 2 such that the FEM-BEM coupling is only performed on the dielectric material intersecting the interface. 

To run the simulation preset:

```{bash}
sh examples/dielectric_precipitate/run.sh
```

In this 2D constant BEM implementation the dielectric precipitate appears to increase the effective evaporation field. This behaviour differs to alternative FEM modelling schemes in 3D which imply a reduced effective evaporation field.

## Example 7 - finFET Evaporation w. reconstruction mapping

This configuration example is for simulation of the finFET geometry.

To run the simulation preset:

```{bash}
sh examples/finfet_evaporation/run.sh
```

The reconstruction mapping can also be generated

```{bash}
python3 tools/reconstruction_mapping.py --input examples/finfet_evaporation/output/mapping.p
```

## Example 8 - test system (benchmarking)

The final configuration example is for simulation of a test system running (A) pure BEM, (B) FEM-BEM, and (C) A hybrid selecting per iteration depending on whether a dielectric phase is intersecting the sample surface.

To run the simulation preset (A):

```{bash}
sh examples/finfet_evaporation/run_BEM.sh
```

To run the simulation preset (B):

```{bash}
sh examples/finfet_evaporation/run_FEM_BEM.sh
```

To run the simulation preset (C):

```{bash}
sh examples/finfet_evaporation/run_hybrid.sh
```

## Videos and Gifs

Videos of sample evaporation can be created using a tool such as ffmpeg e.g. 

```{bash}
ffmpeg -r 10 -f image2 -i 'output_folder/output_file_name_%d.png' 'sample_evaporation.mp4'
```

Gifs can be created using the [ImageMagick](http://www.imagemagick.org/script/index.php) tool.



#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 10:56:37 2017

Main method for running tip evaporation model

@author: charlie
"""

import sys

if sys.version_info[0:2] < (3, 6):
    raise Exception("Detected incompatible Python version. Please run using Python 3.5 or above. \n Program terminated.")

try:
    import numpy as np
except ModuleNotFoundError:
    print("Cannot find numpy package. Please install latest version of numpy")
    print("Program terminated")
    sys.exit()

import math
import time
import argparse
import os
import copy as cp
import pickle
import csv
import warnings

#simulation parameter initialisation
parser = argparse.ArgumentParser(description='Run the tip simulation.')

parser.add_argument('--tip_bm', metavar='geom', type = str, default = 'input/tip_geom_init_blank.tif',
                    help='The input tip geometry bitmap filename')

parser.add_argument('--bm_key', metavar='bm_key', type = str, default = 'input/materials_key',
                    help='The materials key filename')

parser.add_argument('--mat_db', metavar='db', type = str, default = 'input/materials',
                    help='The materials db filename')

parser.add_argument('--global_parameters', metavar='global', type = str, default = 'input/global_parameters',
                    help='The filename for a TSV file containing global simulation parameters relating to physics and geometry')

parser.add_argument('--res', metavar='res', type = int, default = 10,
                    help='Factor by which to scale down initialised tip geometry for simulation')

parser.add_argument('--out', metavar='out', type = str, default = 'output',
                    help='The output directory')

parser.add_argument('--it', metavar='it', type = int, default = math.inf,
                    help='Number of iterations to perform')

parser.add_argument('--frm', metavar='frm', type = int, default = math.inf,
                    help='Number of output frames to perform')

parser.add_argument('--reinit', metavar='reinit', type = int, default = math.inf,
                    help='Number of iterations after which to perform field reinitialisation. Default is never.')

parser.add_argument('--proj', metavar='proj', type = str, default = 'false',
                    help='Perform ion projection')

parser.add_argument('--dielectric_mode', metavar='dielectric_mode', type = str, default = 'dynamic',
                    help='Simulate dielectric materials. If "dynamic" selected, iterator will select BEM or FEM-BEM based on intersecting dielectric material with tip surface.')

parser.add_argument('--mesh_output', metavar='mesh_output', type = str, default = 'false',
                    help='Output the finite element mesh with element dielectric values. Only outputs if dielectric_mode is true.')

parser.add_argument('--profiler', metavar='profiler', type = str, default = 'false',
                    help='Profile the level-set iteration step. Should return an output file that can be viewed using Snakeviz.')

parser.add_argument('--proj_frame', metavar='profiler', type = int, default = math.inf,
                    help='Project ions at this frame and output to filename "ion_projection_frm.png" within the output directory.')

parser.add_argument('--area_estimate', metavar='area estimate', type = str, default = 'false',
                    help='Estimate area per frame and output with time to collapse.tsv within the output directory.')

parser.add_argument('--cbem_unit_test', metavar='Constant BEM unit test', type = str, default = 'false',
                    help='Execute unit test for constant BEM implementation on 2D circle geometry. For this give argument one of "yes", "true", or "1". Compare calculated surface field to theoretical solution.')

parser.add_argument('--integrator', metavar='Time integrator', type = str, default = 'Euler',
                    help='Time integrator for level-set. Choices are "Heun" or "Euler".')

parser.add_argument('--proj_area', metavar='Projection Area Fraction', type = float, default = 30.0,
                    help='The amount of simulated sample area evaporated between ion projections')



def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def str2mode(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return 1
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return 0
    elif v.lower() in ('dynamic', 'd', 'dyn', '2'):
        return 2
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

args = parser.parse_args()

#simulation parameter definition
tip_geometry_filename = args.tip_bm
materials_key_filename = args.bm_key
materials_db_filename = args.mat_db
global_parameters_filename = args.global_parameters
mag = args.res
output_directory = args.out
iterations = args.it
frames = args.frm
proj = str2bool(args.proj)
reinit = args.reinit
dielectric_mode = str2mode(args.dielectric_mode)
mesh_output = str2bool(args.mesh_output)
profiler = str2bool(args.profiler)
proj_frame = args.proj_frame
area_estimate = str2bool(args.area_estimate)
integrator = args.integrator.lower()
proj_area = args.proj_area

if integrator != "heun" and integrator != "euler":
    print("Invalid integrator selected. Please choose from  either 'Heun' or 'Euler'")
    print("terminating...")
    sys.exit()

current_dir = os.getcwd().split("/")[-1]

if current_dir != "casra":
    warnings.warn("Current working directory not detected as '.../casra'. Ignore if manually renamed. If not please change to this directory as otherwise program will likely crash.")

#create output directory if does not exist
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

print("output directory: " + os.getcwd() +  "/" + output_directory)

#check for unit tests
cbem_unit_test = str2bool(args.cbem_unit_test)

import matplotlib.pyplot as plt
import core.boundary_element_library.BEM as BEM

try:
     from scipy.ndimage.filters import gaussian_filter1d
     from scipy.ndimage.filters import gaussian_filter
except ModuleNotFoundError:
    print("Cannot find scipy package. Please install latest version of scipy")
    print("Program terminated")
    sys.exit()

try:
     from skimage import measure
except ModuleNotFoundError:
    print("please install latest version of skimage")
    sys.exit()

try:
    import pandas
except ModuleNotFoundError:
    print("Cannot find Pandas. Please install latest version.")
    print('Program terminated!')
    sys.exit()

try:
    import sqlite3
except ModuleNotFoundError:
    print("Cannot find sqlite3. Please install latest version.")
    print('Program terminated!')
    sys.exit()

###import tip_sim modules
try:
    import core.level_set as core
except ModuleNotFoundError:
    print("Cannot find level_set in core. Check all modules required can be found in core.")
    print(os.listdir())
    print('Program terminated!')
    sys.exit()

try:
    import core.projection as projection
except ModuleNotFoundError:
    print("Cannot find projection in core. Check all modules required can be found in core.")
    print('Program terminated!')
    sys.exit()

try:
    import core.feature as feature
except ModuleNotFoundError:
    print("Cannot find feature in core. Check all modules required can be found in core.")
    print('Program terminated!')
    sys.exit()

try:
    import core.tip_initialiser as tip_initialiser
except ModuleNotFoundError:
    print("Cannot find tip_initialiser in core. Check all modules required can be found in core.")
    print('Program terminated!')
    sys.exit()

try:
    import core.field_initialisation as initialisation
except ModuleNotFoundError:
    print("Cannot find field initialisation in core. Check all modules required can be found in core.")
    print('Program terminated!')
    sys.exit()


if cbem_unit_test:

    import core.unit_tests.unit_tests as unit_tests

    print("Testing constant BEM surface flux for circle geometry")

    if cbem_unit_test == True:
        unit_tests.cbem_circle()

    print("constant BEM unit test complete")
    print("terminating...")
    sys.exit()

if dielectric_mode == False:
    mesh_output = False

#check that input directories exist
if os.path.exists(tip_geometry_filename) == False:
    print("input tip geometry bitmap cannot be found. Please check input.")
    sys.exit()

if os.path.exists(materials_key_filename) == False:
    print("input bitmap-database key cannot be found. Please check input.")
    sys.exit()

if os.path.exists(global_parameters_filename) == False:
    print("No global parameters file detected. Using default values.")
    global_parameters_filename == ""

if os.path.exists(materials_db_filename) == False:
    print("input materials database cannot be found. Please check input.")
    sys.exit()

if os.path.isdir(output_directory) == False:
    os.makedirs(output_directory)

#method
print("Initialising simulation. This may take some time depending on the resolution of the initialisation image.")

domain = core.sim_space()
domain.start = time.time()

domain.init_boundary, F0, amorphous, material_visualisation, color_map, two_dim_grain_map, grain_dict, global_parameters, dielectric_constants, unique_dc, phase_map, col_grain_map = tip_initialiser.geom_init(tip_geometry_dir = tip_geometry_filename, materials_key_dir = materials_key_filename, materials_db_dir = materials_db_filename, global_parameters_filename = global_parameters_filename, dielectric_mode = dielectric_mode, mag = mag)
domain.x, domain.y = F0.shape
domain.features = []

#simulation scaling parameter
scale = global_parameters['scale']

for mat in range(0, len(material_visualisation)):
    lx = np.hsplit(material_visualisation[mat], 2)[0]
    ly = np.hsplit(material_visualisation[mat], 2)[1]
    domain.features.append(feature.feature(f_type = "precipitate", color = color_map[mat], pattern = None, fmin = lx.flatten(), fmax = ly.flatten()))

#control boundary interest - i.e. shank mode or detached collapse
#if extracted boundary intersecting with domain edge, append shank
#else assume detached
if np.sum(domain.init_boundary[:, 0] == 0) + np.sum(domain.init_boundary[:, 0] == domain.x) + np.sum(domain.init_boundary[:, 1] == 0) + np.sum(domain.init_boundary[:, 1] == domain.y):
    domain.boundary_intersect = True
else:
    domain.boundary_intersect = False



#assign shank parameters to simulation object
domain.n = global_parameters['n'][0]
domain.l = global_parameters['l'][0]
domain.solver_type = global_parameters['solver_type']
domain.iterative_tol = global_parameters['iterative_tolerance'][0]
CFL = global_parameters['CFL']
proj_num = 0
proj_calculated = False
last_proj = True

enclosed_area = [math.inf]
surface_field = []
surface_velocity = []
area_time_stamp = []
variance = []

domain.slv, domain.slr = BEM.initial_shank_vector(domain.init_boundary, ang_dist = 10)

#####level-set field initialisation
#append additional shank elements
if domain.boundary_intersect == True:
    domain.two_dim_grid_field = initialisation.initialisation(domain.init_boundary, domain.x, domain.y, domain.boundary_intersect)

    domain.two_dim_grid_field = domain.two_dim_grid_field - 1.5
    #plt.plot(domain.init_boundary[:, 0], domain.init_boundary[:, 1])

    domain.init_boundary = measure.find_contours(domain.two_dim_grid_field, 0.0)[0]
    A, B, C = BEM.prepare_BE(domain.init_boundary, domain.slv, domain.slr, sh = domain.n, l = domain.l)
    domain.two_dim_grid_field = initialisation.initialisation(A, domain.x, domain.y, domain.boundary_intersect)

else:
    domain.two_dim_grid_field = initialisation.initialisation(domain.init_boundary[:-1], domain.x, domain.y, domain.boundary_intersect)
    domain.two_dim_grid_field = domain.two_dim_grid_field - 1.5
    domain.init_boundary = measure.find_contours(domain.two_dim_grid_field, 0.0)[0]
    domain.two_dim_grid_field = initialisation.initialisation(domain.init_boundary[:-1], domain.x, domain.y, domain.boundary_intersect)

print("System size: " + str(domain.two_dim_grid_field.shape))

domain.two_dim_grid = np.zeros((domain.x, domain.y), dtype = np.int)
domain.two_dim_grid[domain.two_dim_grid_field >= 0.0] = 1

domain.init_Efield(global_parameters['amplitude'], global_parameters['beta'], F0, dielectric_constants = dielectric_constants, tip_pot = global_parameters['tip_pot'])
domain.init_anisotropy(global_parameters['gamma'], global_parameters['delta'], two_dim_grain_map, grain_dict)

domain.i = 0
domain.time = 0.0
domain.output_frm = 0
domain.fr_time = global_parameters['frame'][0]
domain.output_frame = True
domain.scale = scale

#set BEM mode - initially set to constant BEM
domain.mode = 0
domain.mapping = dict()

#central difference matrix
domain.cdm_op = domain.central_grad(1)
domain.for_op = domain.forwards_grad(1)
domain.back_op = domain.backwards_grad(1)

domain.voltage_curve = []
domain.voltage_curve_smooth = []
domain.it_time = [0.0]

domain.dynamic_voltage = global_parameters['dynamic_voltage']
domain.FR = global_parameters['FR']
domain.refinement = global_parameters['refinement']

#add detector unit
domain.detector = projection.detector(global_parameters['detector_width'] * domain.scale, 0.0 * domain.scale, global_parameters['detector_distance'] * domain.scale, (domain.x/2.0) * domain.scale, projection_space = [- 1.5 * global_parameters['detector_width'] * domain.scale, 1.5 * global_parameters['detector_width'] * domain.scale, 0.0, 2.0 * global_parameters['detector_distance'] * domain.scale])

uniform_projection = True

if profiler == True:
    import cProfile
    cProfile.run("domain.two_dim_grid_field, domain.two_dim_grid, ext_vel, for_grad, back_grad, dt  = domain.run_field_iterator(domain.two_dim_grid_field, dt = False, a = 1.0, h = 1.0, sigma = 1.0, dynamic = True, dielectric_mode = dielectric_mode, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = global_parameters['field_normal'][0], cBEMmode = global_parameters['BEM'][0], law = global_parameters['evaporation_law'])", "stats.prof_file")
    print("Profiling complete")
    print("terminating...")
    sys.exit()

ii = 0
#initial uniform evaporation to smooth surface
while ii < global_parameters['initial_iterations'][0]:
    if integrator == "euler":
        domain.two_dim_grid_field, domain.two_dim_grid, ext_vel, for_grad, back_grad, dt  = domain.run_field_iterator(domain.two_dim_grid_field, dt = False, a = 1.0, h = 1.0, sigma = 1.0, dynamic = True, dielectric_mode = 0, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = 1, cBEMmode = global_parameters['BEM'][0], law = "constant", uniform_evap_field = True, CFL = CFL)
#Huen's method implementation - broken?
    elif integrator == "heun":
        prev_field = cp.deepcopy(domain.two_dim_grid_field)
        two_dim_grid_field, two_dim_grid, ext_vel, for_grad, back_grad, dt  = domain.run_field_iterator(prev_field, dt = False, a = 1.0, h = 1.0, sigma = 1.0, dynamic = True, dielectric_mode = 0, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = global_parameters['field_normal'][0], cBEMmode = global_parameters['BEM'][0], law = "constant", uniform_evap_field = True, CFL = CFL)
        mod_field = np.multiply(np.maximum(ext_vel, 0.0), back_grad) + np.multiply(np.minimum(ext_vel, 0.0), for_grad)
        domain.two_dim_grid_field, domain.two_dim_grid, ext_vel, for_grad, back_grad, dtx = domain.run_field_iterator(prev_field, dt = dt, a = 0.5, h = 1.0, sigma = 1.0, mod_field = mod_field, dynamic = False, dielectric_mode = 0, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = global_parameters['field_normal'][0], cBEMmode = global_parameters['BEM'][0], law = "constant", uniform_evap_field = True, CFL = CFL)
        domain.two_dim_grid = np.zeros((domain.x, domain.y), dtype = np.int)
        domain.two_dim_grid[domain.two_dim_grid_field >= 0.0] = 1
    ii+=1

beginning_time = time.time()

#iterate level-set
while (domain.i < iterations) and (domain.output_frm < frames):

    #Euler integrator
    if integrator == "euler":
        domain.two_dim_grid_field, domain.two_dim_grid, ext_vel, for_grad, back_grad, dt  = domain.run_field_iterator(domain.two_dim_grid_field, dt = False, a = 1.0, h = 1.0, sigma = 1.0, dynamic = True, dielectric_mode = dielectric_mode, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = global_parameters['field_normal'][0], cBEMmode = global_parameters['BEM'][0], law = global_parameters['evaporation_law'], CFL = CFL)

    #Huen's method implementation - broken?
    elif integrator == "heun":
        prev_field = cp.deepcopy(domain.two_dim_grid_field)
        two_dim_grid_field, two_dim_grid, ext_vel, for_grad, back_grad, dt  = domain.run_field_iterator(prev_field, dt = False, a = 1.0, h = 1.0, sigma = 1.0, dynamic = True, dielectric_mode = dielectric_mode, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = global_parameters['field_normal'][0], cBEMmode = global_parameters['BEM'][0], law = global_parameters['evaporation_law'], CFL = CFL)
        mod_field = np.multiply(np.maximum(ext_vel, 0.0), back_grad) + np.multiply(np.minimum(ext_vel, 0.0), for_grad)
        domain.two_dim_grid_field, domain.two_dim_grid, ext_vel, for_grad, back_grad, dtx = domain.run_field_iterator(prev_field, dt = dt, a = 0.5, h = 1.0, sigma = 1.0, mod_field = mod_field, dynamic = False, dielectric_mode = dielectric_mode, mesh_area = global_parameters['element_size'][0], mesh_angle = global_parameters['element_angle'][0], unique_dc = unique_dc, field_normal = global_parameters['field_normal'][0], cBEMmode = global_parameters['BEM'][0], law = global_parameters['evaporation_law'], CFL = CFL)
        domain.two_dim_grid = np.zeros((domain.x, domain.y), dtype = np.int)
        domain.two_dim_grid[domain.two_dim_grid_field >= 0.0] = 1

    domain.time = domain.time + dt
    domain.it_time.append(domain.time)

    if domain.output_frm % proj_frame == 0 and (domain.output_frm != 0 and proj_frame != 0) and last_proj == True and uniform_projection == True:
        print("Performing uniform ion projection - generating hitmap")
        last_proj = False
        n = np.matmul(np.array([[0.0, -1.0], [1.0, 0.0]]), (domain.B - domain.A).T).T
        n = n/np.linalg.norm(n, axis = 1)[:, None]

        trajectories, hits, tphase, hphase = projection.ion_projection(domain.BV.reshape(len(domain.BV)), domain.BP.reshape(len(domain.BP)), domain.BF.reshape(len(domain.BF)), domain.A * domain.scale, domain.B * domain.scale, domain.C * domain.scale, n, domain.detector, two_dim_grain_map, sys_time = domain.time, projection_fraction = global_parameters['projection_fraction'][0], dl = global_parameters['projection_separation'][0] * domain.scale, domain_scale = domain.scale)

        if global_parameters["plot_projection"][0] == True:
            domain.plot_projection_trunc(trajectories, tphase, col_grain_map, features = domain.features, output_directory = output_directory + '/ion_projection_' + str(domain.output_frm) + '.png', steps = 14)

        if global_parameters["plot_hitmap"][0] == True:

            #output hit-map data to .csv
            print("writing hit-map data to .csv")

            output_data = []

            data = open(output_directory + '/hit_map_data_' + str(domain.output_frm) + '.csv', 'w')

            with data:
               writer = csv.writer(data)
               for a in range(0, len(hits)):
                   output_data.append([hits[a], hphase[a]])
               writer.writerows(output_data)
            print("projection iteration: " + str(domain.i))
            print("finished writing")

    if (domain.i % reinit == 0 and domain.i != 0):
        print("reinitialization...")
        if domain.boundary_intersect == True:
            tA, tB, tC = BEM.prepare_BE(domain.interpolated_boundary, domain.slv, domain.slr, sh = 2, l = domain.l)
        else:
            tA = domain.interpolated_boundary
        domain.two_dim_grid_field = initialisation.initialisation(tA, domain.x, domain.y, domain.boundary_intersect)
        domain.two_dim_grid = np.zeros((domain.x, domain.y), dtype = np.int)
        domain.two_dim_grid[domain.two_dim_grid_field >= 0.0] = 1

    if len(domain.contours) > 1:
        print(domain.contours)
        print("More than one contour detected! Likely caused by tip collapse regime no longer being valid.")
        print("terminating...")
        sys.exit()

    if (np.sum(np.hsplit(domain.interpolated_boundary, 2)[1].flatten() > global_parameters['termination_height'][0]) == 0) and (domain.boundary_intersect == True):
        print("Termination height condition reached")
        break

    #perform projection mapping onto detector - if present
    area = 0.5 * np.abs(np.dot(domain.interpolated_boundary[:, 0], np.roll(domain.interpolated_boundary[:, 1], 1)) - np.dot(domain.interpolated_boundary[:, 1], np.roll(domain.interpolated_boundary[:, 0], 1)))
    if hasattr(domain, 'detector') and (abs(area - enclosed_area[-1]) > proj_area) and (proj == True):

        print("constructing projection mapping")

        n = np.matmul(np.array([[0.0, -1.0], [1.0, 0.0]]), (domain.B - domain.A).T).T
        n = n/np.linalg.norm(n, axis = 1)[:, None]

        mapping, trajectories = projection.derive_mapping(domain.BV.reshape(len(domain.BV)), domain.BP.reshape(len(domain.BP)), domain.BF.reshape(len(domain.BF)), domain.A * domain.scale, domain.B * domain.scale, domain.C * domain.scale, n, domain.detector, dielectric_constants, sys_time = domain.time, projection_fraction = global_parameters['projection_fraction'][0], mapping_res = global_parameters['mapping_resolution'][0], domain_scale = domain.scale)
        mapping['tip_boundary'] = domain.interpolated_boundary
        domain.mapping[domain.i] = mapping
        proj_num += 1
        proj_calculated = True
        enclosed_area.append(area)

        if global_parameters["plot_projection"][0] == True:
            domain.plot_projection_trunc(trajectories, np.array([1] * len(trajectories)), col_grain_map, features = domain.features, output_directory = output_directory + '/ion_projection_' + str(domain.output_frm) + '.png', steps = 14)


    #save image
    if domain.output_frame == True:
        print("output frame: " + str(domain.output_frm))
        if global_parameters['output_frame'][0] == 1:
            domain.plot_surface(domain.features, output_directory, domain.boundary_intersect == False)
        domain.output_frame = False
        domain.output_frm += 1
        proj_calculated = False
        last_proj = True

    if area_estimate == True:
        #calculates enclosed area via the Shoelace formula
        area = 0.5 * np.abs(np.dot(domain.interpolated_boundary[:, 0], np.roll(domain.interpolated_boundary[:, 1], 1)) - np.dot(domain.interpolated_boundary[:, 1], np.roll(domain.interpolated_boundary[:, 0], 1)))
        enclosed_area.append(area)
        area_time_stamp.append(domain.time)
        surface_field.append(np.mean(domain.BF))
        surface_velocity.append(np.mean(domain.BV))

        centre = np.mean(domain.interpolated_boundary, axis = 0)

        var = 1.0/len(domain.interpolated_boundary) * np.sum(((np.linalg.norm((domain.interpolated_boundary - centre), axis = 1) - np.sqrt(area/np.pi))/np.sqrt(area/np.pi))**2.0)
        variance.append(var)

        if area < enclosed_area[1] * 0.1:
            print("Calculated geometry area bellow certain threshold.")
            print("terminating...")
            break

    #save FEM mesh
    if mesh_output == True:
        if hasattr(domain, 'mesh_coors'):
            print("plotting FEM mesh")
            domain.plot_permittivity(domain.mesh_coors, domain.connectivity_matrix, domain.c_constant, output_directory)

    domain.i += 1

print("Collapse Complete!")

if domain.dynamic_voltage == True:
    print("Outputting voltage curve to directory: " + str(output_directory))
    from scipy.signal import savgol_filter
    import scipy.ndimage

    plt.plot(np.array(domain.it_time[:-1]), np.array(domain.voltage_curve))
    plt.ylim([0.0, np.max( np.array(domain.voltage_curve))*1.2])
    plt.xlabel("Simulation Time (a.u)", size=18)
    plt.ylabel("Pulse Voltage (V)", size=18)
    plt.savefig(output_directory + "/voltage_curve_raw.png")
    plt.close()

    plt.plot(np.array(domain.it_time[:-1]), savgol_filter(scipy.ndimage.maximum_filter(np.array(domain.voltage_curve), 9), 51, 3))
    plt.ylim([0.0, np.max( np.array(domain.voltage_curve))*1.2])
    plt.xlabel("Simulation Time (a.u)", size=18)
    plt.ylabel("Pulse Voltage (V)", size=18)
    plt.savefig(output_directory + "/voltage_curve_smoothed.png")
    plt.close()

if area_estimate == True:
    print("Writing to collapse_test.tsv")

    output_data = []

    collapse = open(output_directory + '/collapse_test.csv', 'w')

    with collapse:
       writer = csv.writer(collapse)

       for a in range(0, len(area_time_stamp)):
           output_data.append([area_time_stamp[a], enclosed_area[1:][a], surface_field[a], surface_velocity[a], variance[a]])

       writer.writerows(output_data)


if proj == True:

    print("Saving mapping object in the output directory as 'mapping.p'")

    #pickle and output reconstruction mapping (class dictionary)
    pickle.dump(domain.mapping, open(output_directory + "/mapping.p", "wb"))

print("collapse time: " + str(time.time() - beginning_time))
sys.exit()

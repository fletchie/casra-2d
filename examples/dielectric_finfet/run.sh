python3 tip_simulation.py --tip_bm input/tip_geom_finfet2.tif --dielectric_mode 2 --mat_db input/materials --bm_key examples/dielectric_finfet/materials_key --global_parameters examples/dielectric_finfet/global_parameters --out examples/dielectric_finfet/output --res 10 --integrator euler --reinit 50 --frm 2 --proj_frame 1


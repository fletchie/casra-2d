python3 tip_simulation.py --tip_bm input/tip_geom_onaxis_precipitate.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/faceted_precipitate/materials_key --global_parameters examples/faceted_precipitate/global_parameters --out examples/faceted_precipitate/output --res 10 --integrator euler 



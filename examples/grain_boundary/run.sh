python3 tip_simulation.py --tip_bm input/tip_geom_vertical_grain.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/grain_boundary/materials_key --global_parameters examples/grain_boundary/global_parameters --out examples/grain_boundary/output --res 10 --integrator euler 

python3 tools/anisotropy.py --mat_db input/materials --mat_key examples/grain_boundary/materials_key_a --out examples/grain_boundary/output

python3 tools/anisotropy.py --mat_db input/materials --mat_key examples/grain_boundary/materials_key_b --out examples/grain_boundary/output


import csv
import argparse
import matplotlib.pyplot as plt
import scipy.special
import numpy as np
import os
import sys
#simulation parameter initialisation
parser = argparse.ArgumentParser(description='Output collapse test metrics.')

parser.add_argument('--input', metavar='input', type=str, default = 'examples/circle_collapse_test/variance_collapse/v3.csv,examples/circle_collapse_test/variance_collapse/v2.csv,examples/circle_collapse_test/variance_collapse/v1.csv',
                    help='The input collapse test data.')

parser.add_argument('--output', metavar='output', type=str, default = 'output',
                    help='The output directory.')

parser.add_argument('--label_param', metavar='label_param', type=str, default = '0.75,0.25,0.25',
                    help='The list of varied simulation parameter values (matching input files)')

parser.add_argument('--title', metavar='title', type=str, default = 'CFL fraction',
                    help='Title of varied parameter')


args = parser.parse_args()

filename = args.input
output_directory = args.output
label_param = args.label_param
param_title = args.title

files = filename.split(",")
param_split = label_param.split(",")

first_val = False

for f in range(0, len(files)):
    file = files[f]
    variance = []
    timestamp = []
    print("Reading from input: " + os.getcwd() + "/" + file)
    with open(file) as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            timestamp.append(row[0])
            variance.append(row[4])
    timestamp = np.array(timestamp, dtype = float)
    variance = np.array(variance, dtype = float)

    tm = np.min(timestamp)
    
    if first_val == False:
        tf = np.max(timestamp)
        first_val = True
    
    mv = np.max(variance)
    
    plt.plot((timestamp - tm)/tf, np.log(variance), label=str(param_split[f]))

legend = plt.legend(title = param_title, fontsize=15)
plt.setp(legend.get_title(),fontsize=16)

plt.xlabel("collapse time (normalised)", fontsize=15)
plt.ylabel("log variance from a circle", fontsize=15)

plt.xticks(fontsize = 12)
plt.yticks(fontsize = 12)

plt.text(0.38, -9.5, r"$var = \sum_{i}^k  \ \frac{1}{k} ( \frac{||\mathbf{x_i} - \mathbf{x_c}|| - R(t)}{R(t)})^2 $", fontsize=16)

plt.savefig(output_directory + "/variance.png")

sys.exit()


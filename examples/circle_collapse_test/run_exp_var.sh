python3 tip_simulation.py --tip_bm input/circle_collapse.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/circle_collapse_test/materials_key --global_parameters examples/circle_collapse_test/variance_collapse/global_parameters_exp025 --out examples/circle_collapse_test/output --res 1 --frm 400 --area_estimate 1 --integrator Euler --reinit 50

cat examples/circle_collapse_test/output/collapse_test.csv > examples/circle_collapse_test/variance_collapse/v1.csv

python3 tip_simulation.py --tip_bm input/circle_collapse.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/circle_collapse_test/materials_key --global_parameters examples/circle_collapse_test/variance_collapse/global_parameters_exp05 --out examples/circle_collapse_test/output --res 1 --frm 400 --area_estimate 1 --integrator Euler --reinit 50

cat examples/circle_collapse_test/output/collapse_test.csv > examples/circle_collapse_test/variance_collapse/v2.csv

python3 tip_simulation.py --tip_bm input/circle_collapse.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/circle_collapse_test/materials_key --global_parameters examples/circle_collapse_test/variance_collapse/global_parameters_exp075 --out examples/circle_collapse_test/output --res 1 --frm 400 --area_estimate 1 --integrator Euler --reinit 50

cat examples/circle_collapse_test/output/collapse_test.csv > examples/circle_collapse_test/variance_collapse/v3.csv

python3 examples/circle_collapse_test/collapse_test_var.py

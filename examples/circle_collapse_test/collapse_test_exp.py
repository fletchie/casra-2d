import csv
import argparse
import matplotlib.pyplot as plt
import scipy.special
import numpy as np
import os

#simulation parameter initialisation
parser = argparse.ArgumentParser(description='Output collapse test metrics.')

parser.add_argument('--input', metavar='input', type=str, default = 'output/collapse_test.csv',
                    help='The input collapse test data.')
parser.add_argument('--output', metavar='output', type=str, default = 'output',
                    help='The output directory.')

parser.add_argument('--initial_time', metavar='initial time', type=float, default = 0.0,
                    help='The time from initial collapse which to start plotting the solution.')

parser.add_argument('--beta', metavar='beta', type=float, default = 2.8,
                    help='The solution temperature parameter.')

parser.add_argument('--f', metavar='f', type=float, default = 0.0014,
                    help='The solution evaporation field parameter.')

parser.add_argument('--amp', metavar='amp', type=float, default = 5e-2,
                    help='The solution amplitude parameter.')

parser.add_argument('--u', metavar='u', type=float, default = 1.0,
                    help='The solution potential parameter.')

args = parser.parse_args()

filename = args.input
output_directory = args.output
t0 = args.initial_time
beta = args.beta
u = - args.u
amp = args.amp
f0 = args.f

print("Reading from input: " + os.getcwd() + "/" + filename)
print("output directory: " + os.getcwd() + "/" + output_directory)

timestamp = []
area = []
surface_field = []
surface_velocity = []
variance = []

with open(filename) as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        timestamp.append(row[0])
        area.append(row[1])
        surface_field.append(row[2])
        surface_velocity.append(row[3])
        variance.append(row[4])

#derive analytical solution for collapse
#plot analytical solution
from scipy.integrate import quad
import scipy.optimize

a = beta * u/f0

#print(timestamp[-1], area[-1], surface_field[-1], surface_velocity[-1])
nt = []
narea = []
nvar = []

for i in range(0, len(timestamp)):
    if float(timestamp[i]) >= t0:
        t0 = float(timestamp[i])
        r0 = np.sqrt(float(area[i])/np.pi)
        break

for i in range(0, len(timestamp)):
    if float(timestamp[i]) >= t0:
        nt.append(float(timestamp[i]))
        narea.append(float(area[i]))
        nvar.append(float(variance[i]))

def integrand(r, a):
    return np.exp(a * 1.0/(r * np.log(r)))
    #return np.exp(a * 1.0/r)


def implicit(r, r0, t, t0, a, beta, amp):
    lhs = 1.0/amp * quad(integrand, r0, r, args=(a))[0]
    rhs = np.exp(-beta) * (t - t0)
    return lhs + rhs

r = []
for t in np.linspace(t0, float(timestamp[-1]), 300):
    r.append(scipy.optimize.root(implicit, r0, args = (r0, t, t0, a, beta, amp), tol = 1e-15)['x'][0])

t = np.linspace(t0, float(timestamp[-1]), 300)

plt.plot((t - np.min(t))/(np.max(t) - np.min(t)), np.pi * np.array(r)**2.0/(np.pi * np.array(np.max(r))**2.0), label = "Problem solution")
plt.plot((nt - np.min(nt))/(np.max(t) - np.min(t)), narea/np.max(narea), label = "level-set solution")

legend = plt.legend(fontsize=15, loc='lower left')
plt.setp(legend.get_title(),fontsize=16)

plt.xlabel("collapse time (normalised)", fontsize=15)
plt.ylabel("area (normalised)", fontsize=15)

plt.xlim(0.0, 1.1)

plt.xticks(fontsize = 12)
plt.yticks(fontsize = 12)

plt.savefig(output_directory + "/collapse_test_exponential.png")
plt.close()

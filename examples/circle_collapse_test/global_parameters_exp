#This is a file defining simulation parameters. Undefined values will resort to default values.

parameter	value	description

#global physics parameters
beta	2.2	temperature parameter in field evaporation law
amplitude	5e-5	amplitude parameter in field evaporation law
tip_pot	-1.0	The tip electric potential - in 2D values should be negative 
gamma	1.0	The angular dependent scaling constant within the anisotropic law. In future versions this will be temperature dependent
delta	1.0	The facet dependent scaling constant within the anisotropic law
field_normal	0	Base evaporation physics of electric field normal surface component (rather than field magnitude)
evaporation_law	1	law selection (0 - linear, 1 - Ahhrenius)
FR	1.03	The field reduction fraction
dynamic_voltage	0	Whether to use a floating or fixed electrode voltage
scale	1	The lengthscale set by level-set grid

#simulation parameters
CFL	0.1	The dynamic timestep faction

#initialisation
initial_iterations	0	initial iterations to smooth front

#output parameters
frame	15	The time constituting one simulation frame
output_frame	1	Output the current simulation geometry to .png

#instrument geometry parameters
detector_distance	20000.0	The relative distance of the detector from the tip
detector_width	50000.0	The relative width of the detector

#numerical methods
BEM	0	The BEM implementation used in the FEM-BEM coupling (1 - linear bem, 0 - constant BEM) 

#mesh parameters
element_size	8.0	The maximum size of the FEM mesh elements 
element_angle	14.0	The minimum angle of FEM mesh corners

#projection parameters
mapping_update	5	The number of frames (see frame) after which the tip-detector mapping will be updated
projection_fraction	0.15	The fraction of the tip height above which ions will be projected
mapping_resolution	0.75	The number of ions projected per boundary element
projection_output	1	(0 or 1) Whether ion trajectories should be outputed (into parsed output directory)
plot_projection	1	whether to plot the ion trajectories from projection
plot_hitmap	1	whether to generate and plot hitmap from ion projection
projection_separation	0.5	The distance between ion projections

#termination parameters
termination_height	55.0	The maximum sample height at which the simulation will terminate, regardless of remaining iterations




python3 tip_simulation.py --tip_bm input/circle_collapse.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/circle_collapse_test/materials_key --global_parameters examples/circle_collapse_test/global_parameters_linear --out examples/circle_collapse_test/output --res 1 --frm 401 --area_estimate 1 --integrator Euler

python3 examples/circle_collapse_test/collapse_test_linear.py --input examples/circle_collapse_test/output/collapse_test.csv --f 0.0014 --amp 5e-3 --u 1 --output examples/circle_collapse_test/output



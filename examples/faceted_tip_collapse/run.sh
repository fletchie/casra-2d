python3 tip_simulation.py --tip_bm input/tip_geom_init_blank.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/faceted_tip_collapse/materials_key --global_parameters examples/faceted_tip_collapse/global_parameters --out examples/faceted_tip_collapse/output --res 10 --integrator euler --proj_frame 9 --frm 10 --reinit 50

python3 tools/anisotropy.py --mat_db input/materials --mat_key examples/faceted_tip_collapse/materials_key --out examples/faceted_tip_collapse/output

python3 tools/stacked_hitmap_generate.py --input examples/faceted_tip_collapse/output/hit_map_data_9.csv --output examples/faceted_tip_collapse/output --colors green --bins 120 --height 30



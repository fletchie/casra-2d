python3 tip_simulation.py --tip_bm input/surface_precipitate.tif --dielectric_mode 2 --mat_db input/materials --bm_key examples/surface_dielectric_precipitate/materials_key --global_parameters examples/surface_dielectric_precipitate/global_parameters --out examples/surface_dielectric_precipitate/output --res 10 --integrator euler --reinit 50 --frm 2 --proj_frame 1


python3 tip_simulation.py --tip_bm input/tip_geom_finfet2.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/finfet_evaporation/materials_key --global_parameters examples/finfet_evaporation/global_parameters --out examples/finfet_evaporation/output --res 10 --proj true --frm 40 --integrator euler --reinit 50


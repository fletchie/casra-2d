python3 tip_simulation.py --tip_bm input/tip_geom_finfet2.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/finfet_evaporation/materials_key --global_parameters examples/finfet_evaporation/global_parameters --out examples/finfet_evaporation/output --res 10 --proj_frame 1 --frm 21 --integrator euler

python3 tools/stacked_hitmap_generate.py --input examples/finfet_evaporation/output/hit_map_data_10.csv --output examples/finfet_evaporation/output --colors skyblue,red --height 100 --bins 120



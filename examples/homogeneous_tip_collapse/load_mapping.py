#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sept 12 2018

Method for opening and output reconstruction mapping

@author: charlie
"""

import argparse
import pickle
import numpy as np
import sys
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.interpolate import splev

parser = argparse.ArgumentParser(description='Open reconstruction mapping.')

parser.add_argument('--input', metavar='input', type=str, default = 'output/mapping.p',
                    help='The input pickled mapping dictionary object')

parser.add_argument('--out', metavar='output', type=str, default = 'output',
                    help='The output directory')

args = parser.parse_args()

#parameter definition
mapping_filename = args.input
output_directory = args.out

mapping = pickle.load(open(mapping_filename, "rb"))

def plot_detector_flowlines(mapping, sample_points, demo_directory = ''):

    """
    Generates and plots detector isolines (constant detector coordinates) for the evolution of the reconstruction mapping.

    :param1 self: The level-set space object instance
    :param2 sample_points: The detector coordinates at which to take the isolines
    :param3 demo_directory: The output demo_directory
    :returns: 0

    """

    fig3 = plt.figure(figsize = (10, 10))


    flowline_list = [[] for _ in range(len(sample_points))]

    for m_ind in mapping:
        sample_space = mapping[m_ind]['sample_space']
        det_space = mapping[m_ind]['detector_space']

        vx = []
        vy = []

        for sp in sample_points:

            #find closest 3 points to sample point
            sample_space = np.array(sample_space)
            det_space = np.array(det_space)

            #print(sample_space)
            #print(det_space)
            #sys.exit()

            ind = np.argpartition(abs(sp - det_space), 3)[0:2]
            #print(sp, det_space[53])
            #print(ind)

            pp = np.polyfit(det_space[ind], sample_space[ind], deg = 2)
            val = np.polyval(pp, sp)

            #val = sample_space[53]

            #if len(px) > 1:
            #   print("breakdown in bijection detected")
            #   print(px, py)

            vx.append(val[0])
            vy.append(val[1])

        tip_surface = mapping[m_ind]['tip_boundary']

        for i in range(0, len(sample_points)):
            pos = np.array([vx[i], vy[i]])
            flowline_list[i].append(pos)

        #plot surfaces
        plt.plot(np.hsplit(tip_surface, 2)[0], np.hsplit(tip_surface, 2)[1], c = 'blue')

    #plot detector isolines
    for i in range(0, len(flowline_list)):
        plt.plot(np.hsplit(np.array(flowline_list[i]), 2)[0], np.hsplit(np.array(flowline_list[i]), 2)[1], c = 'red')
        plt.plot()

    plt.savefig(output_directory + "/map" + ".png")
    plt.close(fig3)

#output flowline map
sample = 20
sample_points = np.linspace(0.32, 0.68, sample)
plot_detector_flowlines(mapping, sample_points, demo_directory = 'output')

python3 tip_simulation.py --tip_bm input/tip_geom_onaxis_precipitate.tif --dielectric_mode 2 --mat_db input/materials --bm_key examples/dielectric_precipitate/materials_key --global_parameters examples/dielectric_precipitate/global_parameters --out examples/dielectric_precipitate/output --res 10 --integrator euler



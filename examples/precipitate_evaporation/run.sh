python3 tip_simulation.py --tip_bm input/tip_geom_precip2.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/precipitate_evaporation/materials_key --global_parameters examples/precipitate_evaporation/global_parameters --out examples/precipitate_evaporation/output --res 10 --proj true --frm 40 --integrator euler

python3 tools/reconstruction_mapping.py --input examples/precipitate_evaporation/output/mapping.p


python3 tip_simulation.py --tip_bm input/tip_geom_multilayer.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/multilayer_evaporation/materials_key --global_parameters examples/multilayer_evaporation/global_parameters --out examples/multilayer_evaporation/output --res 10 --proj true --integrator euler --frm 20

python3 tools/reconstruction_mapping.py --input examples/multilayer_evaporation/output/mapping.p


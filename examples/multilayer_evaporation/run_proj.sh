python3 tip_simulation.py --tip_bm input/tip_geom_multilayer.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/multilayer_evaporation/materials_key --global_parameters examples/multilayer_evaporation/global_parameters --out examples/multilayer_evaporation/output --res 10 --proj_frame 8 --frm 9 --integrator euler

python3 tools/stacked_hitmap_generate.py --input examples/multilayer_evaporation/output/hit_map_data_8.csv --output examples/multilayer_evaporation/output --colors green,red --bins 120



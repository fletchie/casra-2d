python3 tip_simulation.py --tip_bm input/tip_geom_pm.tif --dielectric_mode 0 --mat_db input/materials --bm_key examples/heterogeneous_tip_collapse/materials_key --global_parameters examples/heterogeneous_tip_collapse/global_parameters --out examples/heterogeneous_tip_collapse/output --res 10 --integrator euler --reinit 50


